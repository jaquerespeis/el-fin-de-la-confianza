# Edward Snowden le explica blockchain a su abogado - y al resto de nosotras *por Ben Wizner*

(entrevista)

> El informante de la NSA y su abogado con dificultades para las matemáticas
> discuten tuercas y tornillos de la cadena de bloques y cómo esta podría alterar la
> internet, y la confianza misma.

A lo largo de los cinco años pasados, Edward Snowden y yo hemos llevado una
conversación casi diaria, la mayoría sin relación con sus problemas legales.
A veces nos encontramos en persona en Moscú para un vodka (yo) y batidos (él).
Pero nuestra amistad ha tenido lugar en su mayoría en plataformas de mensajería
segura, un canal que era confortable e intuitivo para él pero al que me tomó
tiempo acostumbrarme. Aprendí a escribir con dos pulgares mientras discutíamos
política, leyes y literatura; familia, amistades y perros adoptados. Nuestras
sensibilidades eran similares pero nuestras visiones del mundo bastante
diferentes: Yo a veces le acusaba de solucionismo tecnológico; él me acusa de
tímido incrementalismo.

A pesar de todo, he encontrado que él es el explicador de tecnología más
claro, más paciente y menos condescendiente que he conocido. Yo con frecuencia
he pensado que desearía que más personas - o tal vez diferentes personas -
pudieran espiar nuestras conversaciones. Lo que sigue es una transcripción muy
ligeramente editada de una de nuestras pláticas. En esta, Ed trata de explicarme
la «cadena de bloques» («blockchain»), a pesar de mis mejores esfuerzos por
aferrarme a mi propia ignorancia.

**Ben Wizner**: La Electronic Frontier Foundation recientemente se burló de que
«la cantidad de energía requerida para descargar tweets, artículos y mensajes
instantáneos que describen qué es "la cadena de bloques" y cómo las monedas
"descentralizadas" son "el futuro" pronto eclipsará la cantidad de energía total
usada por el país de Dinamarca». Es cierto que hay muchos «explicadores de la
cadena de bloques» ahí fuera. Y aún así, me avergüenza admitir que yo aún en
realidad no lo entiendo.

**Edward Snowden**: ¿Estás pidiendo otra lección de matemáticas? He estado
esperando este día. Recordás lo que es una función criptográfica de hash,
¿cierto?

**BW**: Aquí es donde se supone que haga una broma sobre las drogas. Pero no, eso no
lo recuerdo ahora ni lo recordaré nunca.

**ES**: Reto aceptado. Empecemos más sencillo: ¿qué sabés acerca de estas míticas
cadenas de bloques?

**BW**: ¿Que pude haber sido rico si te hubiera escuchado acerca de esto hace
cuatro años? Pero en serio, he oído mucho y entendido poco. «Descentralizado».
«Libros de contabilidad» («ledgers»). ¿Qué demonios es una cadena de bloques?

**ES**: Es básicamente sólo un nuevo tipo de base de datos. Imaginá que las
actualizaciones siempre se agregan al final en lugar de meterse con las entradas
viejas, preexistentes - así como podrías agregar nuevos eslabones a una cadena
vieja para hacerla más larga - y estarás en el camino correcto. Empecemos con
ese concepto, y luego llenaremos los detalles conforme avanzamos.

**BW**: Bueno, pero ¿por qué? ¿Cuál es la pregunta para la que la cadena de
bloques es la respuesta?

**ES**: En una palabra: confianza. Imaginá una base de datos vieja en la que
cualquier entrada puede ser cambiada sólo sobreescribiéndola y haciendo clic
en guardar. Ahora imaginá que esa entrada contiene tu balance bancario. Si
alguien arbitrariamente sólo cambia tu balance a cero, eso como que apesta,
¿cierto? A menos que tengás préstamos estudiantiles.

El punto es que en cualquier momento en el que un sistema permite que alguien
cambie la historia con un teclazo, vos no tenés elección más que confiar en
que un enorme número de personas sean tanto buenas y competentes perfectamente,
y la humanidad no tiene una gran trayectoria de eso. Las cadenas de bloques
son un esfuerzo para crear una historia que no puede ser manipulada.

**BW**: ¿Una historia de qué?

**ES**: Transacciones. En su concepción más antigua y mejor conocida, estamos
hablando acerca de Bitcoin, una nueva forma de dinero. Pero en los últimos
meses, hemos visto esfuerzos para juntar todo tipo de registros en estas
historias. Cualquier cosa que necesita ser memorizada e inmutable. Registros
de atención de salud, por ejemplo, pero también escrituras y contratos.

Cuando pensás acerca de esto en el nivel tecnológico más básico, una cadena
de bloques es sólo una forma elegante de sellar la fecha en cosas, de forma que
podás probar a posteridad que no han sido alteradas luego. El primerísimo
Bitcoin creado, el «bloque de génesis», famosamente tiene adjunta una de estas
«certificaciones generales», que aún hoy podés ver.

Fue una versión cypherpunk de la vieja práctica de tomar una «selfie» con el
periódico del día, para probar que esta nueva cadena de bloques de bitcoin no
fue secretamente creada meses o años antes (lo que le hubiera permitido a la
creadora darse una ventaja injusta en un tipo de lotería que discutiremos luego).

**BW**: Las cadenas de bloques son una historia de transacciones. Eso es tan
decepcionante. Porque he oído algunas afirmaciones extravagantes como: la
cadena de bloques es una respuesta a la censura. La cadena de bloques es una
respuesta a los monopolios de plataformas en linea.

**ES**: Algo de eso es el ciclo de «hype». Mirá, la realidad es que las
cadenas de bloques pueden ser aplicadas en muchas formas en teoría, pero es
importante entender que mecánicamente, estamos discutiendo un concepto muy,
muy simple, y por lo tanto las aplicaciones son todas variaciones de este
único tema: contabilidad verificable. Candente.

Entonces, bases de datos, ¿recordás? El concepto es agrupar pequeños paquetes
de datos, y eso puede ser cualquier cosa. Registros de transacciones, si
estamos hablando de dinero, pero así de fácil podrían ser entradas de «blogs»,
fotos de gatos, enlaces de descarga, o incluso movidas en el juego de ajedrez
más innecesariamente complicado del mundo. Entonces, estampamos estos
registros en una forma complicada que estoy feliz de explicar a pesar de las
protestas, pero si le tenés miedo a la mate, podés pensar en esto como la
versión de alta tecnología de una notaria pública. Finalmente, distribuimos
estos registros recientemente notariados a miembros de la red, quienes
los verifican y los agregan a sus copias independientes de esta historia. El
propósito de este último paso es básicamente asegurar que ninguna persona o
pequeño grupo puede falsificar los números, porque muchísimas personas tienen
copias del original.

Es esta descentralización que algunas esperan que brinde una nueva palanca
para derrocar el status quo de censura y monopolios atrincherados de hoy.
Imaginá que en lugar del mundo de hoy, en el que los datos públicamente
importantes con frecuencia mantienen exclusividad con Corporación Genérica
LTD, la cual puede y juega a ser Dios con estos a costa de lo público,
estuvieran en miles de lugares con cientos de jurisdicciones. No hay un
mecanismo de derribo u otro botón de «seamos malvados», y crear uno
requiere un consenso global de, en general, por lo menos el 51% de la red
en apoyo a cambiar las reglas.

**BW**: Entonces incluso si Peter Thiel ganara su caso y recibiera una
orden de la corte de que algún artículo sobre su dieta vampirezca debe
ser removido, no habría forma de hacerla cumplir, ¿cierto? Esto es, si
la *Revista Blockchain* lo re pública.

**ES**: Correcto- siempre y cuando la *Revista Blockchain* lo esté publicando
en una cadena de bloques pública y descentralizada, podrían tener una orden
judicial de quemar su oficina y eso no haría ninguna diferencia en la red.

**BW**: Entonces... ¿cómo funciona?

**ES**: Vaya, estaba esperando esto. Me estás preguntando lo divertido. ¿Estás
listo para un poco de mate abstracta?

**BW**: Tan listo como nunca.

**ES**: Pretendamos que sos alérgico a las finanzas, y empecemos con un
ejemplo de una cadena de bloques imaginaria de entradas de «blogs» en lugar
de ir a los ejemplos normales de Bitcoin. La propiedad matemática interesante
de las cadenas de bloques, como te mencioné antes, es la inmutabilidad general
en un corto tiempo después de la publicación inicial.

Por simplicidad, pensá que cada nuevo artículo publicado representa un «bloque»
extendiendo esta cadena. Cada vez que publicás un nuevo artículo, estás
agregando otro enlace a la misma cadena. Incluso si es una corrección o
actualización de un artículo viejo, eso va al final de la cadena, sin borrar
nada. Si tus principales preocupaciones eran la manipulación o la censura,
esto significa que una vez que está arriba, está arriba. Es prácticamente
imposible eliminar un bloque anterior de la cadena sin destruir cada bloque
que fue creado después de ese punto y convencer a todas las demás en la red
para que estén de acuerdo en que tu versión alterna de la historia es la
correcta.

Tomemos un segundo, y entremos a las razonas por las que esto es difícil.
Entonces, las cadenas de bloques son un lugar para mantener registros hechos
con mate elegante. Genial. ¿Pero qué significa eso? ¿Qué es lo que en realidad
impide agregar un nuevo bloque en algún lugar que no sea el final de la cadena?
¿O cambiar uno de los eslabones que ya están ahí?

Necesitamos cristalizar las cosas que estamos tratando de contabilizar: típicamente
un registro, una marca de tiempo y algún tipo de prueba de autenticidad.

Entonces, en el nivel técnico, una cadena de bloques funciona tomando los datos del
nuevo bloque - el siguiente eslabón de la cadena - sellándolos con el equivalente
matemático de una fotografía del bloque inmediatamente anterior, y una marca de tiempo
(para establecer el orden cronológico de publicación), luego «haciendo un "hash" de
todo esto junto» en una forma que prueba que el bloque califica para ser agregado a la
cadena.

**BW**: ¿«Hacer un "hash"» es un verbo de verdad?

**ES**: Una función criptográfica de «hash» es básicamente sólo un problema matemático
que transforma cualquier dato que se le tire, de una forma predecible. En cualquier
momento que alimentás una función de «hash» con una foto particular de un gato,
siempre, siempre obtenés el mismo número como resultado. Llamamos al resultado el
«hash» de esa foto, y a alimentar la foto del gato en ese problema matemático le
llamamos hacer un «hash» de la foto. El concepto clave de entender es que si a
exactamente la misma función de «hash» le das una foto ligeramente diferente del gato,
o la misma foto del gato con aunque sea la más pequeña modificación, obtenés un número
(«hash») SALVAJEMENTE diferente como resultado.

**BW**: ¿Y podés tirarle cualquier tipo de datos a una función de «hash»? ¿Podés hacer
un «hash» de una entrada de blog, o una transacción financiera, o *Moby-Dick*?

**ES**: Correcto. Entonces le hacemos un «hash» a estos bloques diferentes, que, si
recordás, son sólo actualizaciones glorificadas de base de datos acerca de transacciones
financieras, enlaces web, registros médicos, o lo que sea. Cada nuevo bloque agregado
a la cadena es identificado y validado por su «hash», que fue producido a partir de
los datos que intencionalmente incluyen el «hash» del bloque anterior. Esta cadena
sin romper lleva hasta el primerísimo bloque, que es lo que le da su nombre.

Aquí te estoy ahorrando algunas sutilezas técnicas, pero los conceptos importantes a
entender son que los bloques en la cadena están diseñados para ser verificables,
ordenados estrictamente de forma cronológica, e inmutables. Cada nuevo bloque creado,
que en el caso de Bitcoin ocurre cada diez minutos, atestigua efectivamente acerca de
los contenidos precisos de todo lo que estuvo antes de este, haciendo que los bloques
viejos sean cada vez más difíciles de cambiar sin romper completamente la cadena.

Entonces, para cuando nuestro Peter Thiel se de cuenta de la historia y decida matarla,
la cadena ya ha construido miles de eslabones de historia publicada y confirmable.

**BW**: ¿Y esto... va a salvar la internet? ¿Podés explicar por qué algunas personas
piensan que la cadena de bloques es una forma de esquivar o reemplazar enormes
monopolios de plataformas tecnológicas? Como, ¿cómo podría debilitar a Amazon? ¿O a
Google?

**ES**: Yo creo que la respuesta ahí es «ilusión». Por lo menos en el futuro previsible.
No podemos hablar de Amazon sin entrar en el tema de monedas, pero yo creo que las
cadenas de bloques tienen mucho más oportunidad de perturbar el comercio que la
publicación, debido a su relativa ineficiencia.

Pensá acerca de nuestro primer ejemplo de tu balance en el banco en una base de datos
vieja. Ese tipo de configuración es rápido, barato y fácil, pero te hace vulnerable a
los errores y abusos de lo que las ingenieras llaman una «autoridad de confianza». Las
cadenas de bloques eliminan la necesidad de autoridades de confianza a cambio de un
costo en eficiencia. Ahora, las autoridades viejas como Visa y MasterCard pueden
procesar decenas de miles de transacciones por segundo, mientras Bitcoin sólo puede
manejar como siete. Pero los métodos para compensar por esa desventaja de eficiencia
están siendo desarrollados, y veremos mejorar la tasa de transacciones para cadenas de
bloques en los próximos años hasta el punto en el que ya no serán una preocupación
central.

**BW**: He estado evitando esto, porque no puedo separar criptomonedas de la imagen de
un montón de amigotes técnicos viviendo en un palacio en Puerto Rico mientras la
sociedad se derrumba. Pero es hora de que expliqués cómo funciona Bitcoin.

**ES**: Bueno, odio se el portador de malas noticias, pero Zuckerberg ya es rico.

El dinero es, por supuesto, el mejor y más famoso ejemplo en donde las cadenas de
bloques han probado tener sentido.

**BW**: Con dinero, ¿cuál es el problema que la cadena de bloques soluciona?

**ES**: El mismo que soluciona en cualquier otro lugar: confianza. Sin ponernos muy
abstractos: ¿qué *es* dinero hoy? Un pedacito de papel, en el mejor de los casos,
¿cierto? Pero la mayor parte del tiempo, es sólo un registro en una base de datos.
Entonces el banco dice que tenés trescientas rupias hoy, y vos realmente esperás que
diga lo mismo o más mañana.

Ahora, pensá acerca del acceso a un balance bancario fiable - ese número mágico
flotando en la base de datos - como algo que no se puede dar por sentado, sino que
más bien es transitorio. Vos sos una de las personas del mundo sin cuenta bancaria.
Tal vez no cumplís con los requisitos para tener una cuenta. Tal vez los bancos no
son fiables en donde vivís, o, como pasó en Chipre no hace mucho, decidieron incautar
los ahorros de la gente para sacarse a sí mismos del apuro. O tal vez el dinero mismo
es poco seguro, como en Venezuela o Zimbabwe, y tu balance de ayer que hubiese podido
comprar una casa ya hoy no alcanza para una taza de café. Los sistemas monetarios fallan.

**BW**: Esperá un minuto. ¿Por qué un «bitcoin» vale algo? ¿Qué genera valor? ¿Qué
respalda la moneda? Cuando debo un bitcoin, ¿qué es realmente lo que debo?

**ES**: Buena pregunta. ¿Qué hace que un pedacito de papel verde valga algo? Si no sos
tan cínico como para decir «hombres armados», que son la razón por la que moneda legal
es tratada diferente al dinero del juego de Monopolio, estás hablando acerca de
escasez y la creencia compartida de la utilidad de la moneda como una reserva de valor
o un medio de intercambio.

Salgámonos de las monedas de papel, que no tienen valor fundamental, para pasar a un
caso más difícil: ¿por qué el oro vale tanto más que sus limitados pero reales usos
prácticos en la industria? Porque la gente en general está de acuerdo en que vale más
que su valor práctico. En realidad eso es todo. La creencia social de que es caro
excavarlo de la tierra y ponerlo en un estante, junto con la expectativa de que
también es probable que otras lo valoren, transforman un metal aburrido en la reserva
de valor más viejo del mundo.

Las monedas basadas en cadenas de bloques como Bitcoin tienen un valor fundamental muy
limitado : a lo sumo, son una ficha que permite guardar datos en los bloques de sus
respectivas cadenas, obligando a todo el mundo que está participando en esa cadena a
guardar una copia por vos. Pero la escasez de por lo menos algunas criptomonedas es
muy real: al día hoy, no más de veintiún millones de bitcoins serán creados, y
diecisiete millones ya han sido reclamados. La competencia por «minar» los pocos
restantes involucra cientos de millones de dolares en equipamiento y electricidad,
las economistas gustan afirmar que es lo que realmente «respalda» Bitcoin.

Pero la dura realidad es que lo único que le da valor a las criptomonedas es la
creencia de una gran población en su utilidad como medio de intercambio. Esa creencia
es lo que hace que cada día las criptomonedas muevan enormes cantidades de dinero a
lo largo del mundo de forma electrónica, sin involucrar bancos. Un día, Bitcoin con B
mayúscula habrá desaparecido, pero siempre que haya gente allá afuera que quiera poder
mover dinero sin bancos, es probable que las criptomonedas tengan valor.

**BW**: ¿Pero y vos? ¿Qué te gusta de esto?

**ES**: Me gustan las transacciones de Bitcoin porque son imparciales. En realidad no
pueden ser detenidas o revertidas, sin la participación explícita y voluntaria de las
personas involucradas. Digamos que Bank of America no quiere procesar un pago para
alguien como yo. En el viejo sistema financiero, ellas tienen una cantidad enorme de
influencia, así como sus pares, y pueden hacer que ocurra. Si una adolescente en
Venezuela quiere recibir un pago en una moneda sólida por un trabajo de desarrollo web
que hizo para alguien en París, algo prohibido por los controles monetarios locales,
las criptomonedas lo hacen posible. Bitcoin podrá no ser aún realmente dinero privado,
pero es el primer dinero «libre».

Bitcoin tiene competidoras también. Un proyecto, llamado Monero, trata de hacer que las
transacciones sean más difíciles de rastrear jugando con cubiletes cada vez que alguien
gasta dinero. Una más nueva hecha por académicas, llamada Zcash, usa una matemática
novedosa para habilitar transacciones realmente privadas. Si no tenemos transacciones
privadas de forma predeterminada en cinco años, será por la ley, no por la tecnología.

**BW**: Entonces si Trump trata de cortar tus ingresos bloqueando a los bancos para que
no te envíen los honorarios por las charlas, igual podrías recibir el pago.

**ES**: Y todo lo que podría hacer al respecto es enviar un «tweet».

**BW**: La desventaja, supongo, es que algunas veces la capacidad de los gobiernos de
rastrear y bloquear transacciones es un bien social. Impuestos. Sanciones. Financiamiento
al terrorismo.

Queremos que vos podás trabajar. También queremos que las sanciones en contra de
oligarcas corruptos funcionen.

**ES**: Si te preocupa que los ricos no pueden esquivar sus impuestos sin Bitcoin, temo
que te tengo malas noticias. Dejando las bromas de lado, ese es un buen punto, pero
creo que la mayoría estará de acuerdo en que estamos lejos del nivel más bajo de poder
gubernamental en el mundo de hoy. Y recordá, la gente en general tendrá que convertir
su dinero mágico de internet a otra moneda para gastarla artículos de alto valor,
entonces los días de preocupación real de los gobiernos están lejos.

**BW**: Explorá eso por mi. ¿La necesidad de convertir Bitcoin a efectivo no afectará
también a tu adolescente venezolana?

**ES**: La diferencia es de escala. Cuando una adolescente venezolana quiere intercambiar
un mes de salario en criptomoneda por su moneda local, ella no necesita una verificación
de identidad y un banco para eso. Ese es el nivel de efectivo con el que la gente hace
trueque todos los días, en particular en las economías en desarrollo. Pero cuando un
oligarca corrupto quiere encargar un yate de placer de cuatrocientos millones de dólares,
bueno, quienes construyen el yate no tienen ese tipo de liquidez, y la existencia de
dinero invisible de internet no significa que la policía no hará preguntas de cómo lo
pagó.

La rampa de salida es un requisito duro para uno, pero el otro puede optar por una
acera.

De forma similar, es más fácil para los gobiernos trabajar colectivamente contra
criminales «reales» - pensá en bin Laden - que reprimir disidentes como Ai Weiwei.

**BW**: Entonces, básicamente, estás diciendo que esto en realidad no ayudará tanto a
los malechores.

**ES**: En realidad podría afectarles, en la medida en que depender de las cadenas de
bloques les requerirá registrar evidencia de sus malos actos en computadoras, las que,
como hemos aprendido en la última década, las investigadoras del gobierno son
notablemente hábiles en penetrar.

**BW**: ¿Cómo describirías las desventajas, si las hay?

**ES**: Como con todas las tecnologías nuevas, habrá disrupción y habrá abuso. La
pregunta es si, en balance, el impacto es positivo o negativo. La desventaja más grande
es la inequidad de oportunidades: estas nuevas tecnologías no son tan fáciles de usar
y son aún más difíciles de entender. Presumen un acceso a un nivel de tecnología,
infraestructura y educación que no está disponible de forma universal. Pensá acerca
del efecto disruptivo que ha tenido la globalización en las economías nacionales en
todo el mundo. Las ganadoras han ganado por millas, no por pulgadas, con las perdedoras
siendo dañadas en el mismo grado. La ventaja de quien hace la primera movida por el
dominio institucional de las cadenas de bloques será similar.

**BW**: Y la economía de internet ha demostrado que una plataforma puede ser
descentralizada mientras el dinero y el poder permanecen muy centralizados.

**ES**: Precisamente. También hay más críticas técnicas que hacer aquí, más allá del
alcance de lo que podemos entrar razonablemente. Baste decir que las criptomonedas
hoy están normalmente implementadas a través de uno de dos tipos de sistemas de
lotería, llamados «prueba de trabajo» y «prueba de interés», que son algo así como un
mal necesario que surge de cómo aseguran sus sistemas contra ataques. Ninguno es
genial. «Prueba de trabajo» premia a aquellas que pueden pagar la mayor cantidad de
infraestructura y consumir la mayor cantidad de energía, lo que es destructivo e inclina
el juego en favor de los ricos. «Prueba de interés» trata de quitar el daño ambiental
simplemente rindiéndose y dando el premio directamente a los ricos, y esperando que
sus egoísmo ilimitado en búsqueda de beneficios mantendrá las luces encendidas. No
hace falta decirlo, son necesarios nuevos modelos.

**BW**: Decime más acerca de los daños ambientales. ¿Por qué hacer dinero mágico de
internet usa tanta energía?

**ES**: Bueno, imaginá que decidís ponerte a «minar» bitcoins. Ya sabés que hay un
número limitado de estos disponibles, pero vienen de algún lado, ¿cierto? Y es verdad:
nuevos bitcoins seguirán siendo creados cada diez minutos por los próximos dos años.
En un intento de entregarlos de forma justa, el creador original de Bitcoin ideó un
esquema extraordinariamente brillante: un tipo de concurso global de mate. La ganadora
de cada ronda de aproximadamente diez minutos obtiene el premio de esa ronda: un
pequeñito cofre del tesoro con bitcoins nuevos, jamás usados, creados de la respuesta
a la que llegó en esa ronda del problema matemático. Para evitar que todas las monedas
de la lotería sean ganadas demasiado rápido, la dificultad del siguiente problema
matemático es aumentada con base en qué tan rápido fueron resueltas las últimas. Este
mecanismo es la explicación de cómo las rondas siempre son de aproximadamente diez
minutos, sin importar cuántas jugadoras entran en la competencia.

El defecto en toda esta brillantez fue el no tomar en cuenta que Bitcoin se volviera
muy exitoso. El premio por ganar una ronda, que una vez valía meros centavos, es
ahora cerca de cien mil dolares, haciendo económicamente razonable que la gente
redirija enormes cantidades de energía, y centros de datos llenos de equipamiento de
computadoras, hacia el concurso de mate - o «minería». Godzillas de computación del
tamaño de un pueblo están siendo vertidos en esta competencia, aumentando la dificultad
de los problemas más allá de la comprensión.

Esto significa que las más grandes ganadoras son aquellas que pueden dedicar decenas
de millones de dólares a resolver una serie de problemas sin fin, sin significado
alguno más allá de minar bitcoins y hacer esta cadena de bloques más difícil de atacar.

**BW**: «Una serie de problemas sin fin, sin significado», suena como... nihilismo.
Hablemos de una perspectiva más amplia. Quería entender cadenas de bloques por el
«hype» incesante. Algunos gobiernos piensan que Bitcoin es una amenaza existencial al
orden mundial, y algunos inversionistas de capital juran que las cadenas de bloques
acomodarán una era dorada de transparencia. Pero me estás diciendo que es básicamente
una base de datos fina.

**ES**: La tecnología es tecnología, y es básica. Son las aplicaciones las que importan.
La pregunta real no es «¿qué es una cadena de bloques?», sino «¿cómo puede ser usada?»
Y eso vuelve a lo que empezamos: confianza. Vivimos en un mundo en el que todas
mienten acerca de todo, incluso adolescentes ordinarias agonizando en Instagram por
cómo proyectar mejor un estilo de vida que en realidad no tienen. Las personas
reciben diferentes resultados en búsquedas con las mismas consultas. Todo requiere
confianza; al mismo tiempo que nada la merece.

Esta es la cosa interesante de pensar acerca de cadenas de bloques: podrían ser ese
diminuto engranaje que nos permita crear sistemas en los que no tenés que confiar. Has
aprendido la única cosa acerca de cadenas de bloques que importa: son aburridas,
ineficientes y despilfarradoras, pero, si están bien diseñadas, son prácticamente
imposibles de manipular. Y en un mundo lleno de mierda engañosa, poder probar que algo
es cierto es un desarrollo radical. Tal vez es el valor de tu cuenta bancaria, tal vez
es la procedencia de tu par de Nikes, o tal vez es tu registro real y permanente en
la oficina de la directora, pero los registros van a transformarse en cadenas que no
podemos romper fácilmente, incluso si están abiertas para que cualquier persona del
mundo las vea.

El «hype» es un mundo en el que todo puede ser rastreado y verificado. La pregunta es
si va a ser voluntario.

**BW**: Eso se puso oscuro rápido. ¿Estás optimista acerca de cómo las cadenas de
bloques van a ser usadas una vez que salgamos de la fase experimental?

**ES**: ¿Qué pensás vos?
