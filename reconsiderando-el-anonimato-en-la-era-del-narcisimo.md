# Reconsiderando el anonimato en la era del narcisismo *por Gabriella Coleman*

(ensayo)

> No todo son identidades robadas, MDMA ni pornografía infantil.

*Para todos los nombres en la historia: Ha llegado el tiempo de sacrificar ese
nombre.*

--Anonymous (Manifiesto del nómada anónimo)

*El ego y la fama son, de forma predeterminada, contradictorios inherentemente
al anonimato. El pasto más alto es el que se corta primero. Sea #Anonymous.*

--Anonymous (@YourAnonNews), 16 de abril de 2012.

La premisa de esta colección es que la privacidad y el anonimato se están
desvaneciendo bajo el violento ataque de la vigilancia gubernamental y
corporativa. La premisa no es nueva; en 2009 muchas defensoras, activistas,
bibliotecarias y libertarias civiles estaban encontrando imposible de imaginar
que la privacidad y el anonimato fueran a existir en el futuro cercano. Este
era el tiempo en que ejecutivas de Silicon Valley estaban construyendo la
infraestructura digital de vigilancia capitalista y defendiéndola haciendo ver
a la privacidad como moralmente dudosa. Por ejemplo, cuando una periodista le
preguntó a Eric Schmidt de Google si deberíamos confiarles nuestros datos, su
respuesta condescendiente fue calculada para eliminar cualquier valencia
positiva hacia la privacidad: «Si tiene algo que no quiere que nadie sepa,
entonces tal vez no debería estar haciéndolo en primer lugar».

Pero cerca de este tiempo se hizo prominente un misterioso colectivo portando
el nombre Anonymous - un movimiento global remoto de protesta predicado en la
idea de que identidades camufladas podrían ponerse a trabajar luchando por la
justicia al hacer posibles comportamientos honestos e imposibilitando los que
busquen celebridad. Aunque al inicio fue usado por troles anónimos coordinando
travesuras puntuales de acoso a lo largo de la internet, el apodo Anonymous
tomó nuevos significados en 2008 cuando las participantes identificadas con
la etiqueta participaron en la serie escalonada de jaqueos y operaciones
políticas diseñadas para la adopción mediática. Figuras identificándose como
Anonymous usaron su conocimiento técnico y su sentido del espectáculo
mediático algo trol para llamar a moratoria a la caza de ballenas japonesa
y noruega; demandando justicia para víctimas de ataques sexuales y brutalidad
policial, algunas veces revelando los nombres de los presuntos perpetradores;
jaquear gobiernos y corporaciones por igual; asistir las ocupaciones de
Egipto, Túnez, España y América del Norte; soportar el levantamiento sirio;
«dox» a oficiales de policía que rociaron con pimienta a las protestantes;
exponer pedófilos en línea; e incluso brindar ropas a las personas sin hogar.
Las agencias de noticias llegaron a contar con Anonymous para un flujo
constante de historias sensacionalistas. Una banda afiliada llamada LulzSec
se dedicó a entregar un nuevo «jaqueo diario» por cincuenta días. Al
infiltrarse en Sony Pictures, publicar noticias falsas en el sitio web de
PBS, y robar correos electrónicos de la organización Arizona Public Safety,
ellas sirvieron pasto a la prensa mientras alegremente auto reportaban sus
hazañas en redes sociales a una base de fans creciente y satisfecha. «En las
últimas pocas semanas estas chicas han ganado cerca de 96,000 seguidoras de Twitter. Eso es 20,000 más que cuando revisé ayer. Twitter le ha dado a
LulzSec un escenario para presumir, y están presumiendo», escribió una
investigadora de seguridad. Anonymous consiguió cortejar incluso más
controversia con los trucos rituales como «Viernes de joder al FBI», que vio
a las jactivistas ir a Twitter al final de cada semana y burlarse de la
agencia encargada de exponer a sus miembros. Para una antropóloga que estudia
las culturas del jaqueo y la tecnología, este fue un momento vigorizante; yo
estaba pegada a mi silla.

Pero conforme el momento ejemplar pasó, la historia de Anonymous viró hacia lo
irónico, y por último incluso trágico, cuando las participantes centrales
fueron traicionadas y arrestadas, y el nombre comenzó a prestarse para
operaciones militares - como campañas anti terrorismo en servicio del estado
nación - a las que muchos de sus miembros anteriores se habían opuesto
vehementemente en ocasiones. Dado el poder omnívoro de la máquina
contemporánea de vigilancia digital para extraer datos de humanos y luego
usarlos contra nosotras, yo nunca fui tan ingenua como para creer realmente
que Anonymous podrían ser nuestras salvadoras. Mi opinión fue más humilde:
estaba más que nada maravillada de la forma en que estas disidentes
enmascaradas abrazaron el anonimato como un tipo de ética para prevenir
comportamientos de pavoneo social y motivar a las participantes en un
silencio solidario antes que la búsqueda de crédito individual, incluso cuando
estaban acosadas, y buscaban la publicidad colectiva, por sus jaqueos, bromas
y protestas épicas. Ciertamente ayudó que Anonymous contribuyera a un número
de causas políticas que yo apoyaba, como Ocupar Wall Street, la exposición de
firmas de vigilancia, y luchas contra la corrupción gubernamental. Yo aprecié
que grupos de personas estuvieran tomando el manto del anonimato en gran parte
para bien - incluso si parecía que podría ser por una última vez antes de que
el anonimato mismo se disipara del todo.

Mi pesimismo acerca de la viabilidad de que el anonimato y la privacidad
sobrevivan (y mucho menos que prosperen) aún domina mi optimismo
generalmente. Pero incluso conforme los días gloriosos de Anonymous han
menguado, finalmente se ha incorporado un movimiento de anonimato y privacidad
ligeramente más fuerte. En parte gracias a la filtración masiva de documentos
de la NSA de Edward Snowden, que brindó una prueba mucho más fuerte de la que
existía anteriormente sobre la vigilancia gubernamental y su confabulación con
el sector privado, ahora se está librando vigorosamente una batalla para
preservar la privacidad y el anonimato. Un poco después de las revelaciones de
Snowden, incontables proyectos tecnológicos dirigidos por jáquers galvanizaron
su desenmascaración, continuaron desarrollando el tipo de herramientas para
aumentar la privacidad en las que periodistas, víctimas de violencia
doméstica, trabajadoras de derechos humanos y disidentes políticas ahora
confían para moverse por el mundo con más seguridad. La usabilidad de estas
herramientas ha mejorado considerablemente. Mientas hace cinco años yo sufría
para recomendar herramientas simples de seguridad para amigas y familiares,
hoy puedo señalar a Signal (una aplicación para mensajes de texto y llamadas
cifradas), el navegador Tor (que anonimiza el tráfico web), un media docena
más de otras aplicaciones, cada una que ha recolectado un financiamiento y
voluntariado crecientes gracias al aumento en el escrutinio de las violaciones
a la privacidad por parte del estado y corporaciones. Aunque Google anunció
que instanciarían cifrado estricto de punta a punta en sus servidores para
asegurar que los datos en los que depende para avivar su empresa comercial no
sean tan fácilmente disponibles para otras, aún están por realizar estos
cambios. Las organizaciones existentes de política, tecnología y activismo,
como Electronic Frontier Foundation, Fight for the Future, el Library Freedom
Project, Big Brother Watch y Privacy International también han ayudado a
asegurar que la privacidad se mantenga como un asunto político en marquesina.
Un flujo constante de escándalos nuevos, como las revelaciones de que
Cambridge Analytica usó datos personales recolectados de Facebook para
influenciar resultados de elecciones, ha amplificado estas preocupaciones y
demostrado la extensión con la cual preguntas sobre datos personales y
privacidad se mantienen pendientes.

Como miembro de una confederación libre de defensoras del anonimato,
rutinariamente doy conferencias sobre las formas en que el anonimato puede
habilitar procesos democráticos como disidencia y denuncia de irregularidades.
En el curso de ese proselitismo se ha vuelto aparente que el anonimato por lo
general es más difícil de defender que otras libertades civiles estrechamente
relacionadas como libertad de expresión y privacidad. El anonimato tiene una
mala reputación. Y no es difícil ver por qué: los usos más visibles del
anonimato en línea, como comentarios en foros, tienden hacia lo tóxico.
Numerosos periódicos en años recientes han eliminado esos foros, los han
controlado, o reconfigurado, atentos a las formas en que por lo general fallan
en engendrar una discusión civil y en lugar de esto crían más discurso dañino
y de odio. El anonimato de forma similar permite que troles en redes sociales
esquiven la responsabilidad mientras atacan de forma brutal (en su mayoría) a
personas de color, mujeres y las de género «queer».

Las connotaciones negativas que muchas tiene sobre el anonimato son evidentes
en su percepción de lo que periodistas y alarmistas llaman la «dark web».
Cuando pregunto a mis estudiantes qué piensan que pasa ahí, muchas la
describen como la esquina más siniestra de la red, infestada de tipos
pervertidos y amenazantes que jaquean bilis en nuestros dispositivos,
supurando y eructando en mini volcanes de pasaportes robados, cocaína y
pornografía infantil. Algunas incluso creen que ser anónimas en línea es
equivalente - en cada instancia - a trasegar en la red oscura. La metáfora
de la oscuridad ha funcionado claramente para implantar erróneas imágenes
nefastas en sus mentes, entonces yo contrarresto con una imagen diferente.

Como mis estudiantes tienen poco conocimiento de cómo funciona el anonimato,
primero les explico que, lejos de ser una escogencia binaria como un
interruptor de luz que se enciende y apaga, el anonimato típicamente involucra
un surtido de opciones y gradientes. Muchas personas se esconden a sí mismas
sólo por el nombre, contribuyendo en línea como un nombre de usuaria, alias,
sobre nombre, avatar o del todo sin atribución: «anónima». Este anonimato
social tiene que ver sólo con la atribución pública y protege el nombre legal
de la participante, mientras que otra información identificante, como la
dirección IP, puede aún ser visible a una red de observadoras como las
administradoras de sistemas que ejecutan el sitio en el que el contenido es
publicado. No hay una única herramienta de anonimato divina que brinde
protección omnipotente, infalible, fiable y a prueba de tontos con la
capacidad de esconder cada rastro digital, revolver todo el tráfico de red y
envolver todo el contenido en un caparazón de cifrado. Lejos de esto: el
anonimato técnico perfecto se considera como un arte demandante y exigente
que causa pérdida de sueño incluso a los jáquers más selectos. Una usuaria
que busca anonimato técnico debe coser un surtido de herramientas, y el
resultado será una colcha de protección más o menos robusta determinada por
las herramientas y la habilidad de la usuaria. Dependiendo de cuántas
herramientas se usan, esta colcha de protección podría esconder toda la
información identificante, o sólo algunos elementos esenciales: el contenido
de los mensajes intercambiados, la dirección IP originaria, las búsquedas en
el navegador web, o la ubicación de un servidor.

El mismo anonimato, continuo, usado por el criminal o el matón o el acosador,
es también usado como una «arma del pobre» en la que confían personas
ordinarias, informantes, víctimas de abuso y activistas para expresar opiniones
políticas controversiales, compartir información sensible, organizarse, brindar
protección contra la represión estatal y construir santuarios de soporte.
Afortunadamente, no hay escasez de ejemplos que iluminan los beneficios
derivados de la protección del anonimato: pacientes, parientes y sobrevivientes
se reunen en foros de internet como DC Urban Moms and Dads para discutir temas
sensibles usando aliases, lo que permite discusiones francas de lo que de otra
forma podrían ser temas estigmatizantes. Víctimas de abuso doméstico, espiadas
pos sus perpetradores, pueden técnicamente cubrir sus rastros digitales con el
navegador Tor y buscar información acerca de refugios. Las informantes hoy son
empoderadas para protegerse a sí mismas como nunca antes dada la disponibilidad
de buzones digitales como SecureDrop, ubicados en lo que es llamado un servidor
«onion», u oculto. Estos puntos de entrega, que facilitan el compartir
información de forma anónima, ahora son hospedados por docenas de sedes
periodísticas establecidas, desde *The Guardian* a *The Washington Post*.
Hospedar datos en servidores «onion» accesibles sólo a través de Tor es un
mecanismo efectivo para contrarrestar la represión y censura patrocinadas por
el estado. Por ejemplo, activistas Iraníes críticas al gobierno protegieron
sus bases de datos haciéndolas disponibles sólo como servicios «onion». Esta
arquitectura hace que el gobierno pueda incautar el servidor web conocido de
forma pública, pero que no pueda encontrar el servidor que brinda el contenido
de la base de datos. Cuando los servidores web son desechables, el contenido
es protegido, y el sitio con la información dirigida a empoderar activistas
puede reaparecer en línea de forma rápida, forzando a que las aspirantes
censuras del gobierno tengan que jugar al «whac-a-mole». Al confiar en una
suite de tecnologías de anonimato, las jactivistas pueden descubrir información
política de importancia al transformarse a sí mismas en fantasmas imposibles
de rastrear: por ejemplo, un grupo se infiltró de forma anónima en salas de
chat de supremacistas blancos luego del trágico asesinato de Heather Heyer y
robaron las bitácoras que detallaban los trabajos de grupos de odio
organizándose para el encuentro de Charlottesville, así como sus viles
reacciones y luchas internas.

Aún así, es cierto que cosas terribles se pueden lograr bajo la cubierta del
anonimato técnico. Pero es necesario recordar que el estado está dotado con un
mandato y tiene recursos significativos para cazar criminales, incluyendo a
aquellas envalentonadas por la invisibilidad. Por ejemplo, en 2018 el FBI
solicitó cerca de 21.6 millones de sus $8 mil millones de presupuesto anual
para su programa «Going Dark», usado para «desarrollar y adquirir herramientas
para el análisis de dispositivos electrónicos, capacidad de criptoanálisis y
herramientas forenses». El FBI puede desarrollar o pagar costosas programas
que aprovechen vulnerabilidades de seguridad o herramientas de jaqueo, que han
usado para infiltrarse y tomar control sobre sitios de pornografía infantil,
como hicieron en 2015 con un sitio llamado Playpen. Sin duda alguna, el estado
debería tener la capacidad de luchar contra criminales. Pero si se les brindan
capacidades de vigilancia sin restricciones como parte de esa misión, las
ciudadanas perderán la capacidad de ser anónimas y el gobierno se arrastrará
hacia el fascismo, que es otro tipo de criminalidad. Las activistas, por otra
parte, que son en gran parte pobres de recursos, con frecuencia son puestas en
la mira de actores estatales de forma injusta y por lo tanto requieren
anonimato. De hecho, el anonimato permite hablar y organizarse a activistas,
fuentes y periodistas que aún no están en la mira del estado, como es su
derecho, sin interferencia.

La importancia, usos y significado del anonimato dentro de una entidad
activista como Anonymous es menos clara que en mis anteriores ejemplos. Esto
puede surgir en parte del hecho que Anonymous es confuso. El nombre es un alias
compartido que está libre para que cualquiera lo tome, lo que Marco Deseriis
define como un «nombre impropio». Disponible de forma radical para todas las
personas, este tipo de etiqueta se dota con una susceptibilidad ya incorporada
hacia la adopción, circulación y mutación. El público con frecuencia no está
al tanto de quiénes fueron Anonymous, cómo trabajaron y cómo reconciliaron sus
distintas operaciones y tácticas. Hubo cientos de operaciones que no tuvieron
relación entre ellas y que con frecuencia no estaban alineadas ideológicamente - algunas en apoyo firme a la democracia liberal, otras buscando destruir el
estado liberal en favor de formas de gobierno anarquistas. También es por esta
razón que «Anonymous no es unánime» se volvió una broma popular entre las
participantes, recordándole a las espectadoras el carácter descentralizado y
sin liderazgo del grupo, y señalando la existencia de desacuerdos sobre
tácticas y creencias políticas.

Para miembros del público, así como mis estudiantes, su valoración de las
operaciones de Anonymous con frecuencia dependían de su reacción a una de las
cientos de operaciones con las que se hayan podido cruzar, su percepción de
la figura de Guy Fawkes y otras idiosincrasias como su opinión sobre la
justicia de vigilantes o acción directa. Mientras algunas espectadoras
adoraron su buena disposición de rebelarse contra la autoridad, otras
estuvieron horrorizadas por su prontitud a romper la ley con tal impunidad.
Entre una cacofonía de posiciones sobre Anonymous, yo siempre encontré una
categoría de resistencia a respaldar a Anonymous: el tipo bueno y legítimo
(profesoras de derecho académico o estudiosas de la política liberal, por
ejemplo), siempre escépticas y consternadas de Anonymous en su totalidad por
una pequeña cantidad de operaciones de justicia vigilante que se realizaron
bajo su manto. La cosa extraña era la forma en que esos tipos legítimos
encontraron un acuerdo con una más pequeña, pero vocal, clase de activistas de
izquierda - aquellas entusiastas de apoyar maniobras de acción directa pero
llenas de reservas cuando eran llevadas a cabo de forma anónima. Ellas tendían
a estar de acuerdo en una creencia en particular: que la gente que abraza el
anonimato con el propósito de actuar (y no simplemente de hablar), en especial
cuando tales acciones rodean el debido proceso, son personajes turbios por
defecto porque el anonimato tiende a anular la rendición de cuentas y por lo
tanto la responsabilidad; que la máscara en sí misma es un tipo de mentira
encarnada, refugiando cobardes en quienes simplemente no se puede confiar y
quienes no son responsables de las comunidades a las que sirven.

Pero estos argumentos ignoran la variedad y usos justos del anonimato que
Anonymous puso en servicio de la honestidad y la nivelación social. Con la
distancia concedida por el tiempo, mi convicción de que Anonymous ha sido
generalmente una fuerza en el mundo digna de confianza y una embajadora
encomiable por el anonimato es hoy aún más fuerte. Incluso si su presencia ha
decaído, han dejado atrás una serie de lecciones acerca de la importancia del
anonimato que son más vitales de tomar en cuenta en la era de Trump que nunca
antes. De estas lecciones, consideraré aquí los límites de transparencia para
combatir a la desinformación y la capacidad del anonimato para proteger a
las personas honestas, así como su habilidad para minimizar los daños de la
celebridad desenfrenada.

## Lección 1: La transparencia no es una panacea para la desinformación

Primero consideremos el poder de Anonymous y el anonimato a la luz del clima
político contemporáneo, con periodistas, comentaristas y activistas en una
crisis existencial turbulenta acerca de la confianza, verdad, y noticias
basura. Déjenme declarar desde el principio que demandar transparencia, en mi
libro de jugadas políticas, está alto en la lista de tácticas oportunas que
pueden ayudar a animar las actividades políticas. Buscar transparencia de
personas, corporaciones e instituciones que podrían tener algo malo que
ocultar, y la influencia para lograr ocultarlo, ha servido en incontables
circunstancias para avergonzar a estafadoras y cabronas de forma que salgan
de sus codiciadas posiciones de poder (y yo con resolución defiendo el
anonimato por su capacidad de engendrar transparencia). Aún así, la efectividad
de la demanda de transparencia y honestidad con frecuencia ha sido exagerada,
y sus defensoras algunas veces de forma ingenua atribuyen una fe casi mágica
a tal táctica mientras consideran inmorales los medios anónimos para el mismo
fin de la honestidad. En el pasado, cuando he discutido la importancia del
anonimato y los límites de la demanda de transparencia en busca de la verdad,
muy pocas personas me tomaron del todo en serio, aparte de un pequeño grupo
de académicas y activistas ya imbuidas en hacer afirmaciones similares. Todo
esto cambió cuando Donald Trump se volvió el presidente. De repente era mucho
más fácil ilustrar la lógica detrás de la famosa ocurrencia de Mark Twain:
«La verdad es poderosa y prevalecerá. No hay nada malo con esto, excepto que
no es así».

El sentido común periodístico, aún intacto en gran medida llegando a la
elección, dictó que refutar falsedades preservaría la integridad del mercado
de ideas - la arena en la que la verdad, dado suficiente tiempo al aire, puede
borrar mentiras. Sin embargo, después de que Trump ganó la elección, muchas
periodistas fueron forzadas a confrontar el hecho de que el sentido común, como
tan astutamente lo puso el antropólogo Clifford Geertz, es «lo que la mente
llena de supuestos... concluye». Para las críticas, los fallos morales de
Trump son obvios en su comportamiento vil y mentiras patológicas, ambas
grabadas de forma meticulosa por periodistas. *The Washington Post* ha
rastreado las declaraciones falsas o engañosas desde su primer día en función,
y encontraron que su celo por mentir sólo ha crecido con el tiempo. Sin
embargo, aunque sus partidarias también distinguen a Trump como audaz, están
armadas con un conjunto distinto de supuestos y por lo tanto llegan a
conclusiones radicalmente distintas sobre su carácter y acciones. En la misma
auditoría de declaraciones falsas de Trump en *The Washington Post*, una
comentarista en línea muestra cómo algunas de sus defensoras están dispuestas
a pasar por alto sus mentiras, interpretándolo como auténtico y emocionalmente
cercano comparado con el típico político: «Trump con frecuencia es hiperbólico
y leva sus sentimientos en su manga para que todas lo vean, refrescante dirán
algunas. Una con frecuencia se pregunta si para él es tan siquiera posible ser
tan hipócrita como el político típico. Su corazón y políticas parecen estar en
el lugar correcto».

Apelando a aquellas que desconfían del entorno político contemporáneo, algunas
de las seguidoras más acérrimas de Trump argumentan que él sirve a un propósito
más alto y noble al sacudir el poder establecido. Incluso aunque el sentido
común «varía de forma dramática de una persona a la siguiente», como Geertz lo
dijo, Trump aún ha logrado secuestrar nuestra atención colectiva, provocando a
los medios para que cubran cada uno de sus movimientos, con frecuencia a
través de una actuación de autenticidad falsa pero convincente. Ya sea en
horror, diversión o adulación, el público estadounidense se mantiene unido,
con una cerveza en la mano, las pinzas de barbacoa en la otra, boquiabierto,
hipnotizado por sus payasadas escandalosamente arrogantes. Mientras algunas ven
la presidencia de Trump como un ingobernable desastre que se desenvuelve en
cámara lenta justo frente a sus ojos, otras están claramente eufóricas, animando
a Trump como si estuvieran asistiendo a un rally de camiones monstruos. Trump
es un intérprete tan efectivo que hasta ahora no sólo ha logrado esquivar
cualquier repercusión de sus mentiras descaradas, sino que también está
preparado para acusar a los medios establecidos de ser mentirosos: «Yo tomo
mis propias decisiones, en gran parte basadas en datos acumulados, y todas lo
saben. ¡Algunos medios de NOTICIAS FALSAS, para marginalizar, mienten!» Bajo
tal despiadado ataque, la verdad tiene dificultades para prevalecer.

En contraste con Trump, Anonymous - una cadena de colectivos desgarbada, semi
caótica (aunque a veces bastante organizada), compuesta por miles de personas
y docenas de grupos distintos actuando bajo su nombre en las cuatro esquinas
del globo, con poca a nada coordinación entre muchas de ellas - aparece, en
todo sentido, como una entidad más seria y digna de confianza. Mientras Trump
nos ayuda a ver esto otra vez, yo he hecho el siguiente punto desde hace mucho
tiempo: si una hace balance de la gran mayoría de sus operaciones después del
2010, Anonymous por lo general siguió un guión bastante convencional basado
en un impulso por decir la verdad. Anonymous con frecuencia emparejaba un
anuncio sobre alguna indignación que buscaban publicitar con documentos
verificables u otro material. Tal fue la situación cuando Anonymous lanzó
#OpTunisia en enero de 2011 y fueron algunas de las primeras extranjeras en
acceder y exhibir ampliamente los vídeos de protesta que estaban siendo
generados en el terreno - grabaciones que publicaron en línea para despertar
la simpatía pública y estimular la cobertura mediática. Anonymous de forma
rutinaria adquirió correos electrónicos y documentos (que, por cierto, nunca
se ha encontrado que hayan sido adulterados) y los publicó en línea,
permitiendo que las periodistas posteriormente los minaran para sus
investigaciones. Su impulso para sacar la verdad también fue ayudado por
material pomposo ingeniado para que fuera viral. Decir la verdad, después de
todo, siempre se puede beneficiar de una estrategia de relaciones públicas
más astuta.

En ocasiones, Anonymous se basó en el clásico engaño - presionando con una
mentira que en su debido tiempo sería revelada como una mentirilla para
obtener una mayor verdad. Por ejemplo, LulzSec jaqueó y pintarrajeó PBS en
represalia por su película de *Frontline* sobre WikiLeaks, *WikiSecrets*, la
cual trajo la cólera de miembros de LulzSec que condenaron la película por como
sensacionalizó y psicoanalizó la «oscura» vida interna de Chelsea Manning,
eludiendo los asuntos políticos urgentes planteados por la publicación de
cables diplomáticos por Wikileaks. Al ganar acceso al servidor, las jáquers
implantaron noticias falsas acerca del paradero de dos raperos célebres.
Con una foto de Tupac Shakur, con la cabeza ligeramente ladeada, llevando una
gorra para atrás y una sonrisa cordial, el título anunciaba la exclusiva:
«Tupac sigue vivo en Nueva Zelanda». Continuaba: «El prominente rapero Tupac
ha sido encontrado vivo y bien en un pequeño complejo turístico en Nueva
Zelanda, según reportan las lugareñas. El pequeños pueblo - innombrado debido
a riesgos de seguridad - presuntamente alojó a Tupac y a Biggie Smalls (otro
rapero) por varios años. Un lugareño, David File, murió recientemente,
dejando evidencia y reportes de la visita de Tupac en un diario, que solicitó
fuera enviado a su familia en los Estados Unidos». Aunque de primera entrada
podría parecer poco claro por qué, esta pintarrajeada entregó una declaración
política particularmente potente. Aunque el artículo falso y el jaqueo causaron
gran sensación en la prensa global, la mayoría de periodistas fallaron en
abordar la crítica de LulzSec sobre la exagerada superficialidad de la
película. Y aún así, LulzSec logró forzar la cobertura sensacionalista a
través de su combo de jaqueo-engaño, instaurando a través de esta puerta
trasera su crítica original acerca de las tendencias de periodistas a
sensacionalizar las noticias.

Pero en la mayoría de casos, el engaño fue usado en pequeñas cantidades y
Anonymous simplemente amplificó su mensaje ya siendo transmitido por otras
activistas y periodistas. Por ejemplo, una de sus operaciones más famosas,
#OpSteubenville, tenía que ver con un horrible caso de asalto sexual por
miembros de un equipo colegial de fútbol en un pequeño pueblo metalúrgico de
Steubenville, Ohio. Luego de que el *New York Times* escribió una revelación
detallando el caso, Anonymous continuó exhibiendo hiperactivamente la evolución
acerca del asalto de Steubenville a través de vídeos y en Twitter, asegurando
su visibilidad por meses hasta que dos adolescentes fueron encontrados
culpables de violación en marzo de 2013.

Anonymous, como Trump, atraía al público y a los medios con actos ostentosos
de espectáculo. Pero Anonymous no se unió como un punto de voluntad individual
para buscar crédito, sino como la convergencia de una multitud de actrices
contribuyendo a una multitud de movimientos sociales, colectivos y
organizaciones existentes. Anonymous brilló más intensamente entre 2011 y
2015, durante un periodo tumultuoso de malestar y descontento global, evidente
en el rango de sublevaciones populares de gran escala a lo largo del mundo: el
movimiento 15-M en España, las primaveras árabe y africana, los campamentos
de Occupy, el movimiento estudiantil en Chile, Black Lives Matter, el Umbrella
Movement en Hong Kong. Anonymous contribuyó con cada una de estas campañas.
Su profundo entrelazamiento con algunas de estas amplias causas sociales ha
sido conmemorado por muchas que trabajaron con o se beneficiaron de Anonymous.
En 2011, fue compartida una foto de niños tunecinos sentados en el patio de
su escuela, portando una máscara de papel blanco de Guy Fawkes, como un gesto
de gratitud hacia Anonymous por llevar el mensaje de su situación al mundo.
Más recientemente, considere la prematura muerte de Erica Garner, una activista
en contra de la brutalidad policial e hija de Eric Garner, un hombre que murió
a manos de un oficial del departamento de policía de Nueva York. No mucho
después de su muerte, la persona manejando su cuenta de Twitter le ofreció sus
respetos a Anonymous: «Un grito para Anonymous... Uno de los primeros grupos
de personas que sujetaron a Erica desde el inicio. Ella les amaba a todas de
verdad #opicantbreathe».

El punto de yuxtaponer las mentiras de Trump con las verdades de Anonymous es
meramente para resaltar que la transparencia y el anonimato rara vez siguen
una fórmula moral binaria, con la primera siendo buena y la segunda siendo
mala. Hay muchos estafadores, especialmente en la arena política, que hablan y
mienten sin una máscara literal - Donald Trump, Silvio Berluscconi, George W.
Bush, Tony Blair - y nunca son obligados a rendir cuentas de forma apropiada,
o requiere de un esfuerzo tipo David y Goliath para eliminarles del poder. De
hecho, Trump, actuando al descubierto, es percibido como «transparente» porque
es un individuo que no se esconde detrás de una máscara y porque, para algunas,
es un político honesto por tener la valentía de decir cualquier cosa, sin
importar qué tan ofensiva. (Para algunas, entre más ofensiva mejor.) Como
sugirió el sociólogo Erving Goffman hace mucho tiempo, los humanos - tan
adeptos al arte del engaño - despliegan un lenguaje astuto y a veces una
actuación confabuladora, en lugar de esconderse, para engañar de forma
efectiva.

## Lección 2: el escudo del anonimato

La transparencia se puede lograr a través de marcos institucionales existentes,
ya sea a través del acceso a registros públicos, como usando la Ley por la
Libertad de la información, o usando la función de perro guardián del Cuarto
Estado. Pero cuando estos métodos fallan, las informantes anónimas pueden ser
un mecanismo efectivo para hacer salir la verdad. En el caso de la Corte
Suprema de 1995 McIntyre v. Ohio Elections Commission se articula de forma
convincente el apoyo a esta posición, que argumenta que el anonimato resguarda
a la votante, la persona honesta, e incluso a la opinadora anónima de la
venganza gubernamental o las masas enojadas del cuerpo político. Las juezas
de dicho caso escribieron: «El anonimato es un escudo de la tiranía de la
mayoría.... Así ejemplifica en particular el propósito detrás de la Carta de
Derechos de la Primera Enmienda: para proteger a individuos impopulares de
represalias... a manos de una sociedad intolerante». Para señalar su conciencia
de y contribución a esta tradición, las participantes de Anonymous gustan de
citar a Oscar Wilde: «Una persona es menos sí misma cuando habla en su propia
persona. Denle una mascara, y les dirá la verdad».

Uno de los ejemplos más impactantes y efectivos que confirma el fundamento de
la Corte Suprema y del aforismo de Oscar Wilde involucra una máscara facial
portada por un doctor en medicina. En 1972, un psiquiatra presentando en una
reunión de la Asociación Americana de Psiquiatría se escondió a sí mismo con
un distorsionador de voz, un nombre seudónimo y una máscara de hule.
Llamándose Dr. H. Anonymous, y en servicio en un panel llamado «Psiquiatría:
¿amiga o enemiga de las homosexuales?», el doctor empezó confesando: «Soy
homosexual. Soy psiquiatra». En esa época, la homosexualidad había sido
clasificada por la psiquiatría como una enfermedad, haciéndola particularmente
inmune a las críticas. Esta revelación audaz y valiente logró lo que Dr. H.
Anonymous y sus aliadas se habían propuesto: envalentonar los esfuerzos en
curso para despatologizar la homosexualidad. Sólo un año más tarde, la
asociación había removido la homosexualidad de su manual de diagnóstico y Dr.
H. Anonymous, quien había temido que no recibiría la plaza académica si su
empleadora se enteraba que era gay, permaneció protegido (y con empleo), y
sólo hizo su nombre público veintidós años más tarde, John E. Fryer.

Muchos otros individuos y grupos han hablado y actuado honestamente encubiertos
en un intento de exponer algún abuso o crimen y usar el anonimato para
escudarse a si mismos no solo de pares, colegas o empleadores, como hizo Dr.
Fryer, pero también de retribución gubernamental. Anonymous, Antifa, Chelsea
Manning (durante su corta ocupación como filtradora anónima), Garganta Profunda
(la fuente anónima durante el escándalo de Watergate), y la Comisión Ciudadana
para Investigar al FBI - de las cuales todas han ganado alguna medida de
respeto sólo por sus palabras y acciones, no sus identidades legales - han
entregado una transparencia que ha sido considerada valiosa sin importar su
percibida falta impunidad u opacidad. En la exposición de delitos atroces del
gobierno, el anonimato tiene el potencial de hacer que el acto riesgoso de la
denuncia sea un poco más seguro. Tal fue el caso de la Comisión Ciudadana para
Investigar al FBI, un grupo de ocho paladinas opuestas a la guerra que
irrumpieron en una oficina del FBI en 1971 y se llevaron cajones de archivos
que contenían pruebas de COINTELPRO, un programa secreto de vigilancia y
desinformación impuesto contra docenas de movimientos activistas. El programa
fue eventualmente cerrado luego de ser considerado ilegal por el gobierno de
los Estados Unidos y las intrusas nunca fueron detenidos. Si estas ciudadanas
hubiesen sido atrapadas - el FBI dedicó doscientas agentes al caso pero,
fallando en encontrar tan siquiera a una de las intrusas, se rindió en 1976 -
su destino hubiese probablemente incluido una costosa batalla legal seguida
por tiempo detrás de los barrotes.

Trágicamente, la gente que ha hablado descubierta, a veces ha sido expuesta a
daños y calumnias. Ser honesta y transparente, en especial cuando le faltan
partidarias y fieles, le pone en riesgo de una traumática pérdida de privacidad
y, como en el caso de Chelsea Manning, de seguridad física. Después de ser
expuesta por un jáquer, Manning fue torturada por un año en confinamiento
solitario por su denuncia. La ex gimnasta estadounidense Rachael Danhollander,
una de las primeras que se atrevió en denunciar a Larry Nassar, el doctor
médico del equipo de gimnastas olímpicas de Estados Unidos que asaltó
sexualmente a más de 260 mujeres jóvenes, explicó en un editorial que su vida
y reputación fueron arruinadas por hablar, hasta que la marea empezó a cambiar:
«Perdí mi iglesia. Perdí a mis amigas más cercanas como resultado de abogar por
las sobrevivientes que han sido victimizadas por fallos institucionales
similares en mi propia comunidad. Perdí cada fragmento de privacidad». Todos
estos ejemplos traen a la mente el adagio «privacidad para las débiles,
transparencia para las poderosas». El anonimato puede llenar una prescripción
de transparencia al proteger de represalias a las honestas.

## Lección 3: Contención del ego y daños de la celebridad desenfrenada

El rechazo de Anonymous a los cultos de la personalidad y a la búsqueda de
celebridad es el impulsor menos entendido del anonimato, pero uno de los más
vitales de entender. Los trabajos del anonimato bajo este registro funcionan
menos como un dispositivo de honestidad y más como un evento de nivelación
social. A menos que siguieran Anonymous de cerca, este ethos era más difícil de
deducir, ya que sólo era grandemente visible en los canales traseros de sus
interacciones sociales - en salas de chat privadas o semi privadas con
explosiones ocasionales en Twitter, tales como este tweet de @FemAnonFatal:

> • FemAnonFatal es un Colectivo • NO un movimiento individual No un lugar
> para autopromoción NO un lugar para el ODIO SINO un lugar para la SORORIDAD
> Es un lugar para Nutrir la Revolución Lea Nuestro Manifiesto... • Deberían
> habernos previsto • #FemAnonFatal #OpFemaleSec

Por supuesto, es más fácil proferir tales pronunciamientos elevados sobre
solidaridad que implementarlos. Pero Anonymous imponía este estándar castigando
a quienes salía a la luz buscando fama y crédito. En mis muchos años de
observarles, fui testiga directa de las consecuencias para aquellas que
violaron esta norma. Si una participante novata era vista fijándose demasiado
en obtener elogios de sus pares, ella podría ser reprendida suavemente. Para
aquellas que se atrevieron a adjuntar sus nombres legales a alguna acción o
creación, la restitución era más feroz. Como mínimo, la transgresora era
usualmente ridiculizada o fustigada, con unos pocos individuos ritualmente
«asesinados» al prohibirseles participar en una sala de chat o red.

Junto a momentos salpicados de acción disciplinaria, esta norma tendía a
ser tarareada calladamente en el fondo, pero no de forma menos poderosa -
mandando que todo lo creado bajo la tutela de Anonymous fuese atribuido al
colectivo. Es importante afirmar que, en contraste con sus más conocidos
compatriotas jáquers forajidos, la mayoría de las participantes de Anonymous
estaban maniobrando en un territorio legal sin ambigüedades; aquellas que
conjuraban un mensaje convincente de esperanza, disidencia o protesta a través
de medios como vídeos, manifiestos enérgicos, imágenes u otras astutas llamadas
a las armas ingeniadas para que se hicieran virales no estaban incentivadas
hacia el anonimato por el castigo legal. Es más, el decreto ético de sublimar
la identidad personal tenía dientes: las participantes por lo general se
abstenían de firmar con sus nombres legales estos trabajos, algunos de los
cuales llegaron a la prominencia, recibiendo cientos de miles de vistas en
YouTube. Mientras una recién llegada se hubiera rendido ante este decreto por
miedo al castigo, la mayoría de las participantes llegaron a abrazar este
ethos como una estrategia necesaria para los objetivos más amplios de minimizar
la jerarquía humana y maximizar la equidad humana.

Observar esta contención del ego fue revelador. La pura dificultad de vivir
este credo se reveló en la práctica. Como antropóloga, mi labor metodológica
manda cierto grado de participación directa. La mayoría de mi labor con
Anonymous consistió en trabajo de traducción periodística, pero en unas pocas
ocasiones me uní a un pequeño grupo de creadoras mediáticas para fabricar
mensajes incisivos para vídeos diseñados para provocar que la gente tomara
acción. Como una escritora académica separada de la necesidad de concisión,
recuerdo brillar con orgullo por la redacción compacta que improvisé para
canalizar la furia colectiva acerca de alguna asquerosa injusticia política.
Resistir incluso un poquito de crédito por la hazaña fue difícil en ese momento,
pero a la larga fue satisfactorio, sentando las bases para hacerlo de nuevo.
Aún así, no sólo iba en contra de lo que me había enseñado la sociedad, sino
que también del modo de ser académica - alguien cuyo sustento depende
enteramente de un sistema bien arraigado con siglos de antigüedad que asigna
respeto con base en el reconocimiento individual. Como la auto nombrada
autora de esta pieza, sería hipócrita de abogar por una moratoria total a la
atribución personal. Pero cuando una economía moral que está basada en el
impulso por el reconocimiento individual se expande de tal manera que
desplaza otras posibilidades, podemos descuidar, para nuestro riesgo colectivo,
otras formas esenciales de ser y estar con otras.

Uno de los muchos peligros del individualismo o la celebridad sin control es la
facilidad con la que se transforma en total narcisismo, un rasgo de la
personalidad que obviamente excluye la ayuda mutua, ya que prácticamente
garantiza algún nivel de caos interpersonal, cuando no la total carnicería en
forma de vitriolo, matonismo, intimidación y mentiras patológicas. Trump, de
nuevo, puede servir como una referencia útil, ya que viene a representar un
ideal casi platónico de narcisismo en acción. Su presidencia ha demostrado que
un solipsismo sin complejos puede actuar como un tipo de lente distorsionador,
previniendo el funcionamiento normal de la transparencia, la verdad, el
avergonzamiento y la rendición de cuentas al ofrecer una indiferencia tan
completa que parece casi incapaz de contemplar la difícil situación de otras o
admitir equivocación. Y en el ascenso de Trump yace una lección mucho más
perturbadora y general que contemplar: que obtener una de las posiciones
políticas más poderosas en una de las naciones más poderosas del mundo es
posible sólo porque tales comportamientos en búsqueda de la celebridad son
recompensados en muchos aspectos de nuestra sociedad. Muchos ideales culturales
dominantes nos exigen a buscar reconocimiento - ya sea de nuestras acciones,
palabras o imágenes. Aunque la celebridad como un ideal no es nueva de ninguna
forma, en internet hay proliferantes avenidas sin fin a nuestra disposición
para darse cuenta, registrar de forma numérica (en «likes» y «retweets»), y así
consolidar y normalizar más la fama como una condición de cotidianeidad.

Desde luego, el narcisismo y la celebridad están lejos de estar sin control. Por
ejemplo, los rasgos vanidosos y auto engrandecedores de Trump hoy son sujetos
de una crítica y análisis salvajes por una camarilla de expertas, periodistas
y otras comentaristas. Incluso si la celebridad es un ideal cultural duradero
persistente y siempre en expansión, la humildad también es valorada. Esto es
cierto más obviamente en la vida religiosa, pero una bandada de éticas
prohibiciones mundanas diarias también buscan frenar el apetito del ego
humano por gloria y gratificación. Algo tan menor como la sección de
agradecimientos de un libro sirve - aunque sea mínimamente - para refrenar la
noción egoísta de que los individuos son enteramente responsables de las
loables creaciones, descubrimientos o trabajos artísticos que se les atribuyen.
Después de todo, es la extendida confesión y el momento de gratitud para
reconocer que tal escrito hubiese sido imposible, o mucho peor, sin la ayuda de
una comunidad de pares, amistades y familiares. Pero historias que celebran la
solidaridad, equidad, la ayuda mutua y la humildad son más raras. Y más escasos
aún son los mandatos en los que individuos son llamados a perfeccionar el arte
de la auto obliteración. Anonymous es probablemente uno de los laboratorios
más grandes, abierto a muchas, en llevar a cabo un experimento colectivo para
limitar el deseo del crédito individual, fomentando formas de conectar con
nuestros pares a través de compromisos de indivisibilidad.

Aunque el anonimato puede incentivar todo tipo de acciones y comportamientos,
en el caso de Anonymous significó que muchas de las participantes estaban ahí
por razones de principio. Su misión de principios íntegros para corregir los
errores infligidos en personas encarna el espíritu del altruismo. Su demanda
por humildad ayuda a desalentar, incluso si no eliminó del todo, a aquellas
participantes que simplemente codician la gloria personal al unirse a las filas
del grupo. Voluntarias, obligadas a dar crédito a Anonymous, también
mantuvieron bajo control un problema que azota todo tipo de movimientos
sociales: la auto nominación de estrella de rock o líder, propulsada al
estrellato por los medios, de quienes éxitos o fallos de reputación pueden
servir injustamente como representantes del ascenso y caída del movimiento de
manera más general. Si tal auto promoción se vuelve flagrante, los conflictos
y las luchas internas típicamente afligen las dinámicas sociales, que a su vez
debilitan el poder del grupo para organizarse de forma efectiva. La ya limitada
energía se desvía de las campañas y en su lugar se gasta en manejar individuos
hambrientos de poder.

***

Es peligroso romantizar el anonimato como virtuoso por sí mismo. El anonimato
en línea, combinado con actrices de mala fe - abusadoras patológicas,
criminales, y hordas colectivas de troles - hace posibles comportamientos con
consecuencias espantosas y a veces realmente aterradoras. El anonimato puede
ayudar y e incitar a la crueldad tanto como puede engendrar fines morales y
políticos más nobles - depende del contexto. Hacer un balance de la historia
completa de Anonymous ilustra esta dualidad. Antes de 2008, el nombre Anonymous
había sido usado casi de forma exclusiva para el propósito del «trolling» en
internet - una práctica que con frecuencia significa apuntar a personas y
organizaciones para acoso, profanar reputaciones y revelar información
humillante o personal. Habiendo sido apuntada en 2010 por un ataque de troles
(por suerte fallido), estaba emocionada - aunque bastante sorprendida - del
dramático proceso de conversión que experimentó Anonymous entre 2008 y 2010
conforme empezaron a trolear a los poderosos, eventualmente combinando la
práctica con vocabularios y repertorios de protesta y disidencia más
tradicionales.

Conforme se separaron de troles puros, lo que quedó igual fue el compromiso al
anonimato, usado para fines diferentes en circunstancias diferentes. Aún así,
una cantidad de operaciones de Anonymous que sirvieron al interés público, tales
como el vertimiento en masa de correos electrónicos que violaron la privacidad
de las personas, fueron llevados a cabo de forma imperfecta y son dignos de
condena. Estas operaciones imperfectas no deberían anular los aspectos
positivos que el grupo logró a través del anonimato, pero no obstante deberían
ser criticados por sus violaciones a la privacidad y usados como ejemplos para
mejorar sus métodos.

Prevenir que el estado erradique el anonimato requiere fuertes razones por su
rol esencial en salvaguardar la democracia. Al defender el anonimato, es
difícil simplemente argumentar, mucho menos probar, que el bien que posibilita
pesa más que sus daños, ya que es difícil hacer recuento del resultado social del anonimato. A pesar de las dificultades de medición, la historia ha mostrado
que los estados-nación con poder de vigilancia sin control derivan hacia el
despotismo y totalitarismo. Las ciudadanas bajo vigilancia, o simplemente bajo
la amenaza de vigilancia, viven con miedo de retribución y están desalentadas
para decir lo que piensan, organizarse y romper la ley individualmente, de
formas que mantengan a los estados y corporaciones responsables de sus actos.

Sin equivocación, defender el anonimato de tal forma no hace aceptables todos
los usos del anonimato por parte de las ciudadanas. Cuando evaluamos la vida
social del anonimato, una debe también hacer una serie de preguntas:
¿Qué es la acción anónima? ¿Cuáles personas, causas o movimientos sociales
están siendo ayudadas? ¿Es hacia arriba o hacia abajo? Todos estos factores
clarifican lo que está en juego y las consecuencias de usar el escudo del
anonimato. Invita a soluciones para mitigar algunos de sus daños en lugar de
demandar la eliminación del anonimato totalmente. Las tecnólogas pueden
rediseñar plataformas digitales para prevenir abuso, por ejemplo, habilitando
el reporte de cuentas ofensivas. Reconocer el mal uso del anonimato es la
razón por la que también aseguramos la aplicación de la ley con capacidad
limitada para desanonimizar aquellas que están usando una cubierta para
actividades que la sociedad considera inadmisibles, como pornografía infantil.
En su forma actual, el estado dirige enormes recursos, en la forma de dinero,
tecnología y legitimidad, para una aplicación de la ley efectiva. Además de
esto, llamar a acabar el cifrado fuerte, agregar puertas traseras para el
acceso gubernamental, o prohibir las herramientas de anonimato - algo que el
FBI hace con frecuencia - es llamar a la eliminación inaceptable de muchos
usos legítimos del anonimato.

A pesar de estas justificaciones, es difícil defender el anonimato cuando
algunas personas sólo tienen un rudimentario sentido de la conexión del
anonimato con los procesos democráticos, o que del todo no ven necesidad para
el anonimato, y otras lo ven como sólo un imán para formas depravadas de
criminalidad, cobardía y crueldad. Recientemente me recordaron de este preciso
punto luego de encontrarme con una de mis anteriores estudiantes mientras
viajaba. Sorprendida de reconocerme en el grupo con el que estaba a punto de
bucear, ella con regocijo me identificó por el sujeto de estudio: «¡Usted es
la profesora jáquer!» Unas horas después, cuando salíamos de un pequeño bote,
ella me preguntó de forma espontánea que le recordara mis argumentos en contra
de la común desestimación de la privacidad y el anonimato por razones de que
quien habla «no tiene nada que esconder». Yo solté una risita, ya que mi mente
estaba ocupada con estas mismas preguntas porque estaba preocupada por este
artículo y recité algunos argumentos aquí explorados. No estoy segura si los
argumentos precisos se le olvidaron porque pasaron años, porque mi lección
fue aburrida, o porque los méritos del anonimato son contraintuitivos para
muchas; es probable que haya sido una combinación de las tres. De cualquier
forma, estaba contenta de que ella tan siquiera tuviera la pregunta en su
mente.

Fue un recordatorio de que, en un momento en el que los ejemplos de actrices
anónimas trabajando por el bien no están fácilmente disponibles en las
noticias, tal como lo fueron durante los días de Anonymous, aquellas de
nosotras tratando de salvar la reputación del anonimato debemos presentar
historias convincentes de bien moral posibilitadas por el anonimato, en lugar
de explorarlo sólo como un concepto abstracto, justo por si mismo,
independiente del contexto. Anonymous permanece como un caso de estudio
ejemplar con ese objetivo. Aparte de usar el escudo para la acción directa y
disidencia, para buscar la verdad y transparencia, Anonymous también ha
brindado una zona en la que la recalibración del crédito y la atribución no
sólo han sido discutidas sino realmente promulgadas. Al hacerlo, Anonymous
brindó asilo de la necesidad incesante de disputa por la atención personal,
haciéndose notorias a la vez que atenuaban la celebridad individual, y aún así
lograron combatir la injusticia con espectáculo, mientras se mantenían anónimas
como una sola.

***

## Direcciones Tor

Datos para enviar información anónima:

* *The New Yorker*: icpozbs6r6yrwt67.onion
* *The Guardian*: 33y6fjyhs3phzfjj.onion
* *The New York Times*: nytips4bmquxfzw.onion
* *The Intercept*: intrcept32ncblef.onion
