# La economía de la desconfianza *por Ethan Zuckerman*

(ensayo)

> La duda viene a un alto precio estos días

Facebook tiene un problema de confianza.
Así como lo tenemos todas nosotras.

El domingo 25 de marzo de 2018, la masiva compañía de redes sociales hizo un anuncio de toda una página en los periódicos de los Estados Unidos y el Reino Unido.
El anuncio se disculpaba por los pasos equivocados que Facebook hizo al dar datos de usuarias a Cambridge Analytica, la turbia empresa de consultoría política que trabajó en la campaña de Trump de 2016 y en el apoyo al voto de Brexit de Reino Unido.

La disculpa de Zuckerberg iniciaba bien:
«Tenemos la responsabilidad de proteger su información.
Si no podemos, no la merecemos».
Pero rápidamente se hunde cuando Zuckerberg habla de problemas de confianza.
«Tal vez hayan oído de una aplicación de preguntas construida por un investigador universitario que filtró datos de Facebook de millones de personas en 2014.
Eso fue una violación a la confianza, y siento que no hayamos hecho más en ese momento».

Esas son un par de oraciones cuidadosamente preparadas.
La prueba de personalidad a la que se refiere fue desarrollada por el investigador Dr. Aleksandr Kogan, quien pudo accesar datos de decenas de millones de usuarias de Facebook a través de la aplicación.
En entrevistas, Zuckerberg es claro en que él ve la violación de confianza como la venta de esos datos de Kogan a Cambridge Analytica para que pudieran ser usados para construir perfiles de votantes estadounidenses.

El acto de Kogan fue claramente poco ético, aunque los perfiles de personalidad de Cambridge Analytica son más una pomada canaria que la súper arma psicológica que Steve Bannon esperaba que fueran.
Pero el que Zuckerberg le pase la culpa a Kogan es poco sincero.
Los datos que Kogan recolectó eran datos que Facebook le estaba dando a todas las desarrolladoras de aplicaciones por cerca de cinco años.
Aunque usted fuera una investigadora académica con motivos ocultos, una ambiciosa vendedora o alguien como Ian Bogost, quien creó el sorpresivamente popular juego viral Cow Clicker, diseñado como una sátira de los juegos en línea, a usted le daban como premio una profunda acumulación de información no sólo acerca de las personas que escogieron usar su aplicación, sino también de sus amistades.
Es esta decisión de diseño que debería causarnos poner en duda la confianza que estamos dando a Zuckerberg y sus compatriotas.

Por supuesto, esto es más grande que Facebook.
La abrumadora mayoría de contenido y servicios en internet están soportados por un único modelo:
publicidad enfocada basada en datos de comportamiento y psicográficos.
Este modelo tiene un beneficio notable.
Permite a las proveedoras en línea ganar grandes audiencias al ofrecer sus mercancías de gratis.
Pero tiene masivos inconvenientes.
Los sitios web están en guerra consigo mismos, esperando mantener a sus lectoras en sus sitios, al mismo tiempo que necesitan desviarlas hacia los sitios de sus anunciantes para hacer dinero.
El incentivo de recolectar cualquier dato concebible acerca de una usuaria o su comportamiento es la esperanza de ganar una ventaja al enfocar anuncios hacia ella.
Comprar y vender estos datos de comportamiento se vuelve parte del modelo de negocio de estos sitios.
En el proceso, las usuarias se acostumbran a la idea de que están bajo vigilancia continua.

Un mundo en el que estamos siendo constantemente observadas es uno en el que aprendemos, momento a momento, a desconfiar.
Sabemos que Facebook  nos está explotando, sabemos que nuestra atención está siendo vendida a la mejor postora, y sabemos que incluso si nos decidimos salir del sistema eso no sería un escape, ya que Facebook mantiene perfiles incluso de no usuarias.
Sabemos que Facebook nos fallará de nuevo, entregando datos a gente en la que nunca hubiésemos escogido confiar en persona.
Las revelaciones de las debilidades de Facebook nos llegan menos como un choque que como parte de una realidad decepcionante a la que todas nos hemos acostumbrado, en la que somos forzadas a confiar en instituciones que raras veces merecen o recompensan la fe que ponemos en ellas.

Una respuesta lógica a esta ola de decepción sería desconfiar de todas las agregadoras de datos, todos los sistemas grandes, centralizados, y movernos hacia un mundo en el que no confiamos en nadie.
Esa visión, convincente en la superficie, es más difícil de lograr de lo que parece.

La internet fue construida de una forma que hace posible participar en discusiones en línea sin poner una confianza significativa en terceros, siempre y cuando se esté dispuesta a hacer un poco de trabajo.
Considere el correo electrónico.
Yo uso Gmail para enviar, recibir y leer mi correo electrónico, lo que significa que debo confiarle a la compañía algunos de los más íntimos detalles de mi vida personal y profesional.
Google tiene acceso a lo que mi empleadora me paga, cuando mi jefa me grita, y los detalles de mi divorcio.

Ustedes podrían no estar dispuestas a hacer los compromisos de privacidad que yo he hecho.
Afortunadamente, pueden montar su propio sistema de correo electrónico.
Primero, sencillamente registren un nombre de dominio y configuren apropiadamente los registros DNS y MX para apuntar a la máquina Linux que acaban de preparar.
Ahora descarguen e instalen Postfix para tener un servidor funcional de sendmail, luego escojan un servidor IMAP para instalar y configurar.
También necesitaran configurar un cliente de correo para trabajar con ese servidor IMAP.
Asegúrense de mantener todas estas herramientas parcheadas y actualizadas, así como de descargar regularmente las listas negras para que puedan tratar de mantener sus buzones de entrada libres de spam.

Esto tomará la mayor parte de un día si son experimentadas administradoras de sistemas Unix.
Si no lo son, ya estarán cerca de ser sysadmin para cuando terminen.
Esto es probablemente la razón por la que cuando buscan «cómo ejecutar su propio servidor de correo electrónico», el segundo resultado es un artículo titulado «Por qué no debería tratar de hospedar su propio correo electrónico».

Ah, e incluso si hacen todo esto, Google aún recibirá una copia de los mensajes que me envíen, porque yo decidí confiar en Gmail.
Entonces si de verdad quieren mantener su correspondencia sin cifrar fuera de las manos de compañías fisgonas, necesitan convencer a sus amistades de tomar los pasos que ustedes acaban de tomar.

(Probablemente sea mejor sólo usar Signal, una aplicación que usa cifrado poderoso para proteger sus conversaciones y llamadas.
Pero por supuesto van a tener que asegurarse de que Open Whisper Systems, quienes hacen la aplicación, no tengan puertas traseras en el código que les de acceso a su correo a los empleados.
Hablando de código, leyeron todo el código de Postfix y de su servidor IMAP para asegurarse de que no hubieran puertas traseras en ese sistema de correo electrónico enrollado a mano de ustedes, ¿cierto?
Ustedes se preocupan por la privacidad, ¿cierto?)

Para la mayoría de personas - incluyendo muchas personas tecnológicamente sofisticadas - confiar en Google al final tiene más sentido que hacer el trabajo de administrar sus propias herramientas de comunicación.
No es de sorprender que las sysadmins de Google sean mejores en seguridad que la mayoría de nosotras.
Lo que es cierto para el correo electrónico es por lo menos igual de cierto para redes sociales.
Es posible administrar la propia alternativa a Facebook, o por lo menos a Twitter - Mastodon ofrece una alternativa completa al servicio de mensajería con limites de caracteres - pero requiere tiempo, conocimiento, y la cooperación de la gente con la que se quiera comunicar.
La alternativa es conveniencia y facilidad, al costo de poner su confianza en una compañía que es poco probable que responda a sus quejas.

La confianza que ponemos en Facebook, Google y otras gigantes de internet puede ser - y probablemente lo será - violada, ya sea de forma intencional, como ha hecho Facebook al compartir datos sensibles con socias que construyen aplicaciones, o sin intención, en una violación de datos.
Mientras estas compañías hablan de datos como un bien preciado, como el petróleo, se comportan más como si fuera basura tóxica cuando se fugan o derraman.

Incluso cuando las compañías trabajan para proteger nuestros datos y usarlos de forma ética, confiar en una plataforma le da a esa institución control sobre nuestra expresión.
Quien sea que esté en control de estas plataformas, o en el centro de una red de comunicaciones, puede controlar lo que decimos y a quién se lo decimos.
Las plataformas rutinariamente bloquean las expresiones que ven contrarias a sus términos de servicio, ya sea porque promueven el odio o la violencia, son degradantes o acosadoras, o de alguna otra forma violan las normas de la comunidad.
Algunas veces esta censura es visible, y usuarias son bloqueadas de publicar ciertas expresiones que violan los términos de servicio; otras veces puede ser más sutil, cuando la expresión es sencillamente «depriorizada», enterrada profundo en los canales compartidos con otras usuarias.
El canal de noticias de Facebook, que favorece historias que los algoritmos piensan que nos interesarán, escoge cientos de historias al día que probablemente nunca veremos, deseleccionadas por razones que no se nos permite conocer.
Probablemente queramos y necesitemos algún tipo de moderación para permitir que operen comunidades sanas, ya que muchos espacios en línea descienden en odiosa anarquía sin atento cuidado.
Pero confiar en una plataforma significa confiar en esta para decidir cuándo una no puede hablar.

Al final, la confianza lleva a la centralización.
Es difícil abandonar Facebook precisamente porque tantas personas han decidido confiar sus datos a Zuckerberg y su compañía.
Plataformas como Facebook se benefician de los efectos de red:
se vuelven cada vez más más valiosas conforme más personas las usan, porque se pueden conectar con más personas.
Y porque a compañías como Google y Facebook no les da pena tragarse a sus competidoras (y porque a las reguladoras de Estados Unidos les da bastante pena intervenir), las compañías en las que más confiamos rápidamente se pueden convertir en casi monopolios en los que nos vemos obligadas a confiar, aunque sea sólo porque han eliminado a sus más efectivas competidoras.

Una respuesta a sistemas en los que no confiamos y que no deberíamos confiar es construir un nuevo sistema que obvie la confianza.
Miles de personas y millones de dolares están ahora participando en un experimento de un sistema «sin confianza»: la cadena de bloques («blockchain»).
Como Satoshi Nakamoto, el aún sin identificar y probablemente seudónimo creador de Bitcoin y la cadena de bloques, explicó en un artículo anterior, «La raíz del problema con la moneda convencional es toda la confianza que se requiere para hacerla funcionar.
Se debe confiar en que el banco central no devaluará la moneda, pero la historia de las monedas fíat está llena de violaciones a esa confianza».
Nakamoto estaba preocupado acerca de la confianza no sólo en los bancos centrales sino también en los bancos comerciales y las proveedoras de transacciones como Visa:
«El comercio en internet ha llegado a depender casi de forma exclusiva en instituciones financieras que sirven como terceras partes confiables para procesar pagos electrónicos.
Aunque el sistema funciona suficientemente bien para la mayoría de transacciones, aún sufre de las debilidades inherentes del modelo basado en confianza».

Usando algo de matemáticas muy brillantes, Nakamoto construyó un sistema que elimina el requisito de una tercera parte confiable para asegurar que el dinero digital no sea «gastado dos veces».
A diferencia de una ficha física (una moneda), un bitcoin no es nada más que una cadena de números, y la única cosa que le impide gastarlo múltiples veces (y engañar al sistema) es alguien vigilando para asegurar que cuando Alicia le da a Bob una moneda, su cuenta sea debitada y la de él sea acreditada.
En lugar de preguntar a Visa, un banco, o algun otro tercero que maneje estas transacciones, Nakamoto creó un libro de cuentas distribuido, una tabla de todas las transacciones públicamente revisable.
Esta tabla es verificada por miles de computadoras, propiedad de diferentes grupos, que compiten por verificar el trabajo tan rápido como sea posible, ya que la ganadora recibe un bitcoin recién acuñado a cambio de sus esfuerzos.

Bitcoin funciona, más o menos.
Fue propuesto inicialmente como un método para conducir micro transacciones en línea, pagos de pocos centavos o menos, que no eran económicamente posibles de conducir sobre sistemas como Visa.
Pero el sistema probó ser demasiado lento y demasiado costoso como para hacer que esas transacciones sean posibles en el presente.
Sus partidarias entonces esperaron que serviría como un «almacén de valor», un bien que puede ser almacenado y recuperado en una fecha posterior con la expectativa de que continuará siendo valioso.
Pero Bitcoin ha probado ser inmensamente más volátil que los almacenes de valor tradicionales como el oro, llevando a que la gente compre muy alto y pierda hasta la camisa.
La novedad del mercado significa que está lleno de inversionistas ingenuas y especuladoras de dinero - presa fácil para ladronas, que han tenido éxito al jaquear en los sitios de intercambio de bitcoin succionando millones de dólares en dinero real.

La forma real en la que Bitcoin - junto con todas las cadenas de bloques contemporáneas - es defectuoso es que es increíblemente costoso en términos de electricidad y tiempo computacional.
Esto es por diseño.
Bitcoin depende de una «prueba de trabajo» para validar las transacciones existentes - esencialmente, miles de computadoras tienen que ejecutar millones de intentos para resolver un problema matemático antes que las demás, para verificar un conjunto de transacciones.
Si fuese fácil resolver estos problemas, sería sencillo inyectar información falsa en el libro de cuentas público, dándose a uno mismo monedas a las que no tiene derecho.
Para evitar el problema de confiar en una entidad central, Bitcoin hace imposiblemente caro cometer fraude.

El resultado neto es que Bitcoin es ordenes de magnitud menos eficiente que un sistema centralizado como Visa.
Las gigantes granjas de computadoras que validan el libro contable distribuido y minan bitcoins en el proceso ahora se estima que consumen 0.27 por ciento de la electricidad del mundo, aproximadamente el mismo porcentaje que la nación de Colombia.
(Esta figura cambia todos los días, y es probable que sea más alta para cuando lean esto - el Bitcoin Energy Consumption Index tiene estimados actuales.)
Quienes impulsan Bitcoin dirán que el costo bajará una vez que un nuevo algoritmo de «prueba de interés» reemplace los costosos sistemas de prueba de trabajo actuales, o que el costo vale la pena para tener una moneda libre de confianza.
El primero podría eventualmente ser cierto, mientras que el segundo es en esencia una declaración de creencia religiosa.

Todo esto para decir que la desconfianza es inherentemente costosa.
Francis Fukuyama detalló esto en su libro *Trust*, en donde examinó las economías de comunidades de alta - y baja - confianza en Italia.
Fukuyama descubrió que en comunidades en las que la confianza a extrañas era baja, era muy difícil para las empresas crecer y expandirse.
Las razones son bastante simples: considere los retos de hacer negocios con alguien a quien nunca ha conocido contra hacer negocios con una socia de mucho tiempo.
Porque una confía en la socia, está dispuesta a enviarle bienes antes de que su cheque sea liquidado porque se confía en que cumplirá, acelerando todo el proceso.
Hacer transacciones con una extraña involucra múltiples, precavidos y finalmente costosos pasos en los que ambos lados verifican la buena fe de la otra parte antes de seguir.

Hacer transacciones en un mundo sin confianza es muy parecido a hacer negocios en los primeros días de eBay.
Conforme las personas se empezaron a vender en línea unas a otras dispensadores PEZ y computadoras personales, descubrieron una gran cantidad de formas de defraudarse entre ellas.
Entonces eBay se vio forzada a construir un sistema de reputación, rastreando reportes de participantes acerca de quién era confiable.
Luego construyeron un sistema de fideicomiso, brindando garantía al actuar como una tercera parte de confianza y asegurando que los artículos costosos hayan sido entregados antes de liberar los pagos.
Adquirieron y luego gastaron bastas sumas anuales en PayPal para poder diseñar una moneda que resistiera algunas de las formas más obvias de fraude en línea.
Pueden pensar que mucho del mercado de capitalización con múltiples miles de millones de dólares de eBay es como el valor de brindar confianza a lo que de otra forma sería un mercado sin confianza.

Es posible que la desconfianza sea similarmente costosa en nuestras vidas políticas, un concepto preocupante en un momento en el que la confianza en el gobierno y las instituciones sociales está llegando a bajos históricos.
En 1964, el 77 por ciento de las estadounidenses reportó que confiaba en que el gobierno hiciera lo correcto la mayoría de las veces.
Esa figura ahora es 18 por ciento.
Mientras el gobierno, y específicamente el Congreso, es sujeto de niveles muy bajos de confianza en los Estados Unidos, virtualmente todas las instituciones - iglesias, bancos, la prensa, el sistema médico, la policía - reciben significativamente menos confianza que en las décadas de 1970 y 1980.

Como resultado, es difícil para las instituciones ejercer el poder que alguna vez tuvieron.
Cuando las estadounidenses ven con nostalgia a la prosperidad y crecimiento del periodo después de la segunda guerra mundial, están volviendo a ver un momento en el que las personas confiaban en el gobierno para que construyera autopistas y puentes, soportara educaciones colegiales e hipotecas, y usara los poderes de tributación y gasto para construir bienes públicos y reducir la desigualdad.
La confianza en el gobierno que permitió tanto la autopista inter estatal y el éxito del movimiento de los derechos civiles ha sido reemplazada con un escepticismo penetrante de todas las grandes instituciones.

Donald Trump fue elegido por aprovechar esta desconfianza.
Luego de prometer aumentar la confiabilidad «secando el pantano», en lugar de eso él ha gobernado activamente buscando erosionar la confianza del electorado en las instituciones.
Su mantra de «noticias falsas» está diseñado no solo para dirigir a quienes le apoyan hacia fuentes de noticias amistosas con Trump como Fox News y Sinclair, sino que también para disminuir nuestra confianza en la habilidad de actrices independientes para revisar o controlar su poder.
Por lo menos así de peligrosa es la narrativa del «estado profundo» elevada en las esquinas de la Trumposfera y amplificada por los ataques del Presidente al FBI y otras instituciones gubernamentales.
Al postular una conspiración contra la presidencia liderada por partisanas remanentes de administraciones anteriores, Trump invita a sus partidarias a imaginar un mundo en el que cualquier persona que habla en contra de él es una conspiradora golpista, intentando socavar su poder para propósitos partisanos siniestros.

Sabemos que la corrosión de la confianza en instituciones hace más difícil para los gobiernos construir infraestructura que enlace a las personas y que fomente el crecimiento económico.
Sabemos que estamos perdiendo la confianza en distintas instituciones - la iglesia, el gobierno - que fomentan que trabajemos juntas hacia objetivos comunes.
Lo que no sabemos es qué nos hace como individuos este bajo nivel de confianza perpetuo.

Recibí un ejemplo ilustrativo el otro día, cuando una mujer me escribió preocupada porque el Servicio Postal de los Estados Unidos estaba censurando su correo.
(Escriban suficiente acerca de privacidad y vigilancia, y ustedes también recibirán estos correos electrónicos.)
Los libros que ella ordenó acerca de organización y derechos civiles nunca llegaron, lo que significa que el correo estaba monitoreando su correspondencia y previno que ella recibiera textos subversivos de Amazon.

Gasté un rato componiendo una respuesta para esta mujer.
Empezaba con la idea de que el experimento estadounidense de democracia estaba muy literalmente basado alrededor de la oficina de correos, que representaba más de tres cuartos de los trabajos del gobierno federal en 1831.
Escribí sobre cómo Benjamin Franklin imaginó una república de cartas, en la que la esfera pública de periódicos intercambiados libremente permitía a las ciudadanas de una nueva nación debatir ideas y gobernarse a sí mismas.
Subrayé las leyes que prevenían que el servicio postal interfiriera con el correo, las inspectoras postales que son responsables de hacer cumplir esas leyes, la supervisión del congreso hacia el servicio postal y las historias de prensa escritas acerca de los reportes generados por esas audiencias en el congreso.

Mientras estaba escribiendo, ella me escribió de nuevo diciéndome que había recibido su libro.
«Entonces tal vez no ocurrió esta vez.
¿Pero cómo podría una realmente saber si quisieran impedir que la información llegara?»

Esa es una pregunta sorprendentemente profunda.
Si confía en las instituciones - la oficina de correos, el Congreso, la prensa - hay un sistema predecible de supervisión que incluye actrices dentro y fuera del sistema, respaldadas por una larga historia de normas que desalientan fuertemente el tipo de comportamiento del que ella está preocupada.
Pero si no confía en las instituciones - digamos, si la cara más visible del gobierno está en la televisión todos los días diciéndole que tema una conspiración del estado profundo y que no confíe en los medios mentirosos - ¿cómo podría saber si ese libro perdido fue un error en la entrega o algo más siniestro?
Una vez que se ha perdido la confianza en las instituciones que están diseñadas para proteger y supervisar los sistemas en los que dependemos, ¿somos nosotras mismas responsables de auditar esas instituciones?
¿O simplemente debemos vivir con un bajo nivel de confianza perpetua hacia, bueno, todo?

No hay forma de verificar lo que Facebook está haciendo con nuestros datos.
En 2011, mucho antes de las revelaciones acerca de Cambridge Analytica, Facebook recibió un tirón de orejas de la Comisión de Comercio Federal por decirle a sus usuarias que podían mantener sus datos privados, y hacerlos accesibles a terceros.
Facebook estuvo de acuerdo con la lista de cargos - una sentencia convenida - certificando que se comportaría mejor en el futuro.
Cambridge Analytica - de la que sólo sabemos porque una informante dio un paso adelante - sugiere pocas razones para confiar a Facebook nuestra privacidad o a la Comisión nuestra supervisión.

Las defensoras de las seguridad digital diagnostican una peligrosa condición llamada «nihilismo de privacidad».
Ocurre cuando una entiende en cuántas formas su información e identidad pueden ser comprometidas en línea, y simplemente se rinde de protegerse.
Es una postura que puede parecer profundamente conocedora y de mundo, pero que  le deja más vulnerable ante la explotación que si hubiera tomado los pasos defensivos imperfectos pero sabios.
La militarización de la desconfianza en la política estadounidense sugiere una forma más profunda de nihilismo, una comprensión de que nuestros sistemas políticos son tan frágiles, tan dependientes de un conjunto de normas que previenen comportamientos escandalosos, tan sujetos de captura por partes que favorecen el partidismo sobre el compromiso, que los controles y balances pueden volverse sellos de hule rápidamente.

La solución a la confianza en los sistemas en línea no es eliminar la confianza por completo, como las cadenas de bloques buscan hacer.
Es construir sistemas que nos ayuden a confiar mejor y más sabiamente - el sistema de reputación de eBay ha brindado suficiente seguridad como para que vender nuestra basura vieja en línea sea ahora una rutina como lo es realizar una venta de garaje.
Airbnb está construido en la extraña noción de que podemos confiar en una extraña para que nos rente un cuarto por la noche, o para recibir una extraña en nuestro cuarto que sobra.
Cuando nos subimos a un Uber para dirigirnos al restaurante recomendado por Yelp, no estamos viviendo en un mundo libre de confianza, sino en uno en el que hemos aprendido a confiar profundamente en extrañas.

Estos sistemas están lejos de ser perfectos.
Pero funcionan lo suficientemente bien, la mayoría del tiempo, que generan un enorme valor para aquellas que los usan.
En un momento en el que muchos sistemas, desde la atención a la salud hasta el Congreso, parecen estar fallándonos, la pregunta podría no ser cómo recuperamos la confianza en sistemas rotos, sino cómo diseñamos nuevos sistemas que nos permitan cooperar y confiar entre nosotras.

Facebook podría no merecer nuestra confianza, y tal vez tampoco el gobierno de los Estados Unidos.
Pero responder con nihilismo es salir del campo de juego y conceder la victoria a aquellas que explotan la desconfianza.
Necesitamos aprovechar nuestra desconfianza, usarla como combustible.
Esto significa abandonar la costosa fantasía de un mundo sin confianza y hacer el trabajo duro de construir nuevos sistemas que merezcan nuestra confianza.
