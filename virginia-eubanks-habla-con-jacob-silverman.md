# Virginia Eubanks habla con Jacob Silverman

(entrevista)

> «Una vez que se está en el sistema, se está ahí por siempre.»

La autora Virginia Eubanks advierte lo que pasa cuando los algoritmos determinan quién obtiene ayuda - y quién no.

El nuevo libro de Virginia Eubanks, Automatizando la inequidad: Cómo herramientas tecnológicas de alto perfil vigilan y castigan a las personas pobres, es un estudio de las injusticias y lo absurdo que viene con la digitalización de múltiples programas de servicio social.
El tema podría parecer académico, incluso de poco valor, pero el logro del libro es mostrar que todas las personas, tristemente, vivimos en lo que Eubanks llama «el hospicio digital».
Como Eubanks muestra, las elecciones que hacemos como sociedad sobre cómo tratar a las personas pobres tiene amplios efectos que definen la cultura.
(Eubanks aprendió esto de primera mano cuando la emergencia médica de su compañere la hundió en una espiral burocrática de drenaje de dinero que incluyó ser acusada de fraude de seguro médico).
Los programas que ella critica afectan casi a todas las Estadounidenses de alguna forma, haciéndolas más merecedoras de mejoras, en especial en un momento en el que la derecha está tratando de desfinanciar la salud y otros programas sociales.

Tomando de su experiencia académica en estudios de mujeres y ciencias políticas, la atención de Eubanks va desde los beneficios de asistencia social en Indiana hasta la asistencia para vivienda en Los Ángeles y los servicios de protección a la infancia en Pittsburgh.
Invariablemente, estos programas privilegian a los programas computacionales sobre la intervención humana (y la empatía humana), criminalizan la pobreza, y hacen que los beneficios sean más difíciles de obtener.
También hay instancias de corrupción corporativa, incompetencia, y mera ceguera ante las necesidades reales de las personas trabajadoras y marginalizadas.

«Las tecnologías para la gestión de la pobreza no son neutras», argumenta Eubanks.
«Están formadas por el miedo de nuestra nación a la inseguridad económica y el odio a las personas pobres; que a su vez forman las políticas y la experiencia de la pobreza».

En el curso de su libro, Eubanks muestra que estas tecnologías de automatización - junto con las suposiciones políticas que las apuntalan - tienden a dañar a las personas pobres más de lo que ayudan.
Al establecer una nueva forma de vigilancia basada en datos, una en la que las personas receptoras de asistencia social están bajo constante escrutinio y pueden ser penalizadas por absolutamente cualquier contacto con el sistema, estos sistemas despojan a las personas trabajadoras de su privacidad y autonomía.
Ejercen un control punitivo sobre las personas que se supone deben apoyar, y aseguran que el precio de acceder a los servicios básicos es la entrega completa de sus datos personales.

Recientemente hablé con Eubanks por teléfono acerca del hospicio digital, la criminalización de la pobreza y cómo podríamos luchar contra estos sistemas deshumanizantes.
Nuestra entrevista fue ligeramente condensada y editada por claridad.

**Jacob Silverman**: Usted se ha estado aventurando aquí en un territorio muy nuevo.
¿Cómo describiría su «beat»?

**Virginia Eubanks**: Diría que escribo sobre tecnología y pobreza, y una de las cosas que es interesante al describirlo de esta forma es que la gente tiende a asumir inmediatamente que estoy hablando de la falta de acceso a las herramientas más recientes de la revolución tecnológica para las personas pobres y trabajadoras.
Por veinte años, mi trabajo ha señalado lo contrario: que las comunidades trabajadoras pobres tienden a ser el objetivo de algunas de las innovaciones tecnológicas más nuevas - y en el trabajo reciente que he hecho -más invasivas y punitivas.

Entonces, parte de mi trabajo es ayudar a que las personas entiendan que los problemas de justicia social alrededor de la tecnología van más allá del acceso, van más allá de lo que en el pasado se hubiera llamado la brecha digital.
Aunque el acceso sigue siendo realmente importante, hay todo un mundo de otros problemas específicamente alrededor de la focalización y la vigilancia, que tenemos que mantener en la agenda de la justicia social.

**JS**: He visto algunos de sus comentarios acerca de querer ir más allá de la definición tradicional de privacidad para hablar sobre autonomía o agencia personal.
¿Puede hablar de a qué se refiere con esto?

**VE**: La razón por la que estoy tratando de empujar un poco estas ideas de privacidad es que para las personas a las que les hablo más - personas sin casa, personas que están en asistencia pública, personas que están siendo investigadas o que han tenido algún tipo de contacto con los sistemas de asistencia infantil en este país - la privacidad simplemente no es lo primero que surge en su lista de preocupaciones.
Es algo que a las personas les importa, pero realmente ni siquiera está entre las cinco más altas.
Lo que las personas me están diciendo es que en sus vidas, la expectativa de privacidad está muy influenciada por la cantidad de poder económico o político que tienen las personas.
En el sistema de asistencia pública, por ejemplo, sencillamente no hay expectativa de privacidad.
El legado histórico de la asistencia pública en los Estados Unidos es que las personas cambian algunos de sus derechos humanos básicos, incluyendo su derecho a la privacidad y su derecho a contar la historia en su propia forma, por acceso a beneficios públicos.
Esto es parte del contexto por el que las personas entran en contacto con estos sistemas, y yo creo que es injusto.

Entonces en realidad encuentro que, cuando la mayoría de personas hablan acerca de lo que quieren, lo que quieren es autodeterminación.
Quieren la posibilidad de tomar decisiones por ellas mismas, acerca de las cosas que más impactan sus vidas, como la forma en la que gastan su dinero, el lugar donde viven, si su familia permanece unidad.
Esas son cosas que las personas quieren auto determinar.
La privacidad podría ser parte de eso, pero no es necesariamente una expectativa o una prioridad para las personas con las que hablo.

**JS**: ¿Usted aún usaría el término «vigilancia» al hablar sobre el sistema de asistencia social que recolecta enormes cantidades de información sobre las personas?
He notado que en su libro usa una forma del término «vigilancia basada en datos».
¿Describiría esto como vigilancia hacia las personas que solicitan estos servicios, o es algo más?

**VE**: No creo que usaría mucho la palabra _vigilancia_.
Una de las cosas que espero que logre mi trabajo es que pensemos de forma más expansiva sobre vigilancia.
Los procesos de vigilancia, de poner personas en cajas para que sean más manejables, son procesos que ocurren a través de todo tipo de servicios públicos diferentes, entonces si vemos los servicios de protección de la niñez, o los servicios de personas sin hogar, o la asistencia pública, hay procesos de vigilancia ocurriendo en todas estas áreas.
Entonces pienso que una de las razones por las que no siempre uso la palabra vigilancia primero es que tiene una tendencia a crear una imagen en la mente de las personas de una oficial de policía sentada en un carro al otro lado de la calle de su casa.
Uno, esto ya no es realmente como se ve la vigilancia, y dos, quiero invitar a las personas a ver estos procesos de vigilancia más allá de la policía.

No diría que _no_ es vigilancia.
Sólo no se si ese es el término más evocativo para lo que está ocurriendo.

**JS**: Una cosa que me llamó la atención del libro es la noción de que hay muchos datos siendo recolectados, y que una vez que se está en el sistema, se está ahí por siempre.
Si algo le ocurre a una persona y eso llama la atención de servicios sociales o de la policía, podrían terminar escarbando en su pasado y pidiendo explicación, por ejemplo, de alguna visita de servicios de protección de la niñez que ocurrió hace años porque un vecino enojado decidió llamarles.
¿Es esto algo diferente?
Este registro infinito y meticuloso.
¿O hay un linaje más largo de este tipo de prácticas?

**VE**: El punto de usar la metáfora del hospicio digital en el libro es que tenemos la tendencia a hablar sobre estas nuevas herramientas como si hubieran aparecido de la nada - el monolito de _2001: Odisea en el espacio_ que sólo llega del espacio.
El punto de usar esta metáfora es basar estas nuevas tecnologías en la historia, y aunque hablamos de estas como disruptivas, tienden a ser más una evolución que una revolución en los servicios públicos.
La clave de la historia de origen de estas herramientas, para mi, es el establecimiento de un hospicio real de ladrillos y cemento, que era una institución para encarcelar a las personas pobres.

El movimiento de los hospicios surgió en los Estados Unidos como respuesta a una crisis económica realmente grande - la depresión de 1819 - y específicamente para que las personas pobres se organizaran alrededor de sus derechos y su supervivencia en respuesta a esa depresión.
La lógica es básicamente que hay una distinción entre pobreza, que es la falta de recursos, y pauperismo, que es la dependencia de beneficios públicos.
Y esto debería sonar familiar: la idea entre las élites económicas es que el problema no es la pobreza, incluso en medio de una depresión masiva; el problema es la dependencia.

Entonces la solución que el hospicio ofrecía era que, para recibir ayuda, las personas tenían que acceder básicamente a entrar en esta prisión voluntaria - esta institución tan atemorizante y peligrosa.
Tenían que ceder un montón de sus libertades civiles, si las tenían, si eran hombres blancos.
El derecho a casarse, a votar.
Por lo general se les separaba de sus hijos e hijas.
Y con frecuencia arriesgaban su vida, porque la tasa de muerte en algunos hospicios era de 30 por ciento anualmente, entonces un tercio de las personas que entraban morían cada año.
Este es un momento de nuestra historia en el que como nación tomamos la decisión de que los servicios públicos deberían ser más un diagnóstico moral que una provisión universal de recursos; es más un barómetro moral, menos un piso universal.
Esta es la programación profunda que vemos salir en estos sistemas una y otra vez.

Y luego la pregunta sobre el registro eterno, específicamente si es algo nuevo, y de muchas formas no lo es.
Una de las historias de origen de este libro es que fui a buscar el año en que el sistema de asistencia social del estado de New York se hizo digital.
Creí que sería en los años 1980 o 1990, y de hecho terminé yendo mucho más atrás en los archivos del estado de New York, hasta los libros de registro de los hospicios del condado.
Hay mucha continuidad en estos sistemas, yendo hasta los 1800.
Pero esta práctica de recolectar cantidades masivas de información sobre las personas que están viviendo situaciones en las que sus derechos no están siendo necesariamente respetados: eso es una idea vieja.
La forma que está tomando ahora es bastante diferente.

La metáfora del hospicio digital no llega tan lejos, porque hay cosas acerca de los hospicios que no son posibles en los hospicios digitales.
Como, una de las cosas más interesantes acerca de los hospicios es que fue una institución integrada: personas viejas, jóvenes, de todas las razas, de todos los lugares, y literalmente durmiendo juntas en los mismos cuartos, y comiendo en la misma mesa.
Uno de los resultados del hospicio real del condado es que las personas en realidad encontraban alianza y solidaridad, mientras que el hospicio digital en realidad es acerca de separar y aislar unas personas de otras, porque parte de la forma en la que trabaja es vigilando nuestras redes, lo que hace que muchas personas se aíslen como respuesta.
El hospicio del condado de ladrillo y cemento tenía registros pero, con raras excepciones, eran registros en papel que eventualmente se deshacían en polvo.
Ahora, tenemos lo que es un potencial registro eterno.
Que no es decir que no es realmente difícil, como alguien que sabe algo acerca del archivo de registros digitales - no exageremos qué tan eternas son estas cosas.

**JS**: Usted escribe que todas vivimos en el hospicio digital.
¿Es esa una reflexión de la noción de que muchas personas pasan crisis de pobreza u otros tipos de precariedad y tienen que lidiar con estos sistemas, o hay alguna otra forma en la que todas vivimos bajo este sistema?

**VE**: Lo primero es que cuando digo personas pobres y de la clase trabajadora, en realidad es importante que la gente entienda que una de las narrativas contra las que estoy empujando es la idea de que la pobreza sólo ocurre para una pequeña minoría de personas probablemente patológicas.
La realidad es que en los Estados Unidos el 51 por ciento de nosotras nos hundiremos bajo la línea de la pobreza entre las edades de veinte y sesenta y cuatro, y un total de dos tercios de nosotras accederemos a la asistencia pública condicionada al nivel de recursos.
Y esto no es seguro social o almuerzos gratis en la escuela, es asistencia social.
Cuando digo personas pobres y trabajadoras, estoy hablando de la mayoría de las personas en los Estados Unidos, quienes en algún momento dependerán de estos sistemas.
Entonces, dos tercios de nosotras estarán en estos sistemas en una forma realmente directa.

Lo segundo que estoy tratando de señalar al decir «todas vivimos en un hospicio digital» es que siempre hemos vivido en un mundo que creamos para las personas pobres.
Los Estados Unidos es bastante excepcional porque no seguimos enfoques de derechos humanos para la provisión pública de bienes sociales compartidos.
Hemos creado este conjunto de programas que es costoso, punitivo, paternalista y con frecuencia criminalizante hacia personas que están sufriendo un choque o contratiempo económico o una enfermedad.

Cuando vemos la falta general de salud y bienestar en los Estados Unidos, todas vivimos esto porque hemos creado estos sistemas de salud que son difíciles de navegar, y que están basados en el libre mercado en lugar de en la provisión básica de derechos humanos, como atención médica.
Siempre hemos vivido en este mundo en el que aceptamos una cultura disfuncional porque queremos castigar personas pobres.

Y francamente, también tiene mucho que ver con raza y la historia racial en los Estados Unidos.
Parte de esto no es solo que no queremos que personas pobres en general tengan acceso a los servicios públicos en los Estados Unidos; personas estadounidenses blancas no quieren que las personas de color tengan acceso a los recursos públicos compartidos.
Entonces hemos creado esta cultura realmente punitiva alrededor de los servicios públicos, que impacta todas nuestras vidas en estas formas realmente profundas.
Los tipos de comunidades en los que vivimos, la clase de escuelas que tenemos, los tipos de atención médica que recibimos - todo está impactado por esta actitud punitiva que tenemos hacia la inequidad económica.

**JS**: Algo que realmente me pegó fue un comentario que hizo en el libro acerca de cómo buscar muchos de estos servicios es alabado de una forma u otra, considerado una marca de buen autocuidado, si se hace en el sector privado - servicios como tratamiento de drogas o terapia o programas después de la escuela.
Pero cuando se hacen a través de medios públicos, a través de asistencia social o servicios sociales, las personas son despreciadas de alguna forma.
¿Es esta una actitud de ver la pobreza en términos punitivos?

**VE**: Sí, en el libro digo, en específico cuando estoy hablando sobre la Allegheny Family Screening Tool, que es el modelo estadístico que se supone predice cuáles niños y niñas serán víctimas de abuso y negligencia en el futuro, que estos sistemas con frecuencia confunden la paternidad siendo pobres con pobre paternidad.
Para muchas familias pobres y de clase trabajadora, porque hemos limitado tanto el acceso a los beneficios de efectivo, estampillas de comida y la vivienda pública, el sistema de asistencia social es con frecuencia el proveedor de recursos de última opción.
Porque también pueden mover algunos hilos.
Como que si están siendo investigadas por negligencia infantil, la trabajadora del caso podría llevarles a una vivienda pública.
O, podrían apoyarse en que la persona casera haga reparaciones en la casa que, si se dejan sin atender, podrían resultar en la pérdida de las niñas por negligencia infantil.
Es tan solo esta enorme, horripilante ironía, que este es el sistema de última opción para muchas familias, pero que la única forma posible de involucrarse en esta es dándole al estado el poder de romper la familia.
Entonces, al solicitar estos recursos, las personas sienten que con frecuencia tienen que canjear su derecho a la integridad familiar.

Una de las mujeres con las que hablé en Pittsburgh, Allegheny County, dijo que ellos ayudarían, pero que primero es necesario rendirse.
Ella siente que deben poner a sus niñas en el sistema antes de que le ayuden.
Y esa orientación se extiende en el diseño de estos sistemas, entonces la Allegheny Family Screening Tool está construida en un almacén de datos diseñado en 1999 que ahora tiene mil millones de registros, que es ochocientos registros por cada individuo que vive en Allegheny County.
Pero por supuesto no recolecta datos de todos los individuos de Allegheny County; solo recolecta información de aquellas personas que están interactuando con servicios públicos.
Entonces, si se solicita apoyo para salud mental o adicción a un servicio del condado, se termina ahí.
Pero si se paga por apoyo para adicción o salud mental a través de un seguro privado, no se termina ahí.
Si se solicita ayuda a un servicio del condado para el cuido de niñas o una prórroga, se termina en el almacén de datos.
Si se puede pagar una niñera o una au pair, no.

Entonces esta estructura se construye bajo el supuesto que solicitar apoyo de los recursos públicos compartidos es una señal de paternidad riesgosa, cuando en realidad con frecuencia es una señal de que se está haciendo exactamente lo que es necesario.
Obtener el apoyo necesario para criar una familia feliz, saludable y segura.
Este es uno de mis grandes miedos, que estos sistemas tengan como objetivo a estas personas que en realidad están haciendo lo mejor que pueden para sostener a sus niñas y ser buenas madres, y que esto incluso podría resultar en que las familias no alcancen estos recursos que necesitan, aislándose cada vez más y carentes de esos servicios.
Según el CDC, la carencia de recursos y el aislamiento social son dos de los predictores claves del maltrato infantil.
Entonces, en realidad esto puede crear exactamente lo que más temen.

**JS**: Usted se refirió al hecho de que los sistemas sobre los que están construidos estos servicios con frecuencia pueden ser políticos en alguna forma.
También escribió en su libro que estos sistemas «se adelantan a la política».
¿Podría hablar un poco de cómo sucede esto? ¿Cómo la política es abstraída?

**VE**: Tendemos a creer que nuestras herramientas digitales de toma de decisiones son objetivas y neutrales.
Pero uno de los grandes argumentos del libro es que en realidad están todos estos supuestos morales y políticos incrustados en estas, pero que con frecuencia son difíciles de ver porque se discuten como cambios administrativos básicos, no como cambios de política.
Déjeme darle un ejemplo concreto de esto.
Uno de los casos de los que hablo en mi libro es este intento de automatizar todo sobre el proceso de elegibilidad para el sistema de asistencia pública en el estado de Indiana.
Esto es asistencia social en efectivo, Medicaid y lo que en ese momento se llamaba estampillas de comida y ahora se llama SNAP.
Y este sistema se basa en la creencia de que las relaciones personales entre las trabajadoras de los casos y las receptoras eran una invitación a la confabulación y el fraude.
Entonces, cuando el gobernador pasó por el estado hablando acerca del sistema y el contrato de $1400 millones que el estado firmó con IBM, en cada parada habló sobre este famoso caso en Indianápolis en el que una gran cantidad de trabajadoras de casos habían confabulado con las receptoras para defraudar al gobierno de, creo que eran como $8000.
Mencionó esto en cada parada de este tour.

Entonces, parte del diseño de este sistema fue romper la conexión entre trabajadoras de caso y las familias a las que servían, y que para Indiana eso se vio en la movilización de quinientas trabajadores de caso locales, que en el pasado habían sido responsables de una lista de familias, hacia centros privados de llamadas en los que se volvieron responsables, en lugar de familias, de una lista de tareas conforme caían en su sistema computacional.
Lo que esto significó fue que ninguna persona era responsable de un caso de inicio a fin, y cada vez que una beneficiaria llamaba a uno de estos centros de atención, hablaban con una trabajadora de caso diferente, entonces tenían que reiniciar cada vez.
Y el resultado de esto fue un millón de negaciones de beneficios en los primeros tres años del proyecto, que fue un 54 por ciento de incremento en las negaciones de tres años atrás.
Y la mayoría de personas recibieron una negación por esta razón que la computadora llamó «fracaso en la cooperación para establecer elegibilidad» - que básicamente consideraba cualquier error que cualquier persona hizo en el sistema, ya sea que alguien olvidó firmar la página 34 de esta aplicación realmente grande, o que alguien en el centro de atención cometió un error, o que alguien en el centro de escaneo cometió un error - todas estas fueron interpretadas como fracasos activos de cooperar de parte de la persona aplicando por los servicios.

Y esto tuvo un enorme impacto en las vidas diarias de las personas.
Personas a las que se les negaron incluían a Sophie Stipes, una niña de seis años con parálisis cerebral de un pueblo blanco rural de Indiana central - escribí sobre ella y su familia en el libro - y a Omega Young, una mujer afro americana de Evansville, Indiana, que perdió una llamada telefónica de recertificación porque estaba en el hospital sufriendo un cáncer terminal de ovario y fue expulsada de Medicaid por esto.
La lección aquí realmente es que hablamos de estos sistemas como simples mejoras administrativas, pero con frecuencia contiene decisiones políticas realmente importantes.
En Indiana, la decisión política fue que es más importante bloquear el acceso a recursos a personas que podrían no estar participando completamente en el proceso, en lugar de extender un poquito de ayuda extra para asegurarse que las personas que son muy muy vulnerables estén recibiendo los recursos a los que tienen derecho y para los que son legalmente elegibles.

Chris Holly, un abogado de Medicaid y una de las personas que entrevisté en Indiana, lo puso de una forma realmente bella cuando dijo que tenemos un sistema de justicia en el que supuestamente, por lo menos filosóficamente, es más importante para nosotras que diez personas culpables salgan libres a que una persona inocente cumpla tiempo en la cárcel.
Pero este sistema en Indiana está de cabeza.
Era más importante para el estado que una persona no elegible no reciba recurso a que diez personas elegibles sean negadas.
Y esto es un gran cambio político - esto es una decisión política.
Con frecuencia escondemos estos tipos de decisiones políticas tras una cubierta de estas supuestas tecnologías neutrales y objetivas.

**JS**: ¿Es aquí una solución contratar más trabajadoras de caso y confiarles más la toma de decisiones? Estaba escuchando una entrevista que le hicieron en NPR en la que admitió que, ey, siempre han habido sesgos y problemas con el trabajo de estas personas, pero parece que por lo menos brindaban más flexibilidad y un toque humano.

**VE**: Estoy parafraseando a un buen amigo y científico político, Joe Soss, quien dijo que la discreción es como energía: nunca se crea ni se destruye, simplemente se mueve.
Una de las cosas en las que necesitamos mantener nuestros ojos es en que estas herramientas digitales en realidad no eliminan la discreción, solo la mueven.
En Indiana, le quitaron la discreción a las trabajadoras de caso y se la dieron a las compañías privadas que construyeron el sistema.
Esto movió la discreción de la línea del frente del trabajo de casos, trabajadoras públicas, y se lo dio a IBM y a ACS.
Y en realidad estamos rastreando cómo funcionan estos sistemas dentro del contexto del ataque a trabajadoras públicas.
Porque con frecuencia estos sistemas se racionalizan con el objetivo de eliminar el sesgo de la línea del frente de sistemas de servicio público.

Yo también tengo mucho mucho recelo de esta filosofía que dice que la toma de decisiones humana es algo opaca e imposible de conocer, y la toma de decisiones computarizada es justa y abierta y transparente.
La toma de decisiones computarizada no es tan transparente como parece - hay todo tipo de supuestos escondidos dentro de estos sistemas.
Y por otro lado, en realidad creo que podemos hablar de toma de decisiones humana, podemos tratar el sesgo en estos sistemas sin quitar la discreción y sin descalificar a trabajadoras de caso de la línea del frente.
Esto puede ser parte del desarrollo personal de las personas, que las trabajadoras describan cómo están tomando sus decisiones, y de dónde creen que sus supuestos están viniendo.
Creo que las trabajadoras de caso de la línea del frente tiene habilidades y conocimientos que son realmente importantes para que las personas obtengan acceso a los recursos que necesitan.
Tengo mucho recelo de los sistemas que están diseñados para quitar la discreción a las personas en el sistema quienes generalmente son en su mayoría de clase trabajadora, en su mayoría mujeres, y por lo general las más diversas de toda la fuerza de trabajo de los servicios públicos.

**JS**: ¿Cómo se organiza la gente contra estos sistemas, o por lo menos tratan de criticarlos y asegurar sus propios derechos?

**VE**: Creo que en realidad estamos tratando legítimamente de comprender eso ahora mismo.
Le puedo dar un ejemplo del libro.
En Indiana hubo una movilización exitosa de personas ordinarias contra el sistema, y es por eso que este contrato de diez años terminó luego del tercer año.
Ese sistema se revirtió.
Hubo una organización realmente increíble en el territorio en Indiana que inició con un proceso de reuniones en el ayuntamiento en las que las personas hablaron abiertamente acerca de sus experiencias tratando de mantener sus servicios públicos bajo este sistema.
En realidad empezó asegurándose que había espacio para que esa voces de personas que fueron más impactadas por este sistema pudieran compartir su experiencia.
Y eso se convirtió en una cadena de audiencias a lo largo del estado, que lanzaron un montón de giras de prensa en los que las periodistas hablaban con las personas que habían sido impactadas y crearon presión política, lo que llevo a una resistencia bipartidaria en contra del sistema.
Entonces definitivamente hay modelos en los que las personas han hecho este trabajo en el pasado y han tenido éxito.

Me gustaría ver más de este trabajo en el que estamos levantando las voces de esas personas que son impactadas de forma más directa por estos sistemas.
Y para mi, estas serían las personas en asistencia pública, personas sin hogar, personas interactuando con el sistema de justicia criminal, y personas que están interactuando con el sistema de asistencia infantil.
Creo que esos son muy buenos lugares para empezar, porque esos son sistemas con bajas expectativas de que los derechos de las personas sean protegidos, entonces es donde se encuentran algunos de los sistemas de políticas más abusivas e invasivas.

**JS**: ¿Ayuda en algo, como usted menciona en el libro, que estos sistemas nos afectan a todas en algún sentido?
Eso parece un lugar desde el que podemos hacer una causa común, y mientras estos sistemas afectan a las personas pobres de forma más extrema, parece que los datos de todas nosotras están siendo recolectados y siendo sujeto de sistemas de evaluación y clasificación.

**VE**: Empiezo el libro con una historia personal acerca de mi pareja siendo atacada y al final necesitando una cirugía reconstructiva bastante extensa.
Pasamos este tiempo increíblemente difícil juntas, pero uno de los momentos más difíciles para mi fue cuando nuestro seguro de vida fue suspendido.
Yo sospecho que fue suspendido porque estaban haciéndonos una investigación de fraude y suspendieron nuestros beneficios hasta que completaran la investigación.
Nunca lo sabré, nunca lo podré confirmar, porque la compañía de seguros dice que sólo fueron un par de dígitos faltantes en la base de datos.

La razón por la que empiezo con esta historia personal es para decir que no todas interactuamos con estos sistemas de la misma forma.
Mi familia no estaba atrapada con frecuencia en el sistema de Medicaid ni en el de servicios de protección infantil.
No hay ese tipo de escrutinio digital diario en mi vecindario.
Logramos prevalecer contra este intento de bloquear nuestro seguro de vida, entonces no todas experimentamos estos sistemas de la misma forma, pero cada vez más todas entramos en contacto con estos sistemas.
Y parte de mi objetivo al escribir este libro es ayudarnos a ver la forma en que nuestras experiencias se reflejan en otras.
Yo creo que este es un lugar en el que podemos empezar a construir solidaridad y alianza y poder - reconocer que aunque no todas somos iguales, aunque no todas somos impactadas de la misma forma, todas somos impactadas.

Y eso el que pasó en Indiana también.
Una de las razones por las que la organización fue tan exitosa fue que empezó a afectar a las abuelas y abuelos de las personas, que estaban recibiendo Medicaid en los asilos de ancianos, y que creó una resistencia realmente amplia contra este sistema porque parecía afectar a todas las personas.
Hay formas de ver similitudes en nuestra experiencia, pero, de nuevo, yo creo que es crucial empezar con las personas que enfrentan la peor parte, porque tiene la mayor información sobre los problemas, y el mayor interés en crear soluciones inteligentes.
