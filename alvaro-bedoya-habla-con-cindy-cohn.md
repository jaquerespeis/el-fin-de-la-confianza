# Álvaro Bedoya habla con Cindy Cohn

(entrevista)

> «No tengo nada que esconder» es otra forma de decir «tengo privilegio».

Álvaro Bedoya y Cindy Cohn examinan cómo la vigilancia masiva impacta de
forma desproporcionada a las comunidades de color.

Uno de los lugares que con frecuencia se pasan por alto, en el que podemos
vislumbrar el fin de la confianza, es la forma en la que el gobierno nos
trata, a su gente.

A lo largo de los últimos dieciocho años más o menos, el gobierno de los
Estados Unidos ha abandonado en gran medida la idea de que su gente puede
asociarse, comunicarse, o incluso mostrar sus caras en público sin que el
gobierno tenga acceso a esa información.
En lugar de esto, de forma local y nacional, vemos una serie de tácticas
gubernamentales - reconocimiento facial, lectores de placas de autos, acceso
a la columna vertebral de la internet, búsquedas en enormes bases de datos
de gigantes de la tecnología, acceso en tiempo real a grabaciones de cámaras
de seguridad privadas - que ampliamente, y en gran parte de forma
indiscriminada, hacen que nuestras vidas sean sujeto de la vigilancia
gubernamental.

Cuando estas técnicas son consideradas en el Congreso o en debates públicos
más amplios, la gente con frecuencia hace referencia a 1984 u otras piedras
angulares de nuestra cultura en las que todas son vigiladas por igual.
Sin embargo la realidad vivida es que los efectos de estas técnicas no son
distribuidos equitativamente a través de la sociedad.
La vigilancia, incluso la vigilancia masiva, ha sido y continúa siendo
dirigida desproporcionadamente a las personas de color, inmigrantes, y otros
miembros marginalizados de la sociedad.
También impacta desproporcionadamente a aquellas que están involucradas en política o activismo, quienes con frecuencia son las mismas personas.

Hablé con Álvaro Bedoya, Director Ejecutivo del Centro sobre Privacidad y Tecnología en Georgetown Law, sobre el impacto dispar y sobre cómo cualquier conversación acerca de privacidad está incompleta sin reconocer esta realidad.
También hablamos acerca de algunas formas de avanzar bajo la Constitución, y del hecho que Harriet Tubman fue básicamente imparable.

**Cindy Cohn**: Quiero hablar hoy un poco sobre el impacto dispar que los programas de vigilancia masiva tienen en las personas de color - programas como intervenir la columna vertebral de la internet, recolectar registros telefónicos, recolectar placas de autos y utilizar software de reconocimiento facial.
Yo se que usted ha pensado mucho sobre esto, Álvaro, entonces dígame qué piensa sobre estas cosas.

**Álvaro Bedoya**: Bueno, déjeme contarle sobre mi abuela, mi Abuelita Evita.
Mi abuela era de un pueblo en las tierras altas de Perú llamado Cajamarca.
Ella nació en 1914.
Creció en una casa vieja con un patio español rodeado de personas
centenarias.
En realidad, personas que vivieron noventa y nueve anos - no cien, ni noventa y ocho - noventa y nueve.
Ella también vivió noventa y nueve, su madre vivió noventa y nueve, y también su tía.
Evita se convirtió en maestra de escuela y luego eventualmente directora de una escuela primaria.
Como fue maestra por cuarenta o cincuenta años, ella había enseñado casi a la mitad del pueblo para cuando se retiró.
Tenía un pollo de mascota, no confiaba en los bancos.
Recuerdo ir a su casa cuando era pequeño y descubrir fajos de su difunta moneda peruana - yo creo que eran intis - debajo de los sartenes en la cocina.
Ella era salida de una novela de Gabriel García Márquez.

Como sea, después de que mi familia vino a los Estados Unidos en 1987, cada fin de semana, sin importar cuánto costara, nosotras llamábamos para ver cómo estaba Abuelita Evita.

Entonces - ¿por qué estoy hablando de ella? Porque es probable que, desde 1990 hasta el 2013, cada vez que llamábamos a Evita, la Drug Enforcement Administration creaba un registro en lo que hoy se reconoce como el precursor del programa de registro de llamadas de NSA.
Este programa básicamente rastrea todas las llamadas desde los Estados Unidos hacia un gran número de países, incluyendo eventualmente a la mayoría de Latinoamérica.

No fui sólo yo. Es probable que todas las demás inmigrantes llamaban a su familia en Latinoamérica en ese periodo.
Y esto no ocurrió a las personas que estaban llamando a sus abuelas en Topeka o Nueva York o donde sea.
Ocurrió a las inmigrantes llamando a su familia en el extranjero, en especial en Latinoamérica.

Creo que hay un problema cuando la gente habla sobre vigilancia y se enfoca en la parte «masiva» de la vigilancia, la parte en la que «todas están siendo vigiladas».
Cuando nos enfocamos sólo en el hecho de que «todas» son vigiladas, perdemos el hecho de que algunas personas son vigiladas un montón más que otras.
Y también perdemos el sentido de cuál es el valor de la privacidad en las comunidades particulares más afectadas por su ausencia.

El programa de registro de llamadas de la NSA, el programa doméstico de vigilancia más amplio y más invasivo admitido, podría decirse que fue probado como beta en inmigrantes Americanas.
Y eso es parte del patrón.
Pero si escuchan la forma en la que oficiales electos, o incluso los medios de comunicación, hablan sobre la vigilancia, ellos no hablan de esto.

**CC**: Eso es realmente acertado.
Cuando hablamos acerca de vigilancia masiva, es fácil olvidar que estamos hablando acerca de técnicas que tienen dos pasos fundamentales:
primero, una mirada a todo - ya sea a través de un empalme de internet o a través de un lector de placas que van pasando o a través de la cámara en el cuerpo de una policía.
Eso ya es bastante malo y por sí mismo puede impactar de forma desproporcionada a personas marginalizadas, ya que sabemos que están demasiado vigiladas.
Esto fue verdadero para la DEA también, cuando se enfocó en inmigrantes latinoamericanas llamando a sus abuelas y no en personas de Iowa como yo.

Pero es peor.
Hay un segundo paso, cuando los «objetivos» del gobierno son sacados del flujo de la masa.
En los casos de la NSA, alegan que esa lista es un secreto de estado.
Y para la DEA, en realidad aún no sabemos.
Entonces en el primer paso es la vigilancia a granel o masiva que puede ser apuntada de forma desproporcionada.
Y cuando llegamos al segundo, ahí también hay personas que son más afectadas que otras.
En ambos pasos aquellas afectadas son desproporcionadamente personas de color u otros grupos marginalizados, incluyendo, por supuesto, inmigrantes.
Al final del día hay personas cuyas vidas reales están siendo afectadas por esto más que otras y omitir esto es perder una pieza de lo que en realidad está pasando.

**AB**: Las personas que trabajan en estos problemas con frecuencia usan el peor de los escenarios en el que Estados Unidos se convierte en una sociedad de vigilancia distópica y todas las personas son sospechosas.
Bueno, yo creo que hay muchas comunidades que *ya* viven en una sociedad de vigilancia.
Muchas comunidades urbanas de clase trabajadora, principalmente afroamericanas, ya viven en una sociedad de vigilancia.
Vean Baltimore.
Hemos visto vigilancia aérea persistente, el uso de estos dispositivos llamados Stingrays que imitan torres celulares, reconocimiento facial - lo que sea.

Lo mismo es cierto para inmigrantes que no tienen documentos o que viven en familias con situación mixta.
Pueden ver muchas de estas tecnologías, que primero fueron desarrolladas para los campos de batalla de Iraq o Afganistán, ser utilizadas de la forma más agresiva para encontrar a estas inmigrantes.
Pero cuando vemos que dicen «vigilancia» en la prensa, típicamente no se refieren a estas personas.
Es usada para hablar de «todas».

Y, por cierto, esto no es algo nuevo.
Esto es algo muy viejo.
Si pensamos en un líder de derechos civiles, una persona líder de derechos civiles afroamericano del siglo veinte, es muy probable que le hayan vigilado en nombre de la seguridad nacional.
No fue sólo Martin Luther King: fue Withney Young, Fannie Lou Hamer, W.E.B Du Bois, Marcus Garvey, Muhammad Ali, Malcolm X.
La lista sigue y sigue.

**CC**: El programa COINTELPRO del FBI que funcionó desde 1956 hasta 1971 vigiló el movimiento de independencia de Puerto Rico, el Partido de Panteras Negras, a nacionalistas cubanas e irlandesas, junto con las activistas contra la guerra de Vietnam, el Partido Comunista, y el Partido de Socialistas Trabajadoras.
Y eso es sólo lo que conocemos.

Lo que es nuevo es que las técnicas de vigilancia modernas pueden recolectar un rango de información cada vez más amplio durante ese primer paso que mencioné.
El gobierno ahora también puede escudriñar y almacenar más datos que nunca antes.

Eso significa que a diferencia de todas esas otras situaciones, ahora es muy difícil esconderse en la multitud.
Y eso significa que corremos el riesgo de perder muchas de esas cosas que se hacen en ese relativo anonimato en la multitud.

El experimento de los Estados Unidos es un experimento de auto gobierno, pero inherente en este está la idea de que las personas pueden cambiar su gobierno.
Necesitamos poder tener una conversación que las líderes actuales del gobierno no quieren tener - acerca de cómo queremos votar para sacarles de sus oficinas o cambiar una ley injusta o una ley que está siendo aplicada de forma injusta.
Yo creo que estamos construyendo un mundo tecnológico en el que vamos a tener que ser más explícitas acerca de la importancia de esta conversación privada.
Cuando el país apenas estaba empezando, era más difícil tecnológicamente que un gobierno supiera lo que todas las personas - o incluso algunas en específico - estaban diciendo entre ellas, entonces de forma implícita todas tenían la capacidad de tener una conversación privada.

**AB**: Vea la tecnología de reconocimiento facial.
Esta es tecnología que le permite a la policía apuntar una cámara a un grupo de protestantes, escanear las caras de esas personas, y seleccionar, por nombre, quiénes están en esa multitud.
Esta fue usada en Rusia para nombrar y avergonzar públicamente a protestantes anti Putin, partidarias de Alexei Navalny.
Aquí en los Estados, en Baltimore, vimos que fue usada para identificar y arrestar a personas protestando por la muerte de Freddie Gray.
La policía tomó fotos que habían sido publicadas en redes sociales, escaneó esas caras, encontró personas que tenían ordenes de arresto pendientes, y luego las arrestó.
Esto es real.

La privacidad protege diferentes cosas para diferentes personas.
Si estoy luchando con mi salud mental, tal vez me permite obtener ayuda o sólo me permite saber que no estoy sola.
Si soy gay y estoy en una familia conservadora, tal vez me deja explorar mi sexualidad en línea.
Para ciertas comunidades, la privacidad es lo que les permite _sobrevivir_.
La privacidad es lo que les permite hacer lo que es correcto cuando lo que es correcto es ilegal.

Tome el ejemplo de Harriet Tubman.
Lo que ella estaba haciendo - liberar personas esclavizadas - era muy ilegal.
Y no sólo en la Confederación; era muy ilegal en los Estados Unidos antes de la Guerra Civil.
Para Harriet Tubman, fue su capacidad de evadir la captura, evadir la detención, lo que la llevó a hacer las cosas por las que la celebramos hoy.
Es lo que le permitió ir repetidas veces a la misma área, las mismas plantaciones - los lugares en los que ella era _más_ perseguida y buscada - y continuar liberando gente.
Lo hizo una y otra vez.
Antes de que construyamos un sistema que sea literalmente capaz de rastrear a todas las personas y a todo, creo que necesitamos preguntarnos si estamos construyendo un mundo sin redes subterráneas.

**CC**: Harriet Tubman era tanta fuerza.
Me pregunto si aquí estamos impidiendo a las fuerzas del futuro.

Una de las cosas que como abogada constitucional tomo un poquito a pecho es que, debajo de los cambios tecnológicos, hay un problema que los redactores de la Constitución y las Cortes Supremas del pasado en realidad entendieron bastante bien.
Ellos entendieron la necesidad de que la gente tenga conversaciones privadas porque, honestamente, así fue como nuestro país llegó a existir.
Entonces si podemos redactar las cosas de forma correcta, hay algunas partes de la Constitución que nos pueden ayudar.

Por ejemplo, la Primera Enmienda no sólo protege el derecho a la libre expresión - protege el derecho a la asamblea, y el derecho a la asociación.
Para mi, el derecho a la asociación es especialmente importante para que nosotras hablemos en relación con la gente de color porque fue usado en esta misma forma exactamente durante la era de los derechos civiles.
Hay un grupo de demandas legales que surgen de la organización de la NAACP en el Sur.
La principal es llamada _NAACP v. Alabama_, en la que el estado de Alabama está requiriendo que la NAACP entregue sus listas de membresía como una condición para operar en el estado.
La Corte Suprema derribó este requisito como una violación al derecho a la asociación de la Primera Enmienda.
La lógica fue bastante fácil: la corte reconoció que personas ordinarias de Alabama tendrían miedo de unirse a la NAACP si supieran que el estado de Alabama sería informado.
Recuerden, aquí estamos hablando acerca del Gobernador de Alabama George Wallace: extremadamente hostil hacia los derechos civiles y la NAACP.
La Corte Suprema dijo que por supuesto no es funcional requerir a una organización como NAACP que entregue sus listas de asociadas al gobierno.
Esta regla es necesaria en una sociedad que abraza la idea de que es la gente la que tiene el control sobre su gobierno, no lo opuesto.

Entonces, lo que está sucediendo en realidad es que nuestro gobierno está usando nuestros «metadatos» - a quién le hablamos, cuándo y con qué frecuencia, y desde dónde, y usándolo para mapear las asociaciones de las personas.
Le llaman «encadenamiento de contactos», lo que suena poco amenazador.
Pero si dejamos la terminología y hablamos acerca del hecho de que el gobierno está demandando el derecho a rastrear nuestras asociaciones, entonces el caso de _NAACP v Alabama_ puede ayudarnos a argumentar que la Primera Enmienda requiere que el gobierno cumpla una muy alta carga, algo que llamamos «escrutinio estricto», antes de poder hacerlo.
Esto requiere que el gobierno tenga tanto un objetivo muy importante y un ajuste muy estrecho entre la técnica utilizada - encadenamiento de contactos - y el objetivo.
Si aplica el escrutinio estricto, el gobierno no debería poder recolectar este tipo de datos masivos ya que impacta a una gran cantidad de personas no sospechosas.
El ajuste no es lo suficientemente estrecho.

Este es uno de mis objetivos principales en los siguientes años - ayudar a las Estadounidenses, incluyendo a las juezas Estadounidenses, a que vean que el rastreo digital de la asociación de las personas requiere del mismo tipo de análisis cuidadoso de la Primera Enmienda que la recolección de las listas de membresía de NAACP en Alabama requirió en los años 1950.
El uso de lectores de placas de autos para rastrear a personas que van a la iglesia o a la mezquita, o para rastrear las llamadas de niñas pequeños a sus abuelas en Perú, requiere este análisis cuidadoso sin importar el uso de palabras sanitizadas como «análisis de metadatos» y «encadenamiento de contactos».
Pero la buena noticia es que para aquellas que tenemos que ir a convencer a las juezas de cosas, para este asunto hay bases fuertes en la Constitución.

**AB**: Las hay. Dicho esto, hay mucho trabajo por hacer al respecto porque, por ejemplo, el caso de la NAACP tuvo que ver con listas preexistentes de membresía que creo que el gobierno estaba tratando de obtener de la NAACP.
Hoy en día, con reconocimiento facial, la policía en realidad puede crear estas listas fotografiando y escaneando las caras de las personas.
Recientemente, la Corte Suprema decidió que tenemos derecho a la privacidad de nuestros movimientos públicos por periodos más largos que un cierto intervalo.
La policía no nos puede rastrear por una semana sin obtener una orden.
Pero no sabemos cómo esa decisión aplicará cuando, digamos, la policía toma una foto de una persona en una protesta y luego escanea sus caras para identificarles.

Pero comparto un poco de su optimismo, porque creo que la gente lo entiende.
A veces, la gente no se enoja por las «cookies» u otros métodos de rastreo digital porque no es _real_ para ellas.
Pero una tecnología como reconocimiento facial, eso lo entienden.
Entienden que darle al gobierno la capacidad de rastrear sus caras es invasivo y francamente, sencillamente pavoroso.

Está en las personas llegar y hacer que su gobierno rinda cuentas y decir, «Ey, solo porque pudieron hacer algo en el pasado que estaba remotamente relacionado con lo que están haciendo ahora para rastrearnos no significa que pueden hacerlo ahora.
Es diferente.»
Entonces, por ejemplo, hay viejos casos en la corte que dicen que está bien tomar fotografías de protestantes.
Pero el reconocimiento facial es diferente a la fotografía.
Las personas necesitan argumentar eso y reconocerlo.

Entonces yo creo que hay esperanza, pero al mismo tiempo, aunque las personas actúen en esa esperanza hay un riesgo real de perder algunas de nuestras protecciones más viejas.
Para algunas personas, esa es una pregunta de libertades civiles.
Pero para otras, es una pregunta de supervivencia.

**CC**: Yo creo que hay una forma en la que las cosas que no son visibles para las personas que están en la cultura mayoritaria son muy muy presentes para las personas que no lo están, y es realmente importante cerrar esa brecha.
Esta todo eso del viejo tropo de «si no tiene nada que esconder, no debería tener nada de miedo por la vigilancia».
Con frecuencia trato de que las personas de la mayoría den un paso atrás y piensen no sólo acerca de ellas mismas sino acerca de todas las personas que conocen, todas las personas con las que podrían estar relacionadas, e incluso más amplio sobre el tipo de sociedad en la que quieren vivir.
¿Querrían vivir en una sociedad en la que el único gobierno que pueden tener es el que tienen ahora?
Todo movimiento social ha empezado con una conversación privada.
Para las abolicionistas y las sufragistas no siempre fue seguro reunirse en público; abortos e incluso algunas adopciones ocurrieron a través de redes de susurros.
Durante mi vida, hablar sobre personas gay que se casaran era algo que podría terminar en muerte.
Entonces esto no es sólo acerca del pasado.

Pero cada movimiento social que celebramos hoy empezó en un periodo en el que las conversaciones tenían que ocurrir en secreto, o por lo menos sin el gobierno de ese momento escuchando, antes de que se pudiera organizar lo suficiente y reunir a suficiente gente para cambiar las cosas.

También creo que, al ver cómo ocurre el cambio social, se necesitan sistemas que no sólo protejan a unas pocas personas valientes que están dispuestas a ir a la cárcel por sus creencias - eso es muy importante - pero también se necesitan sistemas que aseguren que muchas más personas puedan mostrar su apoyo.
Esto incluye gente que está ocupada criando a sus hijas, o que tienen dos trabajos, o que son vulnerables de otras formas.
Me preocupa que algunas veces, cuando nos enfocamos sólo en los grandes nombres heroicos de nuestros movimientos, perdemos la necesidad de proteger todo el camino para las pocas y las muchas.
Sin ese camino no es posible mover a la sociedad, ya sea que el movimiento venga a través de la aprobación de leyes, ganando elecciones, o asegurando victorias en la corte.

**AB**: Yo creo que «No tengo nada que esconder» es otra forma de decir «tengo privilegio», o «Soy una persona relativamente poderosa que viene del lado correcto del pueblo, que tiene opiniones políticas que no son consideradas radicales, que tiene el lujo de ser del género y orientación sexual correctos».
Necesitamos dejar de hablar acerca de privacidad porque es impreciso e indefinido.
Necesitamos reconocer que es un escudo para las vulnerables.
Privacidad es lo que permite que esas primeras y «peligrosas» conversaciones ocurran.

**CC**: Otra cosa de la quería pensar con usted es esta idea de que la privacidad es más una cosa de equipo, y cómo, especialmente cuando pensamos acerca de la vigilancia masiva, con frecuencia lo olvidamos.
En lugar de esto, todo se resume a «¿Tengo algo que esconder?», «¿Y yo qué?»

Pero enfocarse en individuos, y específicamente sólo nosotras como individuos, realmente pierde muchos de los mecanismos en los que la Constitución en realidad está pensando con el derecho a la asociación, asamblea, a la petición por quejas.

Aún así, lo que nos dicen las profesionales de mercadeo a nosotras que trabajamos en estos problemas es que si no se hace personal, a las personas no les va a importar.
Creo que una de las cosas fundamentales con las que lucho es cómo hablar acerca de algo que es en realidad acerca del colectivo en una sociedad en la que todo el mercadeo y el alboroto es acerca de mi, mi, mi.
Para muchas personas en la cultura de la mayoría, tienen razón de que no van a ser sujetas de las técnicas de vigilancia masiva - el lector de placas de autos, vigilancia de la columna vertebral de internet, cámaras de reconocimiento facial - el resultado de esas técnicas podría nunca ser girado contra ellas.

**AB**: Las leyes luchan con la privacidad como un derecho comunal.
Por ejemplo, en 1967, cuando la Corte Suprema presentó una prueba para resolver la definición de «búsqueda» en la Cuarta Enmienda, decidió que la prueba incluía una «expectativa razonable de privacidad».
Hace dos preguntas: primero, si usted tiene personalmente una expectativa de privacidad, y segundo, ¿es esta una expectativa que la sociedad está preparada para reconocer como razonable?
Si se piensa, es una prueba realmente rara porque, bajo esta, el gobierno podría ir alrededor del pueblo pegando afiches que digan «Estamos viendo a todas y oyendo todo» - y luego, en teoría, nadie tendría una expectativa de privacidad y el gobierno podría buscar todo.

Pero la Cuarta Enmienda no debería ser vencida tan fácilmente.
Es acerca de más que mi derecho individual de mantener al gobierno fuera de mis asuntos.
También es acerca de proteger cierto tipo de relación entre el gobierno y su gente.

¿Cómo se logra que suficientes personas defiendan el derecho comunal a la privacidad?
Esa es la pregunta del millón de dólares.
No se si nosotras como abogadas vamos a resolver esto.
Creo que esa pregunta será resuelta por artistas y escritoras y personas involucradas en la cultura, en lugar de mi, un abogado que ama nerdear sobre precedentes obscuros.

**CC**: Sí, y creo que eso es realmente importante.
Aunque quiero lanzar ahí que, en términos de esas cosas en las que la Cuarta Enmienda no funciona muy bien, la Primera Enmienda en realidad _está_ _tratando_ de hacer algunas.
Es una herramienta que debería ser capaz de hacer un poquito más.
Por ejemplo, en el caso de la búsqueda de dispositivos en las fronteras que EFF y ACLU están manejando llamado _Alasaad_, la corte recientemente reconoció que la búsqueda de los dispositivos de las personas en las fronteras, especialmente de periodistas, es un problema de la Primera Enmienda porque las búsquedas revelan todas las personas con las que han hablado.

Parte de la razón por la que estamos teniendo esta conversación en _McSweeney's_ es para intentar llegar a artistas, escritoras, y nuestra cultura más ampliamente.
Pero yo también pienso que hay herramientas en el lado legal.
La buena noticia, ya que quiero terminar con algo positivo, es que yo realmente creo que hay partes de la ley que están esperándonos para cuando estemos listas como sociedad para hablar sobre estas cosas.

**AB**: Amen. Terminemos con algo positivo. No algo locamente depresivo.
