# ¿Deberían las agencias policiales usar vigilancia?

(debate)

> Hamid Khan y Ken Montenegro de Stop LAPD Spying Coalition van cara a cara con el profesional de aplicación de la ley Myke Cole.

## Apertura

* A favor: Myke Cole.

Sí, la aplicación de la ley debería practicar vigilancia, pero necesita hacerlo con responsabilidad ante un público furioso que con razón no confía en la policía.

La vigilancia, tanto electrónica como física, es una herramienta crítica para la policía.
Habilidosos criminales tienen éxito precisamente porque han hecho un estudio de las capacidades de la policía, y trabajan para reducir la posibilidad de detección.
Las fuerzas policíacas faltas de personal no son capaces de estar en todos los lugares en que un crimen podría ocurrir.
La vigilancia enfocada en donde hay una sospecha razonable de actividad criminal puede ayudar a detener el crimen.

Pero esa es vigilancia usada de forma correcta.
Y ahora no está siendo usada de esa forma.
Desde el uso de Stingray en la ciudad de Nueva York, a la extralimitación del FBI buscando puertas traseras en dispositivos Apple, a la vigilancia xenófoba de musulmanas luego del 9/11, los policías han trabajando tiempo extra para degradar la confianza pública.
Con el telón de fondo de las matanzas policíacas arbitrarias de hombres negros desarmados a través del país, la policía ha malgastado la fe pública, trayendo duda a todas las prácticas policíacas, incluso  aquellas que son efectivas y necesarias.

La «vigilancia por consentimiento» de Sir Rober Peel imaginaba una policía ética que recibía poderes de un público cuya confianza trabajaban de forma asidua por mantener.
El Departamento de Policía de Nueva York cuenta con cerca de treinta mil oficiales uniformadas, suportadas por aproximadamente veinte mil civiles, para supervisar una ciudad que se acerca a los nueve millones.
Si nueve millones de personas no consienten con la supervisión realizada por cincuenta mil, no hay absolutamente nada que la policía pueda hacer.

El consentimiento Peeliano depende la compenetración entre la policía y el público, que se construye de dos formas:
(1) La policía se mantiene rindiendo cuentas, y es enjuiciada cuando viola la ley.
(2) La policía prueba que es efectiva haciendo las comunidades más seguras.
Para lograr la primera, necesitamos hacer públicamente que la policía rinda cuentas cuando matan personas negras desarmadas, se exceden en su autoridad, y participan en prácticas corruptas.
Para lograr la segunda, necesitamos cada herramienta en el arsenal del cumplimiento de la ley, incluyendo la vigilancia correcta y responsable, sin mancha por sesgos.

La solución no es abolir la vigilancia, sino vigilar de forma correcta  y responsable.
Entender que el mantenimiento del consentimiento Peeliano es crítico, y que vale la pena sacrificar algunas, más no todas, las ventajas en la lucha contra el crimen.
Un nuevo modelo es necesario, en el que un público involucrado se informa acerca de los tipos, el alcance, y también de los límites esperados de la vigilancia policial, y entiende por qué es necesario.
Y cuando la vigilancia se usa de forma incorrecta, un público enojado no debe ser topado por un muro azul de silencio, sino por un ajuste de cuentas que ve que aquellas responsables por el mal uso sean castigadas con rapidez y en público.

## Apertura

* En contra: Hamid Khan y Ken Montenegro

Preguntar si las agencias policiales deberían practicar vigilancia oculta la pregunta correcta: es como preguntar si soldados deberían usar armas químicas durante la guerra en lugar de preguntar por qué resolvemos diferencias con una guerra.
Si esquivamos estas preguntas sobre la historia y el rol de la vigilancia, terminamos en discusiones acerca de cuánto veneno es letal en lugar de empezar por interrogar por qué estamos tomando veneno.

La vigilancia policial es integral para la construcción de sistemas de conocimiento y estructuras de poder que preservan y sostienen la supremacía y el privilegio blanco.
Como Dr. Simone Browne subraya, la historia de la vigilancia es la historia de la anti negritud. La vigilancia es sólo otra práctica construida sobre el linaje de patrullas esclavistas, leyes linterna, Jim Crow, Escuadrones Rojos, la guerra contra las drogas, la guerra contra el crimen, la guerra contra pandillas, y ahora la guerra contra el terrorismo.
La vigilancia policial nunca ha sido aplicada de una manera neutral; siempre ha sido convertida en un arma contra aquellas en los márgenes y aquellas luchando por la liberación.

La pregunta correcta es si la vigilancia policial es una herramienta que respeta o niega la vida.
Es elemental investigar la historia de la vigilancia, cómo las prácticas de vigilancia han evolucionando, y quiénes han experimentado el mayor contacto con la policía.
También es importante demandar que los recursos que se gastan actualmente en crear vigilancia estatal que produce perjuicio sean redirigidos a entidades respetuosas de la vida como programas para la juventud, la salud mental, vivienda y educación, entre otros.

Una vez que hayamos nombrado la intención de causar perjuicio que está cocinada dentro de la vigilancia policial, nuestra siguiente línea de investigación debería ser si esta siquiera funciona.
De acuerdo con la Academia Nacional de las Ciencias, los intentos de prevenir el crimen a través de vigilancia de la conducta y minería de datos están fundamentalmente equivocados.
Es más, el umbral para los datos ingresados en las bases de datos policiales es extremadamente bajo.
La vigilancia policial está equivocada por diseño: uno de los muchos ejemplos es la inclusión de infantes en la base de datos CalGang.

La vigilancia contemporánea es una vigilancia altamente militarizada.
Una porción de las herramientas de vigilancia usadas por la policía viene de la contrainsurgencia y la otra porción viene del sector comercial.
Un sector toma vidas y el otro sector reduce valiosas experiencias y relaciones a puntos de datos monetizados.
Estas armas con frecuencia viajan desde zonas de guerra distantes como Irak y Afganistán hasta puntos locales de conflicto como la frontera sur de Estados Unidos, Ferguson, Baltimore y Standing Rock.

La policía no debería practicar vigilancia porque la vigilancia es defectuosa por diseño debido a sus sórdidas raíces históricas en la creación de daño, abuso, anti negritud y racismo.

## Refutación

* A favor: Myke Cole.

Todo lo que han dicho es correcto.
La vigilancia se pone en uso en sistemas que perpetúan la supremacía blanca.
Es convertida en arma en contra de aquellas que viven en los márgenes de la sociedad.
El umbral para inicial la vigilancia es muy bajo, su práctica es torpe y extralimitada.
Los datos obtenidos de la vigilancia no se aseguran apropiadamente.

Y también funciona.
Usada correctamente, puede proteger las vidas de aquellas en los márgenes.
Puede impedir actos de intolerancia.
Puede derribar la supremacía blanca y ayudar a que personas marginalizadas vivan en seguridad.

Aunque usadas de forma incorrecta por el NRA, las palabras de Séneca aún se mantienen verdaderas:
«Una espada no mata a nadie.
Es una herramienta en las manos de un asesino».
La vigilancia es sólo eso - una herramienta neutral.
Herramientas capaces de opresión y de terminar vidas fueron desplegadas para liberar a mi familia de los campos de concentración Nazis.
Cuando yo fui al derrame de petróleo de Deepwater Horizon, vi a cañoneros capaces de destruir pueblos ser usados para absorber químicos tóxicos, a las guerreras operándolos ocupadas en restregar a la vida marida empapada en aceite hasta que quedara limpia.
Usadas de forma correcta, las herramientas pueden ir en contra de todo lo incorrecto que han causado.
Incluso pueden revertirlo.

Las comunidades marginalizadas necesitan efectiva protección policial.
Aquellas viviendo bajo la línea de pobreza son víctimas de crímenes violentos a una tasa de más del doble que aquellas con ingresos más altos.

La vigilancia puede funcionar.
Un correo electrónico interceptado previno el bombardeo del metro de la Ciudad de Nueva York de 2009.
La NSA ha hecho mucho para ganar el escarnio y la desconfianza del público estadounidense, pero yo también trabajé con ellas.
Cuando el General Keith Alexander afirma que las intercepciones de SIGINT han interrumpido cincuenta y cuatro conspiraciones de ataques separados, yo le creo.

Teníamos una pancarta en nuestro despacho en la Estación Nueva York.
«El fracaso es inolvidable», decía.
«El éxito, invisible».
La policía se preocupa acerca de los «cisnes negros» - eventos de alto impacto y baja probabilidad.
Desplegamos cañoneros para escoltar el Ferry de Staten Island en el altamente seguro y pacífico Puerto de Nueva York.
Cuando amistades se ríen de esto, siempre les respondo: «9,999 veces, tienen razón. La vez número 10,000, algún idiota empaca un Zodiac lleno de TATP y lo choca contra el lado del ferry.
Ahora tengo mil personas en el agua.
Si es invierno, podría revolver todos los activos en el puerto, y por lo menos cien de ellas van a morir».

La policía tiene que hacerlo bien.
Cada vez.
Todas las veces.
Las comunidades que están sufriendo de abusos policiales también se hubieran montado en el metro en 2009, y en el ferry cuando esa vez número 10,000 llegue.

Reformar la policía no puede ocurrir al precio de volverla ineficaz.

## Refutación

* En contra: Hamid Khan y Ken Montenegro

Es bueno que el otro lado, que ha estado hablando a favor de la aplicación de la ley, concede que la vigilancia es una herramienta que perpetúa la supremacía blanca, porque entonces podemos hacer preguntas, como:
¿Es aceptable el daño que causa la vigilancia?
¿En serio funciona?
¿Quién dicta las tasas aceptables de fracaso, y cómo se ve la vigilancia «efectiva»?
Las respuestas apuntan a la verdad de que toda la vigilancia - incluyendo la de vidrios rotos, guerra a las «pandillas», guerra al «terror» - está diseñada para causar daño.

Empecemos preguntando qué daño es aceptable, hacia quién, y por qué:
comunidades nativas, negras, cafés y pobres experimentan el daño de la vigilancia policial en proporciones genocidas.
Cuando hablamos acerca de vigilancia, sus experiencias y voces deberían ser amplificadas y consideradas más que la hipérbole hablada por la policía «falta de personal» que nos mantiene seguras de las hordas de «otras».
Luego, la pregunta de si funciona:
El NYPD no impide crímenes en Wall Street porque está muy ocupado participando en detener y requisar.
Las estadísticas muestran que la policía no resuelve crímenes en comunidades negras y cafés sino que pone en peligro a esas comunidades en formas emocionales y físicas - incluyendo la constante amenaza de violencia y la violencia real que las comunidades de color experimentan.
La ciudad de Miami resuelve menos de un tercio de todos los casos de asesinato en muchos de sus barrios negros, y cuando el LAPD estaba investigando incompetentemente al asesino serial «Grim Sleeper» le dijo a una activista que él «sólo estaba matando prostitutas».
Lo que parece ser aceptable es en realidad la criminalización a través de las practicas enfocadas en la raza, tales como manejar siendo negra/café, y la creación de daño disfrazada de protección, como detener y requisar o las búsquedas «aleatorias» en los aeropuertos.
Con gente blanca siendo la única población que está en gran parte segura del daño policial, ¿cómo es esto aceptable?
Recordemos que el LAPD usó la designación NHI para denotar llamadas de violencia doméstica negras y cafés.
NHI significa «no humanas involucradas».

Ontológicamente, la vigilancia no puede ser neutral porque nace de y permite la supremacía blanca.
Asegurar que la vigilancia es neutral es tan inexacto, contra intuitivo y peligroso como decir que una arma de fuego es un instrumento de paz.
Hoy podemos ver la policía «predictiva» y ver que es una pseudo ciencia racista disfrazada como ciencia.
Por lo tanto, insistir que la vigilancia «funciona», después de conceder que hace avanzar la supremacía blanca, acepta que la vigilancia y el trabajo policial sólo impide las posibilidades de vida de no blancas y otras gentes en los márgenes.

El cálculo que la aplicación de la ley usa para justificar la vigilancia llega desesperadamente a «porque no podemos fallar» o «sólo fallar una vez» para excusar el daño.
La red interconectada de vigilancia, que llamamos el Estado Acosador, falla repetidamente y aún así nunca rinde cuentas cuando rompe y roba vidas negras y cafés.
Mientras es fácil tocar fibras sensibles y reclamar que las armas (incluyendo las de vigilancia) liberaron al mundo del horror de los Nazis, es importante pensar en el refrán «la policía y el Klan trabajan mano con mano» para recordar que una no necesita llevar una esvástica para hacer avanzar la supremacía blanca.
Por ejemplo, un proceso en apariencia benevolente como el censo usado en contra de la comunidad japonesa estadounidense durante el internamiento.

Finalmente, la discusión de la vigilancia nos ruega examinar el rol violento del estado y sus agentes.
La vigilancia es sólo una de muchas herramientas opresivas, violentas y mortales de la aplicación de la ley.
Si el objetivo es crear daño, entonces sí, la vigilancia es una herramienta valiosa.
Y si la eficacia de la policía está correlacionada con un intento de causar daño, entonces sí, la vigilancia es esencial

Nosotras preferimos un mundo en el que las vidas de las personas de color y otras comunidades tradicionalmente atacadas son más valoradas que la «eficiencia» policial.

## Cierre

* A favor: Myke Cole.

Cuando los sistemas falla, la tentación de abandonarlos es fuerte.
Algunas veces, el diente está demasiado roto como para rellenarlo.
Debe ser sacado.

Con la vigilancia, ese no es el caso.
Ha sido horriblemente mal utilizada.
No ha rendido cuentas.
Ha sido empleada hacia fines dañinos.

Podemos hacerlo mejor.
Podemos hacer que sirva a aquellas que alguna vez atacó injustamente.

Considere este hipotético:
una mujer anciana vive en una vivienda pública.
Ha trabajado toda su vida en un trabajo de salario mínimo para cuidar a sus nietas.
Porque le ha fallado el sistema, a ella se le ha negado la educación con la que podría protegerse a sí misma, y cae presa fácil de correos electrónicos de suplantación de identidad enfocados en su falta de comprensión de los controles bancarios y las normas de TI.
Con herramientas de vigilancia digital, la policía está armada para atrapar a los maleantes que la estafarían.

O este:
una joven activista va a Alabama a exponer la rampante supresión de votantes enfocada en minorías.
El jefe del partido Republicano decide que ha tenido suficiente, y le dice a sus lugartenientes que «el no se sentiría mal si esa idiota apareciera boca abajo en el desagüe».
Unos matones arrinconan a la activista en la noche.
Nadie está alrededor para ayudar.
Pero la cámara de vídeo que la policía instaló en un poste de luz lo atrapa todo.
Incluso si la policía no llega ahí a tiempo para detener el ataque, tiene la evidencia que necesita para atrapar y enjuiciar a los maleantes que lo hicieron, para asegurarse que no lo harán nunca más.

Históricamente, las armas han sido las herramientas de la opresión injusta, pero también de la revolución justa.
Han sido usadas para amenazar, robar y matar, pero también para proteger y asegurar.
Son las herramientas de las fuerzas de paz de la ONU.
Estuvieron en las manos de la Guardia Nacional que forzosamente desegregó la Universidad de Alabama en 1963.
No se equivoquen, George Wallace fue un monstruo, y él desde luego que hubiera usado la violencia hacia las estudiantes afro estadounidenses si las armas no hubiesen estado presentes para obligarlo a rendirse.
La vigilancia no es diferente.
Es una herramienta que puede volverse buena.

Me duele ver la fe en la aplicación de la ley tan malamente erosionada, incluso más al saber que esa erosión es grandemente merecida.
Pero la respuesta no es tirar el grano con la paja.
La vigilancia es una herramienta que se puede poner a trabajar para le gente contra la que ha sido usada.
Puede ayudar, en lugar de dañar.

No la rechacen.
Refórmenla.
Dejen que se enderece a sí misma y denle a nuestras comunidades la protección que se merecen.

## Cierre

* En contra: Hamid Khan y Ken Montenegro

Durante este debate, hemos demostrado que la vigilancia policial está fundamentalmente equivocada por diseño y que es una herramienta de la supremacía blanca.
Es irónico que el otro lado acepta la supremacía blanca y el racismo, y aún así las reduce a una pérdida de razón o función momentánea.
La supremacía blanca ha sido y sigue siendo esencial para el núcleo del DNA social, cultural, económico, político y estructural de los Estados Unidos:
no es una acción, es la experiencia vivida por millones de millones quienes han sido esclavizadas, genocidadas, encarceladas, deportadas, asesinadas.
La policía ha sido y sigue siendo el mecanismo primario de aplicación de la supremacía blanca, y la vigilancia es la principal herramienta policial para trazar, rastrear, monitorear, acosar y asesinar.

En un arrebato para decir que la vigilancia es neutral, quienes apoyan al Estado Acosador borran y minimizan no solo las experiencias de las comunidades marginalizadas y atacadas, sino también la sórdida historia de la vigilancia de aplicación de la ley (atrapando esclavas, rompiendo sindicatos, COINTELPRO, etc).

Las voces y la historia borradas son exactamente aquellas de las personas vigiladas y dañadas a través de narrativas tales como «nativo salvaje», «negro criminal», «latino ilegal», «asiático manipulador» y «terrorista musulmán».
La historia borrada es el origen del cuento de la policía como protectora del capital y atrapadora de esclavas.
No se puede decir que está interesada en proteger a las comunidades marginalizadas y al mismo tiempo criminalizarlas activamente.
No puede admitir que es una herramienta de supremacía blanca y aún así asegurar que la vigilancia protege a las comunidades marginalizadas, mientras que los hechos nos muestran que la policía no ha protegido y no protege a esas comunidades.

La vigilancia policial ha patologizado a las gentes negras, cafés y pobres como riesgos inherentes a la seguridad pública.
La misma práctica de la vigilancia policial se basa en la necesidad de descubrir, acosar y dañar las vidas de los cuerpos «sospechosos» y aquellas consideradas como una amenaza al sistema.
La policía siempre ha sido y sigue siendo primero una herramienta para el control social.
Su método violento de control es golpear a la gente hasta la sumisión, meterlas en cajas, asesinarlas.
Esto es la forma de la supremacía blanca de lidiar con las condiciones de pobreza, descuido y racismo que causan que ocurra la violencia en las comunidades marginalizadas.

La vigilancia liberada para descontextualizar y criminalizar a las comunidades atacadas.
La policía sigue siendo un instrumento de opresión, vigilando poblaciones mientras al mismo tiempo usan la fuerza y la coerción y la reproducción del miedo dentro de las comunidades.
Descontextualizar la historia de la vigilancia pierde este punto clave:
la vigilancia es una forma de violencia racial que debe ser abolida, no reformada.

Al final, la historia de la vigilancia policial es una historia de que la policía se apalanque de la vigilancia y el acoso para su intención de causar daño, con una creciente dependencia en pseudo ciencia envuelta en el lenguaje de algoritmos predictivos, vigilancia del comportamiento y minería de datos (por nombrar unas cuantas).
Si fuéramos a adaptar enfoques basados en la reforma, esencialmente estaríamos diciendo que estamos buscando un racismo reformado, o más amable y gentil.
Reformas y otros retoques en los bordes de esta violencia crean más daño del que remedian.

Invitamos a las personas de conciencia a que trabajen hacia la abolición de la vigilancia policial antes de que la próxima bala ataque un cuerpo.
Digan no a la complicidad, digan no al miedo, digan no al Estado Acosador.
