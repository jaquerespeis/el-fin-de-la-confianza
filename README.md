# El fin de la confianza (_el-fin-de-la-confianza_)

> Traducción de la edición #54 de McSweeney, «El fin de la confianza», en
> colaboración con Electronic Frontier Foundation.

Algo salió mal.

## Tabla de contenido

* [Todo pasa tanto, *por Sara Wachter-Boettcher*](todo-pasa-tanto.md)
* [El punto más alto de la negación, por *Cory Doctorow*](el-punto-mas-alto-de-la-negacion.md)
* [Las camioneras nos están trayendo las noticias del futuro, por *Julia Angwin* y *Trevor Paglen*, moderada por *Reyhan Harmanci*](las-camioneras-nos-estan-trayendo-las-noticias-del-futuro.md).
* [¿Deberían las agencias policiales usar vigilancia?, por *Hamid Khan*, *Ken Montenegro* y *Myke Cole*](deberian-las-agencias-policiales-usar-vigilancia.md).
* [La economía de la desconfianza, por *Ethan Zuckerman*](la-economia-de-la-desconfianza.md)
* [Edward Snowden le explica blockchain a su abogado - y al resto de nosotras, por *Ben Wizner*](edward-snowden-le-explica-blockchain-a-su-abogado.md)
* [Vigilando el cuerpo negro, por *Malkia Cyril*](vigilando-el-cuerpo-negro.md)
* [*Álvaro Bedoya* habla con *Cindy Cohn*](alvaro-bedoya-habla-con-cindy-cohn.md)
* [Reconsiderando el anonimato en la era del narcisimo, por *Gabriella Coleman*](reconsiderando-el-anonimato-en-la-era-del-narcisimo.md)
* [Se necesita una aldea, por *Camille Fassett*](se-necesita-una-aldea.md)
* [*Virginia Eubanks* habla con *Jacob Silverman*](virginia-eubanks-habla-con-jacob-silverman.md)
* [Las tarjetas postales que enviamos *por Soraya Okuda*](las-tarjetas-postales-que-enviamos.md)

## Mantenedor

[@elopio](https://gitlab.com/elopio)

## Contribuyentes

Lemur

## Leer el original

La versión original en inglés se puede
[descargar en la página de la EFF](https://www.eff.org/document/end-trust-0),
o [comprar en la tienda de McSweeney](https://store.mcsweeneys.net/products/mcsweeney-s-issue-54-the-end-of-trust).

## Contribuir

La traducción de este libro la estamos haciendo en comunidad. Invitamos a
participar a todas las personas que quieran colaborar.

Si saben inglés y español, nos pueden ayudar
[revisando las traducciones en progreso](https://gitlab.com/JaquerEspeis/el-fin-de-la-confianza/merge_requests).
O pueden traducir un nuevo texto; para esto
[reporten un issue](https://gitlab.com/JaquerEspeis/el-fin-de-la-confianza/issues/new)
con el nombre del texto que quieren traducir, para que no dupliquemos el
trabajo.

Para quienes no saben inglés, igual nos pueden ayudar revisando las
traducciones ya aceptadas, que van a encontrar en la
[Tabla de contenido](#tabla-de-contenido).

Si tienen dudas sobre los temas de los artículos, gitlab, el JáquerEspeis o
cómo empezar a colaborar con este proyecto, también pueden
[reportar un issue](https://github.com/JaquerEspeis/el-fin-de-la-confianza/issues/new).

# Licencia

La mayoría de los textos tienen una licencia
[Creative Commons Atribución](https://creativecommons.org/licenses/by/4.0/).
Los textos con esta licencia libre son los que vamos a traducir.

Esta edición tiene una licencia
[Creative Commons Atribución-No Comercial-Sin Derivados](https://creativecommons.org/licenses/by-nc-nd/4.0/), por lo que no podemos traducirla toda.
Los textos de Elizabeth Stix y Jennifer Kabat también tienen licencia
privativa, entonces esos quedan fuera de este proyecto.

***

Hecho con :rainbow: por [JáquerEspeis](https://gitlab.com/jaquerespeis).
