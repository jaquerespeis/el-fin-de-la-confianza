# Las tarjetas postales que enviamos *por Soraya Okuda*

> Tips sobre mantenernos vigilantes en la era de la información

## Introducción

Un corazón se sumerge en un carácter japonés, un rizado hiragana あ (a), y luego en una cursiva arábica ي (y). Escrito inclinado en diagonal, el lapicero va de izquierda a derecha, luego en reversa. Gramática persa e inglesa, con vocabulario inventado insertado y manteniendo los fonemas originales, componen la oración.

Lomay. “Te amo”.

Esto era algo que hacía de niña. Amaba crear frases basadas en otros lenguajes y escribir cortas, secretas notas a mi misma. Estas nunca eran notas de importancia, pero eran legibles por mí y solo por mí, lo que las hacía especiales.

Imaginaba que estas notas eran privadas: otros no sabían cómo decodificar los contenidos, y las reglas en las que lo basaba eran dispares y oscuras suficiente para que un observador no quisiera depositar el tiempo en resolverlo.

Conocía otros niños que hablaban Pig Latin, que me era indiscernible hasta que alguien me explicó las reglas de -ay. Otros, usando premios de cajas de cereales y anillos decodificadores, asignaban letras a números, incrementándolos lentamente en lenguaje revuelto.

Eso no me hacía especial entre los niños. A todos nos gustaba tener algo, escrito o hablado, que no fuera comprensible para la mayoría de personas, algo que requiriera ser descifrado y tener un entendimiento de nuestras reglas.

A veces le dábamos acceso a otras personas. Por ejemplo, digamos que decidí compartir la clave de mi lenguaje con solo una persona más: una buena amiga, que se mudaba al otro lado del país. El lenguaje era solo para ella y yo.

Cuando nos vimos por última vez antes de su gran mudanza, le enseñé cómo escribir el lenguaje. Le di la clave de qué significa cada carácter, la gramática básica y cómo leer algo que yo le enviara. Ella preservaría cuidadosamemnte el mensaje de ese papel, la clave, mientras se mudara. Yo también tendría la misma clave para mí misma, así podría saber cómo leer sus mensajes, y escribir de vuelta, en nuestro lenguaje. Mantendríamos nuestras claves en un lugar seguro, plegado en diarios protegidos por candados, escondidos de nuestros padres y hermanos. O tal vez, guardado en medallones que llevábamos alrededor de nuestro cuello.

Nos enviábamos tarjetas postales en nuestro lenguaje secreto. Los detalles sobre el mensaje – el tipo de papel, los lapiceros que usamos, el visible “para:” y “de,”, nuestras ubicaciones, los pequeños rasgones y arrugas de ser manipulada, la fecha y los operadores – sería sabido. Pero el contenido se mantendría incomprensible para los demás: codificado para ambas, legible para ambas y visible pero no comprensible para otros.

De muchas maneras, nuestras computadoras envían información a través del internet como las tarjetas postales. Metadata— datos sobre el mensaje como nuestros nombres, ubicaciones, servicios y redes, los tiempos enviados y recibido y leído y contestado, y tal vez incluso cómo fue manipulado a través de la vía, es visible.

Por defecto, información enviada en línea con texto sencillo, como con una tarjeta postal, también es visible. Sin embargo, podemos proteger el contenido de estos mensajes, usando cifrado para volverlo un criptograma para otros. Cuando está encriptado, los metadatos todavía son visibles, pero el contenido es ilegible a otros que no tengan la llave. Ahora podemos confiar en que el algoritmo de nuestras computadoras use cifrado difícil de romper.

La llave que compartí con mi amigue para nuestro lenguaje secreto es simétrica: la misma llave usada para codificar mensajes fue usada para decodificarlos. Sin embargo, ¿qué pasa si mi amigue y yo nos sintiéramos vacilantes respecto a compartir la llave en persona?

¿Qué si la hermana de mi amigue se escabullera en su cuarto, encontrara la llave mientras duerme, la copiara, y se remitiera a ella para leer nuestros mensajes? ¿Qué si ella falsificara la letra de mi amigue y pretendiera ser elle en una carta? En el internet, esto no es tan descabellado. A esto lo llamamos ataque de “hombre-en-el-medio”. Alguien interceptando un mensaje puede elegir ver el mensaje, alterarlo (o no alterarlo) y pasarlo. Los verdaderos participantes de la conversación puede que nunca lo sepan. La encriptación moderna asimétrica protege ante este escenario.

La encriptación asimétrica, o encriptación con llave publica, acomoda/aloja comunicaciones distribuidas geográficamente para que no tengamos que encontrarnos físicamente para compartir la llave para encriptar y desencriptar información. También provee una forma de verificar identidades.

De HTTPS a VPNs, de criptomonedas a la red Tor, los principios de encriptación simétrica y asimétrica son vistos en todas partes en comunicaciones encriptadas modernas.
La encriptación ha progresado significativamente, pero una encriptación fuerte no viene por defecto en algunas de nuestras comunicaciones digitales. Para muchas personas, lo que está en juego es mucho más que un miembro de familia leyendo nuestros mensajes secretos con un amigo.

Que enviemos nuestras comunicaciones en “tarjetas postales” sin encriptar es, poniéndolo leve, un problema. La persona común ahora necesita preocuparse por números de tarjeta de crédito y de seguridad social siendo robados por ladrones oportunistas mientras usamos un servicio no encriptado. Mientras buscamos a nuestros amigos a través de masas de personas clamando en una protesta pública, nuestros mensajes de texto y llamadas revelan la historia de nuestros contactos. Informantes contactando periodistas deben preocuparse por que sus empleadores vean sus comunicaciones en red, dispuestos a castigar cualquier señal de desobediencia. Periodistas están preocupados por que empresas los acechen, que el crimen organizado les amenace con violencia, que sus gobiernos le presten atención extra a sus mensajes entrantes y salientes, cazando a sus fuentes. Ni siquiera podemos confiar en muchas de nuestras redes de telecomunicación, que no solo lucran de recolectar datos de nuestra ubicación física y hábitos de consumo, pero que son en sí vulnerables a hackeo y a solucitudes de gobiernos y departamentos policiales.

Es fácil sentir una especie de nihilismo respecto al estado de la seguridad y privacidad. Pero aún así hay esperanza. La seguridad es un proceso. Seguridad es aprendizaje y mejora continua. Seguridad se reduce a buena higiene.

En talleres de seguridad digital, nos gusta evocar metáforas de salud pública y educación infantil cuando se discuten estos temas. Recuerda cuando te enseñaron a lavarte las manos con agua caliente y jabón, recuerda cuando te dijeron que tuvieras cuidado con los extraños, recuerda cuando te dijeron que buscaras personas con integridad y evitaras quienes fueran deshonestos. Eventualmente las siguientes sugerencias aflorarán naturalmente. Adoptar una o dos de estas medidas sugeridas – como la encriptación en tránsito, usar encriptación en reposo, y practicando higiene básica de seguridad – te ayudará a mantener tus dispositivos libres de acceso sin autorización. Hazte una linda taza de té y toma un paso a la vez. Lo conseguirás.

## 1. Proteger tarjetas postales usando cifrado

Cada tarjeta postal no cifrada lleva metadatos, tales como la dirección IP de tu computadora o teléfono y otros identificadores únicos. Esta tarjeta postal también lleva los datos, o contenido, de la comunicación. Esta tarjeta postal está siendo copiada como un archivo a las manos de cada computadora (servidores, redes de dispositivos y más) y pasada en el camino hasta que llega a tu amigo.

Y no es solo las computadores que pasan el mensaje de camino las que lo pueden ver: este flujo incluye potencialmente miles de computadores en redes compartidas que pasan el mensaje. Por ejemplo, alguien compartiendo tu conexión no segura a wi-fi en un avión o en un hotel o en una conferencia puede interceptar su actividad, ansioso por verte chequear tus correos, enviar imágenes, o visitar un sitio web. Imagina los cables de fibra óptica bajo el mar, en tierra y en las instalaciones de las empresas operadoras – el internet literal – que lleva las comunicaciones de la humanidad y las almacena, siendo interceptadas por computadoras propiedad de la NSA, como Edward Snowden reveló.

Estas tarjetas postales, sin embargo, pueden ser protegidas de ojos entrometidos. Hay un par de tipos de cifrado que hacen esto, categorizado bajo la más amplia cobertura de “cifrado en tránsito” - cifrado que proteje su información mientras está de camino de ti a tu destinatario.

Cifrado en tránsito debe proveer tres beneficios principales. El contenido del mensaje está cifrado de ojos fisgones, proveyendo privacidad. Adicionalmente, la integridad del mensaje es preservada, ya que es muy difícil que alguien intercepte, altere y edite el mensaje. Finalmente, cifrado asimétrico provee los medios de identificarse.
Identificación es uno de los grandes temas en comunicaciones digitales: ¿cómo probamos que somos quienes decimos que somos? Para personas a las que son dirigidos ataques de persona-en-el-medio (como activistas, periodistas y defensores de derechos humanos), la habilidad para verificar identidades es una característica especialmente útil de cifrado en tránsito. Si usamos medidas como huellas de llaves públicas, certificados y firmas digitales para verificación, podemos hacerlo mucho más dificil para otros hacerse pasar por nosotros, nuestros amigos, o un servicio.

### Ejemplos de claves públicas y privadas, creadas usando el algoritmo RSA.

Llave pública ▼

-----INICIO DE LLAVE PÚBLICA-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7/f9B2ijy0GdLDnEOZNlYErJWF7MXGg8ZM07MLj+/UdE/C8miNrk2PMMPezuznMWGp8fau175sdngwxX4+huk93OX+6RWoRCy8uhlb0Q7TKrDbLkuXJzfPeziJVR3Q8s7HRNbMeY4qzAzJqkeoRfWLglqlvDCH9NqcH5GusssaLo/HH2B1zLWvhulJI74UfawBnGMqguscJO3H0MFJSPtiIOy1oh96jTfw12GmhQEt8zYh8enYeHL9hzQ1fsMX6Fg/4fU4Dd2hD04VHG6m/dWpse999jPi8dkfIRWX9a8P7EkXRbwAyHqZhqgAj1SSbfnpDFxNop/3pq5H9XIrl5xwIDAQAB

-----FINAL DE LLAVE PÚBLICA-----

Private Key ▼

-----INICIO DE LLAVE PRIVADA RSA-----

Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,5F599C6178FF36F2
xLffrsOaMQNVUuy+4CKEENCPxbdChBHAQeB4u0xEaO/AMsLogf5mCSQ05GuoZX+m+ftOHPYfq8sWju9ikjmaHHe1s6BZWMRI2yh43qbYqTi3wu74zn05uNa+mcIiYip3xjB0ZZrynEm5+RC9coJzvPcc/deBAdU5iKteXXafhkmWvou4g9wM2BYglZSDdLyO1iG4G8LYZv45pp5U30JTHoW1NfewmzMXaaR5/KCoCgM5QV+eFaj6fRfD2UJoY3oY734zcmRa8jqx+gu5g0T8En3o4KenvykOZBkG0JOxL3oPecG1W0YYA+b2j/HiR+Zs2/CEGCnE9pN3HRgCcd7HiXgknisxGDO5H0hyLjsMbVyAurqF0GXNOpLbTGJia91xBRbqeOJ00RCUSty+pfKAVljyop/3ZyzMaGuZEXxkdiwPwrYQNnRe20YZUVOQVX12R/TZP0+6EPEJC53U7G6Py35G8TWod8I3ZWGc2hmBd/SUmMRwrmyLCZLPbYxb3/6FKLLyhFuymddKcdoKhTsdwDw/0rTrW9PValh3awTN3UAgY3ZACxqDd401LTvd3t50Gbki4U74HnqhyHXo6kLp3ycqaulVl1CReAlL6ZwzF1WnzBnJnN0oIGZ/6lMfGZmSPPWTsIz/MDYXfLLbJAwLv1I7kJDR56kqoihnEzh7YMFRwM7KK65LHjn6wjcMRPX/SbT17nxXqXUpA7zfoEqUseZZS2VoK0l15/WBQ4ZvQ6+Tiy+hW4dz8hGU5GsmF6QKEkJqhuyjzefyUnUaDKWSHDcGlQh3boOBYA7ZSvMtw9s7F9Yx79omx/kyV5AalAVdFQDQTMoT8q59OHjNspSZfXPXito5XGum6xaTseKNsQJYgM15w3yOfQE21Hq8XQUn7xTxmx77FCaZG39zixNeuWACOjuRhNn78UiOoh1X6d9YQVtxh8NbhZhxq5QdUDpw8gFa+0iSyg/rt2Sww/+4a/vJdkbWXpb7iEwIc+bRa4tgumSsPnoOKJ6Y/ssYx2y3O1gTN7SC+u+FKUa0cK+lfzJ2TLSB4H4AanyVxKA7nb+GU54Qx/znWw/fh1GOK4bJ1DU9SbGo2GHvVHDL8vrDimVy51hjEvtJ+hwgFvMF6T7qjGfRWMTNFlwKtcqmTSWxGy8ergjvhN/NkP3e1cRcfVO2PeIbdITBqbtus7C4JNMiP0MvVyA5e9QKgi/+6bDhL3ezkh8TyCVIKAFfwhA2b6UZuO9WeMd3iAAE+UAvU09hTuVd5aqzQi7123O6jxTJoBeD3hhtgmm6b4F7w3EidA9F9LN4dsb2dFKrGS2eKjRuYLe/MoLh6yIrBmbd//roJqFynrJ4HH4IBqmq3Kmh+sU0Oz00+Sarj7KRBAUhyoOlO2v2i4Er+8/OVPyJtN72MFdB8JnsVoDQ8xVLKdihKZCB1C4bHDqj2NwLWMjKaVz35z5svcVydqWR6C9hdAhutOE/fA/JpV7ixcUPG+L7mTJN7H78cIYRbTKZFmmJFKX8UKr+VFUynsVwr6wfGtFwH0oK0tq6cED9g+qUY+Fuaun/+YYleBfIWcF9m47wLy7WHMm1wR29JD9XdzvoiTvK

-----FINAL DE LLAVE PRIVADA RSA-----


El cifrado en tránsito depende ampliamente en principios de cifrado asimétrico. Un muy bien conocido algoritmo de cifrado, RSA, depende del principio de que es fácil para computadoras multiplicar dos grandes números primos, pero casi imposible desenredar del producto obtenido. Este algoritmo ha hecho posible para nosotros intercambiar información cifrada con personas en destinos remotos sin tener ambas que saber (y resolver una forma segura de intercambio) la clave de descifrado.
Cifrado asimétrico cumple esto al tener dos claves en vez de una: la clave pública, que alguien usa para enviarte mensajes cifrados secretos, y la clave privada, que tú usas para descifrar el mensaje.

En la página anterior hay un ejemplo del par de clave pública y clave privada, creada usando el algoritmo RSA.

Mi amigue usaría mi clave pública para enviar un mensaje cifrado solamente a mí. Nuestra clave pública es compartida ampliamente en nuestro sitio web, en una base de datos o en un mensaje – las metáforas se quedan cortas acá, pero es una forma de dirección a un buzón cerrado.

La clave privada es un archivo que guarda los ingredientes secretos que crearon la llave pública. Para RSA, estos son dos grandes y aleatoriamente elegidos números primos. La clave privada es lo que abre aquello que la clave pública cierra, y debe ser protegido y nunca compartido.

Probablemente yo no interactuaría con mi clave privada para nada mientras descifra el mensaje de mi amigo, mientras mi computadora hace el trabajo: descifrar el lenguaje secreto entre su computadora y la mía. Nuestra clave privada puede ser usada para dejar una firma digital inolvidable, como un sello personal, en los mensajes cifrados que mandemos, que un receptor puede verificar con la clave pública del remitente para autenticar su identidad.

Si mi amigue y yo estamos usando cifrado para escribirnos la una a la otra, ambas habríamos de tener nuestro propio set de claves, y la clave pública de la otra, salvada en nuestras computadoras.

Si la clave privada es “perdida” o compartida, alguien más podría descifrar las mensajes por los que nos esforzamos tanto para mantener privados, y potencialmente, hacerse pasar por nosotros. Por ejemplo, si la clave pública y privada de arriba fuera mi propia, yo ya habría comprometido la seguridad de mi mensaje al compartir mi clave privada más allá de mi computadora en un mail a mi editor, aún más al publicarla. Necesitaría generar un nuevo par de claves para tener comunicaciones seguras, y notificar a mis amigos de que el par de claves viejas se han visto comprometidas.

Eliminar la clave privada (o perder acceso, tal como verse incapaz de trasnferir el archivo de la clave privada desde una computadora averiada) puede significar que ya no seamos capaces de acceder a información cifrada que corresponda con la clave pública.

Dos de las principales formas de cifrar en tránsito son cifrado de capa de transporte y cifrado de extremo a extremo. Ambos métodos usan los principios de claves del cifrado asimétrico, pero con resultados diferentes con objeto de quién puede leer las tarjetas postales cifradas.

### Cifrado de capa de transporte

Digamos que estás buscando unirte a un sitio web de citas gratuito desde tu teléfono o computador. Escribes el nombre del sitio web en tu navegador y aterrizas en una página que inicia con <http://>. Cualquiera en el camino (alguien fisgoneando a escondidas en tu conexión inalámbrica, como otro usuario en una cafetería, o tu proveedor de internet (ISP) o de telecomunicaciones, o alguien en la red del sitio web) puede ver estas solicitudes. Al usar un simple software, pueden ver tu contraseña para la cuenta que creaste, el email con que te registraste, las personas con las que coincides, tus preferencias respecto a citas, tu género, tu orientación sexual, los mensajes entre ti y las personas que coincides, y más.

 HTTP (Hypertext Transfer Protocol) es usado por tu computador para intercambiar información entre los servidores hospedando el sitio web que estás accediendo; HTTPS es la versión segura de dicha comunicación, usualmente simbolizado por un pequeño candado verde junto al URL en tu navegador.

HTTPS provee cifrado de capa de transporte, que cifra información entre el servicio (incluyendo las computadoras y los administradores de ese servicio) y tú. Computadoras fisgonas en el medio de tu conexión no pueden ver las páginas que tú estás mirando, los formularios que llenas, las búsquedas que hagas en un sitio, o las acciones que completes. Tú estás todavía, por supuesto, confiando en el servicio en sí, ya que verá tu información al ser insertada.

Cifrado de capa de transporte usa tanto cifrado asimétrico como simétrico: en otras palabras, un montón de claves. Cuando un sitio web usa HTTPS, tu navegador y el servidor del sitio web tienen una muy rápida serie de interacciones llamado “el apretón de manos”, en la que se verifican la identidad entre sí usando sus claves pública y privada e iniciando una conversación cifrada.

Las computadoras intermediarias pasando los mensajes todavía pueden ver los metadatos (que te estás conectando a la dirección de internet) pero nada más.
Por ejemplo, digamos que yo quisiera buscar “cifrado para defensa personal de vigilancia” usando el buscador de DuckDuckGo. DuckDuckGo está protegido por HTTPS, así que alguiene en mi red solo vería mi acceso a <https://duckduckgo.com/>, y no <https://duckduckgo.com/?t=ffnt&q=cifrado+para+defensa+personal+de+vigilancia&atb=v154-1&ia=web>.
Las buenas noticias son que HTTPS se está convirtiendo de uso general y es fácil de configurar. Usarlo lo más posible ayuda a ser más seguro con tus hábitos. Algunos sitios web tienen una versión HTTPS, pero no es la predeterminada: si te gustaría usar cifrado en sitios web cuando está disponible, puedes bajar la gratuita extensión de protección de privacidad de navegador de EFF, HTTPS Everywhere.

Si administras tu propio sitio web y te gustaría activar HTTPS para proteger a aquelles que visiten tu sitio web, puedes obtener un certificado HTTPS gratuitamente de la autoridad certificadora, Let's Encrypt.

Usar HTTPS lo más posible es una cosa genial. Sin embargo, solo porque un sitio web usa HTTPS no quiere decir que los dueños del sitio web o quienes hospedan código adicional en el sitio web están eligiendo proteger tus datos. Es importante notar que, solo porque un servicio usa algún tipo de cifrado, no significa que el servicio valora la privacidad de sus usuarios.

Abajo hay algunos ejemplos de los que estar atento.

Cuando visitas un sitio web, partes de la página provienen de computadoras distintas al servidor original del sitio web que pediste visitar. Cuando estás en un sitio, probablemente verás anuncios de terceros, botones de me gusta y compartir, secciones de comentarios y otras imágenes y código embebido. En algunas instancias, este contenido de terceros puede traer métodos únicamente para identificar el navegador de tu computador y monitorear tus hábitos de navegación, compras e intereses mientras te mueves entre sitios HTTPS a sitios HTTPS. Para prevenir este tipo de monitoreo de terceros, EFF tiene una extensión de navegador gratuita para los navegadores de Chrome, Firefox y Opera, llamada Privacy Badger.

Algunos servicios web puede que elijan recolectar y retener tanta información del usuario como les sea posible. Digamos que estás usuando un sitio web protegido por HTTPS para manejo de proyectos, servicios de chat, redes sociales o email. Tal vez creaste un pequeño y cerrado grupo dentro de ese servicio: puede ser fácil sentir que la comunicación es protegida entre tú y las personas en ese grupo. El mismo servicio HTTPS tendrá acceso a este set de comunicaciones cifrado por capa de transporte, con un número de empleados teniendo acceso a estas claves a través de los servidores corporativos.

Para aquelles preocupades por vigilancia corporativa, hay un riesgo de que este servicio HTTPS pueda elegir analizar y guardar la información para mercadeo u otros propósitos. Y para aquelles preocupades por vigilancia gubernamental, hay riesgo de que la policía solicite acceso a tus datos de usuarie. Es crítico que las compañías luchen contra estas solicitudes, y que le digan a les usuaries sobre solicitudes de datos de gobiernos, publicar reportes de transparencia anuales y que requieran órdenes judiciales para acceder a contenido. Desafortunadamente, usuaries de estos servicios son cargados con el esfuerzo de evaluar políticas de privacidad y reportes de transparencia para juzgar las protecciones que una compañía pueda proveer.

Cifrado de capa de transporte, tal como HTTPS, provee significativa protección de ataques oportunistas. Pero ¿qué si tú no sientes que puedes confiar siquiera del servicio para proteger tus comunidades de la policía o de la corporación misma? ¿Qué si no confías en una compañía siquiera con tus metadatos o con las llaves para descifrar tus comunicaciones?

### Cifrado de extremo a extremo

La mayoría del tráfico web está cifrado en su camino al servidor con HTTPS, pero la mayoría de email, chat, mensajería y servicios de streaming de audio y video no están cifrados de extremo a extremo. Esto significa que la compañía que te provee con un servicio – sea Google para Gmail, Microsoft para Skype, o Facebook para Messenger – le es gratis acceder a tus datos mientras fluyen a través del servicio.

Esta riqueza de datos es visible para empleados maliciosos con controles de acceso, tales como administradores de sistemas descontentos, y pueden ser entregados a policías y gobiernos.

Ahora tenemos servicios que ofrecen “cifrado de extremo a extremo” para solventar ese problema.

Cifrado de extremo a extremo es un tipo de cifrado en tránsito que cifra las comunicaciones para que solo sean accesibles para tu dispositivo y los dispositivos de tus amigos. Todos los servicios que pasan tu mensaje, incluida la app o la compañía proveedora de la herramienta de cifrado de extremo a extremo, no pueden descifrar tu mensaje. No obstante, el servicio intermediario todavía ve todos los metadatos, como el hecho de que estás usando cifrado de extremo a extremo y a quienes contactes entonces. El cifrado de extremo a extremo está diseñado para que tus claves de descifrado sean tuyas solamente y las de tus amigues de elles. La responsabilidad recae en ti y tus amigues de proteger el hilo de mensajes – por esa razón, usar cifrado de extremo a extremo te hará más consciente a usar claves públicas y privadas.

Aunque tal vez no sea el caso más práctico, correo electrónico cifrado de extremo a extremo es uno de los ejemplos más ilustrativos sobre cómo la noción de cifrado asimétrico es implementada. Digamos que mi amigue y yo usamos el software de correo electrónico cifrado de extremo a extremo de inicios de los 90s llamado PGP. Ambos hemos bajado PGP usando software como Mailvelope, el cual puede ser usado encima de nuestro correo electrónico regular– digamos, Gmail. Correo electrónico como Gmail está cifrado en tránsito usando HTTPS y STARTTLS, protegiéndolo de fisgones. Sin embargo, digamos que mi amigue y yo no nos sentimos cómodos con Google teniendo acceso a los contenidos de nuestro correo electrónico. Yo escribiría mi mensaje secreto en el software de cifrado de mi correo electrónico a elle, luego lo cifraría dirigiéndolo a su clave pública. Una vez está cifrado, el cuerpo del mensaje se ve como tonterías ilegibles.

Le agregaría una línea de asunto aburrida – lo único no cifrado – direccionaría el correo electrónico a su dirección de Gmail y lo enviaría. Cuando elle lo reciba, similarmente parecería tonterías hasta que elle use su software PGP (donde tendría guardada su clave privada) para descifrar el mensaje: “Te extraño, amigue.”
Cifrado de extremo a extremo es ahora posible para mensajería, videollamadas y envío de archivos. Personas que tratan con datos particularmente sensibles, tales como documentos financieros, información médica, datos de clientes y fotos o videos que otros puedan querer, ahora tienen opciones fáciles de usar para proteger el contenido de los mensajes cuando los envían.

Signal, una aplicación de smartphone de Open Whisper Systems, es un ejemplo de una herramienta gratuita y de código abierto que usa cifrado de extremo a extremo por defecto. Puede ser usado a través de wi-fi e internacionalmente. Y a diferencia de correo electrónico cifrado de extremo a extremo, donde tienes una única clave privada que otorga acceso a descifrar tu bandeja de correo completa, una característica cool de Signal es que cada mensaje está cifrado por separado usando una nueva clave simétrica, que le da a tus conversaciones una propiedad llamada “confidencialidad hacia adelante”. Esto quiere decir que si alguien obtiene la clave para descifrar uno de tus mensajes, no podrán descifrar el resto de ellos. Más y más desarrolladores de software están adoptando los protocolos subaycentes de cifrado y las características de Signal; vean por ejemplo el cambio de WhatsApp a cifrado de extremo a extremo en 2016.

En cifrado de capa de transporte, tu navegador y el servidor del sitio web automáticamente chequean la información de la clave pública para verificar que son quienes dicen ser y para reducir la posibilidad de que haya un ataque de hombre-en-el-medio. Con cifrado de extremo a extremo, este proceso de identificación no es automático: la responsabilidad de verificar las huellas digitales del otro compete a ti y a tu amigue.

Para aquelles particularmente interesades en preservar la privacidad de sus mensajes – como periodistas y fuentes, o servicios proveedores y clientes – chequear huellas de claves públicas es una función especialmente importante. Chequear huellas de claves públicas significa leer cada número y cada letra de un hilo y confirmar que es el mismo que fue visualizado en el dispositivo de tu amigue.

Algunos de estos servicios pueden estar conectados a través de dispositivos, por ejemplo en tu teléfono y tu computadora de escritorio. Mantener chats cifrados de extremo a extremo en más de un dispositivo podría sonar como una opción atractiva. No obstante, mientras la clave privada para cifrado de extremo a extremo sea un archivo sensible, almacenar tu clave privada en más de un dispositivo puede ser una decisión de seguridad significativa. Una precaución: si no eres consciente de configuraciones para respaldos, algunos servicios podrían respaldar chats cifrados de extremo a extremo como texto simple, sin cifrar, en servidores corporativos en algún sitio tal como iCloud de Apple.
Usar cifrado en tránsito, como cifrado de extremo a extremo, protege el mensaje mientras viaja. ¿Pero qué tal cuando llega a un dispositivo y es almacenado?


## 2. Hogares digitales

Al utilizar nuestros dispositivos, ellos comienzan a construir otra versión de nuestros hogares. Nuestras búsquedas, reflejando nuestras preocupaciones e intereses en general, son los libros que llenan nuestros estantes. Nuestros archivos, reflejando nuestros trabajo y nuestra información financiera, son nuestros diarios. Nuestras fotos y mensajes son nuestras cartas de amor a familiares y amigos, reflejando nuestras conexiones a otros humanos, las cosas que apreciamos, la forma en que nos relacionamos entre nosotros, nuestras inseguridades.
Así como nuestras casas pueden ser robadas, nuestros dispositivos pueden tener información valiosa que se pierda o se arrebate. No obstante, hay muchas cosas que podemos hacer para proteger esta información, incluido pero no limitado a cifrar los datos almacenados.

### Protecciones para evitar espíritus maliciosos

Cuando un dispositivo ya no esté en tu control – una computadora robada de una oficina o un teléfono que bajó algún malware – aún puede ser protegido.
Si valoras tu información y si restaurarla es una prioridad, respalda regularmente los datos. En caso de que un dispositivo falle y deba ser reemplazado o reparado, tú puedes tener una copia a la cual regresar. Como fue mencionado, sé consciente de las configuraciones del respaldo (en particular, asegúrate de que tus mensajes cifrados no se respalden como texto simple a un servidor remoto como iCloud).

Otra medida importante para proteger tu dispositivo es estar regularmente actualizando tu software, incluyendo aplicaciones celulares. El software puede ser entendido como una infraestructura, sobre la que se construye constantemente. A veces agujeros son encontrados en un sistema operativo, como los cimientos de un dispositivo, compromete su estabilidad. Estos huecos son como ventanas rotas en tu dispositivo, conocidas por una red de ladrones que buscan personas como tú. Imagina a alguien viendo a través de tu cámara web o escuchando a través del micrófono de tu teléfono sin que lo sepas. Usualmente podemos prevenir este tipo de brechas al instalar las actualizaciones sin demora y regularmente y evitando sistemas operativos y software que ya no están recibiendo actualizaciones. Aunque se sienta inconveniente ejecutar estas actualizaciones, ellas protegen el bienestar general de tu sistema.

Algunas veces accidentalmente le damos la bienvenida a esas vulnerabilidades. Malware es software mailicioso, usualmente desarrollado con la intención de extraer datos sensibles de una máquina y enviándolo a la computadora de alguien más.

A través de culturas, tenemos mitos de bellas y doradas cajas que no están destinadas a ser abiertas. Ya sea la caja de Pandora o el truco castigador que ofrece una caja de presunto tesoro en el folclor japonés como el caso de Shitakiri Suzume, la curiosidad nos supera, abrimos estas cajas y un demonio sale. Malware puede ser esparcido de muchas formas, usualmente castigando nuestra curiosidad o nuestra ansia por regalos. Por ejemplo, sé sospechoso de conectar tu teléfono en puertos eléctricos gratuitos en asientos de avión, en cámpuses universitarios o en calles de ciudad. A menos que tu entrada de dispositivo tenga un “condón USB” puesto (un cable USB o adaptador que limite el acceso a los pins de datos cuando tu teléfono esté conectado, manteniendo solo el puerto de carga abierto), conectar a uno de estas estaciones de carga tiene el riesgo de insertarle malware en tu dispositivo. Igualmente, sé receloso de CDs gratuitos, dispositivos USB gratuitos y de otras personas solicitando conectarse a tu dispositivo. Hay historias de hackers y espías dejando memorias USB en lugares públicos con la esperanza de que individuos específicos tomen una y confiadamente la conecten a sus computadoras.

La extracción de datos valiosos también puede ser hecha a través de ingeniería social. Así como un ladrón puede vestirse como un reparador  y (si no es cuestionado) entrar a tu edificio de apartamento, otros usan trucos de suplantación para ganar acceso y confianza, así como con phishing – un tipo de estafa que consiste en que el usuario sea engañado para revelar información personal o confidencial.Un tipo común de phishing es enviar emails, textos, o mensajes de redes sociales a la medida que parecen ser genuinos. El phisher promete ayudarnos si le damos click al enlace o vamos a un sitio web que se ve legítimo (usualmente usando una dirección acertada excepto por un carácter o un enlace oscurecido), o descargar un archivo (por ejemplo, algo aparentando ser un .pdf, .doc, o más sospechosamente, .jar). Phishing también puede tomar la forma de un sitio ofreciendo descargas de software o de apps.

Las clickeamos. Y luego malware se descarga encubiertamente en nuestra máquina.  En el mejor de los casos, puede que solo sea adware – software intrusivo diseñado para vender algo – reduciendo significativamente la velocidad de nuestras máquinas o teléfono. En el peor de los casos, es capaz de mermar las medidas que tomamos para cifrar nuestras comunicaciones, como grabando lo que tecleamos, lo que vemos en nuestros dispositivos, nuestros archivos y lo que nuestra cámara ve. Una clase particularmente problemática de malware es el ransom-ware, donde un sujeto malicioso gana acceso a nuestra máquina, toma control de ella, cifra toda la información y luego demanda dinero a cambio de la llave de descifrado. En 2017, un ransomware conocido como Wanna-Cry atacó más de 200 mil computadoras alrededor del mundo. Fue particularmente dañino al Servicio Nacional de Salud de Inglaterra, obstruyendo el cuidado de pacientes y forzando al personal del hospital a tratar un tipo distinto de enfermedad: un virus de computadora.

Puede ser retador detectar malware en nuestros dispositivos, pero un buen software antivirus puede hacer algún trabajo chequeando archivos respecto a malware conocido y sistemas operativos provistos de herramientas como monitores de actividad o monitores de tareas pueden chequear si nuestras computadoras están usando energía en tareas anormales. Desafortunadamente, la mejor manera de abordar malware es no contraerlo inicialmente: actualizando regularmente software y siendo cuidadoso para evadir intentos de phishing. Si nuestros dispositivos contraen malware, la solución usualmente requiere borrar el dispositivo y restaurarlo a una versión de respaldo. Si es golpeado por un malware particularmente persistente, puede que tengamos que conseguir un dispositivo completamente nuevo.

O tal vez, como es común, estafadores te hacen ingeniería social a través de intentos de phishing artesanales a la medida, conocidos como spear-phishing. Aún si no tienen la intención de instalar malware en tu dispositivo, un resultado similarmente malo puede ser que ganen acceso a una única cuenta para un sitio web. Al depositar erróneamente nuestra confianza en un servicio suplantado, le otorgamos al atacante acceso a nuestra cuenta que tiene la información que desean.

John Podesta, director de la campaña presidencial 2016 de Hillary Clinton, cliqueó un link de phishing que se hacía pasar por un sitio genuino de Gmail, dándole acceso a atacantes para ver los correos electrónicos de la campaña del Comité Nacional Demócrata. Le puede pasar a cualquiera y es una de las formas más baratas y populares para poner en riesgo un dispositivo o cuenta.

Los humanos verifican a otros humanos a través de la cadencia de sus voces, identificadores visuales, la forma que caminan, las palabras que eligen. Los humanos verifican computadoras y servidores al chequear cuidadosamente la redacción o al doble chequear si la conexión está cifrada correctamente a la computadora que pensamos que nos estamos conectando. Desafortunadamente, somos susceptibles a ignorar lo que en otras condiciones levantaría suspicacias: estamos dispuestos a pasar por alto a intrusos disfrazados como reparadores entrando a nuestro edificio y a sitios web que tengan el seguro verde de HTTPS y se vean producidos profesionalmente.

Algunas maneras de mitigar el phishing es verificando con tu amigo a través de otro canal (como decir en persona o a través de una llamada telefónica) si ellos en realidad te enviaron ese email que se sentía sospechoso antes de abrirlo. Ten presente que sujetos maliciosos compran dominios que se ven similares a dominios genuinos pero a los que los URLs les falta algún caracter. Se cuidadoso y teclea el link tu mismo, o mejor aún ten el sitio genuino marcado como favorito en vez de darle click dentro del email o mensaje. Si el email contiene un archivo adjunto que no estabas esperando, usa algún visor desde el navegador basado en la nube como Google Drive o las funciones de previsualización de Dropbox. Es mejor abrir un archivo en la computadora de alguien más (de Google o Dropbox o algo llamado máquina virtual) que arriesgar la propia. También puedes monitorear por actividad extraña o inicios de sesión desde tus cuentas registrándote para notificaciones de cuenta.

Así que, desafortunadamente, tenemos que estar alerta para mantener nuestros dispositivos limpios y protegerlos de malware, hacer respaldos y actualizarlos regularmente.

La última gran forma de proteger los datos en nuestros dispositivos es usando cifrado de disco completo (un tipo de cifrado en reposo), codificándolo en texto incomprensible para cualquiera menos nosotres. Cifrado de disco completo generalmente no protege tu dispositivo ante malware. No obstante, sí protege los datos de tu dispositivo de acceso físico. Imagina a un ladrón encontrando tu computadora apagada, prendiéndola y viéndose frustrado ya que no pueden acceder a la información adentro de ninguna forma. Muchas computadoras permiten cifrado de dispositivos, tales como FileVault para computadoras Apple o BitLocker para dispositivos Windows. Es prudente sacar tiempo para tu dispositivo, cifrar completamente estas masas de archivos y respaldar tus datos antes de activar cifrado de disco completo. (Puedes cifrar estos respaldos también).

Muchos smartphones modernos permiten cifrado de disco completo por defecto siempre que haya una contraseña. Sin embargo, tener una contraseña y ser cifrado por defecto son procesos distintos y solo porque tienes una contraseña no signfica que tu dispositivo está cifrado. Asegúrate de chequear si tu cifrado completo de disco es algo que tienes que activar manualmente, adicionalmente a establecer una contraseña.

### Lo que nos trae a las contraseñas

¿Usas alguna de estas?
	12345678
	Password
	monkey
	Letmein
	Dragon
	Iloveyou

Las computadoras verifican las identidades de otras computadoras emparejando archivos de clave pública a huellas digitales de clave pública, certificados digitales y firmas digitales. Las computadoras incluso interpretan juegos de identidad desconocida con humanos, pidiéndonos constantemente que probemos que somos quienes decimos que somos. ¿Lo probamos con nuestra biología? ¿Somos lo que sabemos? ¿Podemos probar quiénes somos por un objeto sagrado que cargamos con nosotros?

Sistemas enteros se construyen sobre estas preguntas de autenticación.

A medida que nuestro mundo se vuelve más y más como ciencia ficción, tenemos sistemas que pueden ser desbloqueados por nuestro cuerpo. Probamos quiénes somos a nuestro teléfono escaneando nuestro pulgar o nuestra cara. Estos métodos tienen sus propias deficiencias. Por ejemplo, usar una huella digital para penetrar a un teléfono es más fácil de lo que imaginamos: las huellas digitales pueden ser falsificadas con impresoras y los oficiales de policía visitan morgues para desbloquear dispositivos. Ahora nos vemos forzados a elaborar preguntas sobre cómo la información biométrica es protegida por la ley, especialmente a la hora de desbloquear dispositivos. Muchos agentes de policía tratan los dispositivos protegidos biométricamente como si no estuvieran protegidos del todo.

También está el método de verificar quiénes somos a partir de conocimiento que debe provenir de nuestras mentes. En Las mil y una noches, la entrada a una cueva requiere que alguien provea la frase clave (una frase de dos o más palabras). La entrada se abre solamente a quien diga la frase clave: “ábrete sésamo”.

Nuestros teléfonos, dispositivos, y servicios en línea demandan PINs y contraseñas. Nos piden que respondamos preguntas de seguridad, que usualmente están asociadas a información públicamente consultable, como el nombre de soltera de nuestra madre, la escuela a la que asistimos y nuestras direcciones antiguas. Construimos nuestras contraseñas basándonos en nuestros intereses, nuestras canciones favoritas, palabras cortas como mono o tal vez insertando un ! o un 1, cosas sobre las personas que amamos. Es probable que usemos la misma contraseña a través de múltiples servicios, porque es difícil recordarlos todos. Es probable que utilizemos cumpleaños, es probable que utilizemos la dirección de nuestra casa, es probable que utilizemos cosas que ya hemos memorizado.

En otras palabras, la forma que elegimos nuestras contraseñas es predecible. Y por lo tanto, aprovechable por otros.

Cada vez que hay una brecha en la que “millones de correos electrónicos y contraseñas fueron robadas,” debes asumir que actores maliciosos están tratando de ver cuántas personas están usando la misma contraseña a través de diferentes servicios. Ya que mucha gente reutiliza sus contraseñas, hackers van a ganar acceso a servicios críticos con valiosa información financiera o de otro tipo adentro.

También está la amenaza de aquelles que nos conocen bien: si alguien realmente nos conoce lo suficiente o es capaz de recordar cosas que hemos escrito o dicho en el pasado, elles podrían adivinar qué tipo de contraseña usaríamos. Aún en mi secreto lenguaje inventado sería fácil de crackear por alguien que supiera los mismos lenguajes que yo, o que me conociera muy bien.

Hay listas enteras derivadas de diccionarios, letras de canciones, poemas y referencias de cultura pop, que son empleadas por computadoras que pueden, en fracciones de segundo, probar cada contraseña para ingresar a un servicio. Entre más corta la contraseña y más simple la combinación, lo más posible es que una computadora pueda adivinar correctamente o que una persona maliciosa pueda ganar acceso. En un estudio del 2011 de seis millones de contraseñas por Mark Burnett, 91 por ciento de las contraseñas eran parte de las 1000 contraseñas más comunes.

La interacción de estos problemas a escala necesita que hagamos contraseñas y preguntas de seguridad que sean difíciles para que humanos las adivinen, al igual que para que computadoras lo hagan.

Una contraseña difícil de crackear es larga, única y aleatoria. Por larga me refiero a la extensión de al menos unas cuantas palabras. Por única me refiero a usar contraseñas completamente nuevas para cada servicio (y no solamente una variación de la misma). Por aleatoria me refiero a que no haya conexión entre ninguna de las palabras, números o símbolos entre sí, y que sean idealmente generadas aleatoriamente.

Depende de usted cómo conservar una lista larga, única y aleatoria de contraseñas. Algunas personas les gusta mantenerlas en un cuaderno y guardarlo celosamente. Para el resto de nosotres, hay administradores de contraseñas, como LastPass o 1Password. Un administrador de contraseñas es un software que cifra una base de datos de tus contraseñas, preguntas de seguridad y otra información sensible, y está protegida por una contraseña maestra.

Se preguntarán qué clase de contraseña protege una base de datos protegiendo muchos, muchos otros bits de información sensible. Tal vez les gustaría usar una frase clave, que podría incluir de tres a ocho palabras aleatorias, para proteger esta base de datos. Una frase clave tiene el beneficio de ser difícil de adivinar par acomputadoras (tiene alta entropía, así que hay demasiadas combinaciones posibles para crackear) y difícil de ser adivinada por humanos. También está el beneficio de la memoria: es más fácil para humanos el construir una historia alrededor de estas palabras aleatorias y así recordar las frases claves. Por ejemplo, tú puedes inventar una historia con una frase clave de cinco palabras “boscosa calumnia pantaña champán serpiente” mediante un dispositivo mnemotécnico: “En la zona boscosa, alguien calumnia al teléfono. Prenden la pantalla de su televisión, virtiéndose champán mientras ven una serpiente.”

Para aquellos que gustan de entropía y están interesades en generar palabras clave fuertes, uno de los métodos recomendados es el diceware: una técnica de tirar 5 dados y generar un número de 5 dígitos, luego encontrar la palabra correspondiente de una lista lo suficientemente larga de palabras sin relación, hasta crear una palabra clave de la extensión apropiada. Por ejemplo, usando 5 dados, tiré 4-1-5-2-3. Busqué 41523 en la larga lista de palabras de EFF y encontré su pareja correspontiente: mummify. Tiré los dados cuatro veces más: 3-1-5-6-2, 6-2-5-4-1, 2-3-1-2-3, 3-3-3-3-3. Mi frase clave a partir de diceware es “mummify fructose tribunal despise handgrip.”

Asumiendo que has hecho todo lo anterior, estás en buena forma: has logrado evadir USBs y dispositivos sospechosos, no le permites a nadie conectar nada a tu computadora, has actualizado tu software, has empleado contraseñas aleatorias a través de tus cuentas, has sido cuidadose. Pero digamos que alguien escucha a escondidas mientras compartes una contraseña. Digamos que eres spear-phished.

Hay una cosa principal que puedes hacer, que por lo menos protege tus cuentas individuales en el desafortunado evento de que alguien obtenga tu contraseña. Como un talismán u objeto sagrado que los personajes mitológicos cargan con elles para que otros sepan que son quienes dicen ser, las computadoras confían en otra forma de autenticación (además de contraseñas) antes de brindar acceso a tu atesorada cuenta. La última medida de autenticación es algo que tu tienes, un objeto que cargas contigo.

La autenticación de dos factores depende del principio de “algo que sabes, algo que tienes”. El algo que sabes es tu contraseña, mientras que el algo que tienes es un dispositivo físico u objeto que puede ser usado para verificación de identidad. Muchas redes sociales y servicios de bancos ofrecen autenticación de dos factores, y es una de las formas más efectivas que puedes hacer para proteger tus cuentas. ¿Por qué? Porque es difícil robar remotamente una contraseña y robar un objeto físico a alguien. Esto requiere que el usuario de autenticación de dos factores sea consciente de no perder su objeto.

La autenticación de dos factores se da usualmente en la forma de un token de seguridad que calza en un puerto USB, como Yubikey, que calza en un aro de llaves. Luego de insertar su contraseña, se debe usar el segundo factor: presionar ligeramente el pequeño botón plano del token, lo que provee acceso a tu cuenta.

O el objeto sagrado puede ser un teléfono o una tablet. Puedes bajar una app de autenticación, como Autheticator, que tiene códigos de acceso basados en tiempos que rotan cada 30 segundos. (También está la opción de recibir un mensaje vía SMS, pero como se mencionó en la sección de tarjetas postales, los SMS no están cifrados y pueden ser accedidos por personas que escuchen a escondidas.)

Una buena medida protectiva, si eres alguien que tiene probabilidad de perder su objeto físico o que le sea robado, es escribir en físico los códigos de respaldo – códigos de un solo uso que puedas utilizar si tu método principal de autenticación de dos pasos falla, o si pierdes tu teléfono o token.

Desarrollar buenos habitos de seguridad lleva tiempo, pero la higiene de datos para individuos y comunidades empieza con estos tips.

Las cosas cambian, las cosas se deshacen. Los consejos pueden rápidamente volverse obsoletos por nuevas consideraciones.

Para mantenerse actualizado en consejos, puedes seguir el blog llamado Deeplinks, que presenta análisis de EFF a medida que el paisaje de seguridad y privacidad cambia. Si estás interesado en más consejos sobre seguridad digital – así como de aprender sobre modelaje de amenazas, pseudónimos, Tor, VPNs, y más escenarios avanzados, chequea el proyecto de EFF sobre autodefensa frente la vigilancia. Si estás interesado en enseñar a tu comunidad sobre seguridad digital, puedes usar los recursos de EFF de acompañamiento en educación sobre seguridad.

### ¿En quién podemos confiar para que proteja nuestros datos?

Nuestros dispositivos se puden hacer más dignos de confianza a través de cosas como el cifrado completo de disco o la protección de contraseñas. Nuestras cuentas pueden ser configuradas para requererir mayor verificación, como se hace a través de la autenticación de dos factores. Nos podemos comunicar seguramente con servicios a través de cifrado de banda de transporte como HTTPS. Podemos comunicarnos con nuestros amigues a través de una forma más confiable mediante herramientas de cifrado de extremo a extremos como Signal. Podemos tomar medidas significativas para proteger nuestros datos y para proteger los datos de nuestros amigos.

La confianza se gana laboriosamente.

Otros que almacenan nuestros datos, como corporaciones y gobiernos, deben también proteger lo que tienen nuestros. Ellos, también, deben prometer cifrar nuestros datos en reposo, mantener nuestra información segura en sus computadoras cuando es recolectada. Ellos, también, deben usar contraseñas fuertes y tratar estas protecciones seriamente. Necesitan proveer las formas más básicas de protección, tales como seguridad de banda de transportes y un mandato a no recolectar más información que la que estamos dispuestos a darles. Debemos alentar y aplaudir los servicios que proveen cifrado de extremo a extremo.

Para aquelles de nosotres que somos lo suficientemente privilegiados para hacerlo, hay también una responsabilidad ética de evangelizar y usar liberemente cifrado, ya que provee gran protección para aquelles que realmente lo necesitan.

Las protecciones son dobles. Hay un efecto de onda de la minería de datos: corporaciones y gobiernos pueden buscar tus conversaciones en orden de encontrar las conversaciones de tu amigue. Al usar cifrado, ayudas a proteger la información de aquellos con los que te comunicas. Adicionalmente, estás ayudando a normalizar las prácticas de cifrado en primer lugar: mientras más personas usen cifrado, más se normalizarán los metadatos de usar cifrado. Al incrementar nuestro propio uso de cifrado, somos capaces de ayudar a proveer un manto de protección para personas que son focalizadas, como informantes y periodistas, que de otra forma serían singularizados o sospechados a partir de su uso.

Une niñe podría preguntar, “¿Por qué tu diario tiene un candado? ¿Qué estás intentando ocultar?” El principio no es que hay algo ahí que ocultar, sino que, con ese candado, podemos ser más libres respecto a cómo nos sentimos y cómo interactuamos. Que se siente bien tener algo privado. Que se siente bien escribir en un lenguaje propio.

## Tips

1. Usa HTTPS cuando sea posible. Baja HTTPS Everywhere para hacer tu navegación más segura.

2. Baja Privacy Badger para prevenir el traqueo de terceros a través de sitios web.

3.  Envía mensajes con herramientas cifradas de extremo a extremo como Signal para proteger la información de tú y tus amigues.

4. Chequea la huella pública de tu corresponsal para asegurarte de que son quienes dicen ser.

5. Respalda regularmente sus datos – y chequea que tu computadora está respaldando tus chats cifrados como texto criptado y no texto plano.

6. Regularmente actualiza tu software para parchar brechas de seguridad.

7. No conectes tu teléfono en puertos libres sin un “condón USB” para evadir malware.
