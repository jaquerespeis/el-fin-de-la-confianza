# Las camioneras nos están trayendo las noticias del futuro

(entrevista)

> La periodista Julia Angwin y el artista Trevor Paglen discuten su objetivo
> común de exponer la vigilancia masiva.

Julia Angwin y Trevor Paglen en conversación, moderada por Reyhan Harmanci.

En medio de la arremetida diaria de encabezados demenciales, dos palabras se
encuentran con frecuencia formando un redoble, conectando dos eventos en
apariencia dispares: *seguridad* y *privacidad*. Las redes sociales son una
historia en la que nosotras sacrificamos nuestra privacidad, consciente e
inconscientemente. Este sacrificio con frecuencia está enmarcado como una
elección individual en lugar de una acción colectiva. Violaciones en bancos,
máquinas de seguridad, hospitales, ciudades y compañías de datos como Equifax
exponen la mentira de la seguridad - a saber, que nuestra información está
siempre segura de intrusas. Pero el artista Trevor Paglen y la periodista
Julia Angwin, dos de las mentes más afiladas en estos temas, preferirían no
usar esas palabras.

Con base en Berlín, Paglen, quien tiene un doctorado en geografía de Berkeley,
ha construido su carrera artística ayudando a hacer visible lo oculto - usando
la fotografía para traer nuestra atención a los cables corriendo bajo el
Atlántico que constituyen la infraestructura de internet, por ejemplo, o
reclutando rastreadoras aficionadas para ayudar a encontrar satélites
estadounidenses clasificados y otro tipo de escombros espaciales desconocidos.

Angwin, una periodista ganadora de premios y autora, recientemente dejó
ProPublica para construir una sala de redacción que continuará su trabajo de
hacer responsables a empresas de tecnología por su impacto en la sociedad. Ella
ha revelado, por ejemplo, el racismo y la intolerancia que sustentan los
algoritmos de Facebook, y que le permiten a la compañía vender demográficas
como «odiador de judías» a anunciantes dispuestos. Hablé con Trevor y Julia
en la ciudad de Nueva York acerca de todo esto y más.

**Reyhan Harmanci**: Entonces ustedes dos se conocen.

**Julia Angwin y Trevor Paglen**: Sí.

**RH**: ¿Cuándo se cruzaron sus caminos?

**JA**: Nos conocimos *antes* de Snowden, sí, porque yo estaba escribiendo un
libro sobre vigilancia, y usted estaba haciendo su arte impresionante.

**TP**: Y usted me contactó y yo como, «Usted es alguna persona loca», y no
le escribí de vuelta.

**JA**: Yo le escribí, y yo como, «Quiero usar algunas de sus piezas de arte
en mi sitio web, y ¿cuánto costaría?» y él como, «Lo que sea, usted ni siquiera
es suficientemente "cool"».

**TP**: Ese es uno de los momentos más vergonzosos de mi vida. Ya sabe, recibo
un millón de esos correos electrónicos por día.

**JA**: ¡Yo también! Y los ignoro todos, y un día esa persona estará sentada
al lado mío, y yo como, «Je, lo siento».

El problema con los correos es que todas las personas del mundo le pueden
contactar. Es demasiado, ¿verdad? No es una situación sostenible. Pero sí, nos
conocimos en algún momento de la era pre Snowden, que ahora parece como con un
tipo de tonalidad sepia.

**TP**: ...carretas y pioneras...

**JA**: Exacto. Y lo que es interesante de este mundo es que hemos estado
caminando los mismo caminos. Ha sido un viaje interesante, sólo como
movimiento, ¿verdad? Empezó sobre derechos individuales y vigilancia y el
gobierno, y lindamente se movió hacia la justicia social, y yo creo que es la
trayectoria correcta. Porque el problema con la palabra anteriormente conocida
como *privacidad* es que no es un daño individual, es un daño colectivo.

**TP**: Es cierto.

**JA**: Y enmarcándolo alrededor de individuos y derechos individuales en
realidad, creo que ha llevado a trivializar los problemas. Entonces estoy
realmente feliz con la forma en que el movimiento ha crecido hasta abarcar
muchos más problemas de justicia social.

**RH**: Quiero llegar a por qué ustedes prefieren la palabra *confianza* antes
que *vigilancia* o *privacidad*, dos palabras que yo creo están asociadas con
ambas de ustedes en formas diferentes. ¿Tienen memoria de la primera vez que
llegaron a entender que hay un aparato que les podría estar viendo?

**JA**: Esa es una pregunta interesante. Yo era la niña que creía que mis
progenitoras sabía todo lo que yo estaba haciendo cuando no estaba con ellas,
entonces tenía miedo de romper las reglas. Era tan obediente. Mis progenitoras
pensaban que la televisión era malvada, entonces en las casas de mis amigas
yo como, «No puedo ver televisión». Y ellas como, «¡Pero esta es su única
oportunidad!»

Y he pasado toda mi vida recuperándome de ese nivel de obediencia tratando de
ser tan desobediente como sea posible. Sentía a mis progenitoras como una
entidad que todo lo ve, ya saben, estaba aterrada de eso. Y pienso que
probablemente formó algo de mi paranoia constante, tal vez.

**RH**: Cierto, sí. Yo también era una niña realmente buena.

**JA**: Sí, no está bien, esa bondad.

**TP**: Yo creo que tuve la misma sensación al crecer con estructuras que no
funcionaron para mi, y sintiéndome como que había una profunda injusticia
alrededor de eso. Sintiendo como que el mundo estaba configurado para bajarme
de ciertos caminos e imponer ciertos comportamientos y normas que no
funcionaron para mi, y dándome cuenta que el valor de esta palabra
anteriormente conocida como *privacidad*, por lo contrario llamada
*libertad*, no sólo juega a la escala del individuo, sino también como un tipo
de recurso público que permite la posibilidad de, por un lado, experimentar,
pero entonces, por el otro lado, cosas como libertades civiles y auto
representación.

Uno se da cuenta que para tratar de lograr que el mundo sea un lugar más
equitativo, y un lugar en el que haya más justicia, se deben tener sectores de
la sociedad en los que las reglas puedan ser más flexibles, que puedan ser
impugnadas y retadas. Por eso es que creo que ambas de nosotras no estamos
interesadas en la privacidad como un concepto tanto como el anonimato como un
recurso público. Y creo que hay un aspecto político a eso en términos de crear
un espacio para que la gente ejercite derechos o demande derechos que antes no
existían. El clásico ejemplo son los derechos civiles: hay muchas personas que
rompieron la ley. Igual con el feminismo, igual con ACT UP, igual con toda la
historia de movimientos de justicia social.

Y luego, conforme vemos la intensificación de los sistemas de detección, ya
sea una NSA o Google, estamos viendo esas formas de poder estatal y poder del
capital invadiendo momentos de nuestras vidas diarias que antes no podían
alcanzar. En lo que eso se traduce si usted es una corporación es tratar de
extraer dinero de momentos de la vida a los que antes no tenía acceso, ya sea
mi historial de búsqueda de Google o los datos de ubicación de mi teléfono que
le dicen si voy al gimnasio o no. Hay un conjunto de derechos y libertades de
facto que surgen del hecho que no todas y cada una de las acciones de su vida
diaria tienen consecuencias económicas o políticas. Esa es una de las cosas
que creo que está cambiando, y una de las cosas que está en riesgo aquí.
Cuando se tienen corporaciones o estructuras políticas que pueden ejercer
poder en lugares de nuestras vidas a los que antes no tenían acceso,
sencillamente porque eran ineficientes, eso se suma en una sociedad que va a
ser mucho más conformista y mucho más rígida, y en realidad mucho más rapaz.

**JA**: Estoy de acuerdo.

**RH**: ¿Qué les hace tener aversión a la palabra *privacidad*?

**JA**: Bueno, cuando yo pienso sobre la privacidad, pienso en querer estar
sola. Digo, yo creo que ese es el significado original. Pero la verdad es que
los problemas que estamos discutiendo en realidad no son sobre querer ser
solitaria. En realidad son sobre querer tener la posibilidad de participar en
la maravillosa conectividad que la internet ha traído al mundo, pero sin
renunciar a todo. Entonces privacidad, sí, yo quiero proteger mis datos y, en
realidad, mi autonomía, pero eso no significa que no quiero interactuar con
personas. Yo creo que privacidad solo tiene una connotación de, «Bueno usted
solo es, como, antisocial», y en realidad es lo opuesto.

**TP**: Sí, totalmente. Yo creo que está bien articular el bien colectivo que
viene a través de la creación de espacios en sociedad que no están sujetos a
los tentáculos del capital o el poder político, ¿saben? Similar a la libertad
de expresión, no todas nosotras tenemos alguna gran cosa que queramos decir
con nuestra libertad de expresión, pero nos damos cuenta que es un bien
colectivo.

**JA**: Sí, exacto. Y yo también pienso que *privacidad* se siente muy
individual, y el daño del que estamos hablando también es en realidad un daño
social. No es que no sea una buena intermediaria; yo uso esa palabra todo el
tiempo porque es una abreviatura y la gente sabe de lo que estoy hablando, en
especial desde que escribí un libro con eso en el título. Pero siento que
simplemente no está del todo bien.

**TP**: El otro lado de la *privacidad* es la palabra *vigilancia*, que
ciertamente es una palabra que empezó a ser popular en los 90 cuando las
cámaras de vigilancia fueron instaladas alrededor de las ciudades, y todas
eran como, «Ah, usted está siendo vista por algún tipo en frente de un montón
de pantallas». Pero esas pantallas se fueron, y ese tipo desapareció y ahora
está desempleado porque todo eso ha sido automatizado. El otro aspecto de la
vigilancia, creo yo, es que ha sido históricamente asociada con formas de poder
estatal, y eso ciertamente sigue siendo verdad. Pero con eso es ahora
vigilancia capitalista, que es cómo usted monetiza la habilidad de recolectar
tipos de información muy íntima.

**JA**: Sí, y yo creo que *vigilancia* es una palabra ligeramente mejor para lo
que estamos hablando, pero por la connotación mucha gente la ve sólo como
vigilancia estatal. Eso no abarca todo, y lo que tampoco abarca es el efecto
que tiene. Vigilancia es sólo el *acto* de observar, pero ¿qué le ha hecho a
la sociedad, verdad? ¿Qué le ha hecho a usted? ¿Qué hace cuando no hay
bolsillos en los que pueda tener puntos de vista disidentes, o experimentar con
auto representación en diferentes formas? ¿Cómo se ve? Es en realidad sólo una
forma de control social, y, como usted dijo, una movida hacia la conformidad. Y
entonces eso es, creo yo, por qué la *vigilancia* por sí misma no es una
palabra tan suficientemente agresiva para describirlo.

**TP**: Porque *vigilancia* también implica un tipo de pasividad de parte de la
persona haciendo la vigilancia, y eso ya no es cierto; estos son sistemas
activos.

**RH** ¿Qué significa ser un sistema activo?

**TP**: Un ejemplo súper simple: usted está manejando su auto y hace un giro
ilegal en una luz roja, y una cámara lo detecta, graba su placa y emite una
multa de tránsito - Y no hay ninguna humana involucrada. Usted puede abstraer
eso a que se le niegue el empleo con base en algún tipo de sistema
algorítmico que está haciendo filtrado por palabras claves en currículos.
Julia, mucho del trabajo que usted ha estado haciendo es sobre esto.

**JA**: Sí, quiero decir que, la vigilancia es sólo el inicio del problema. Es
sobre que, una vez que los datos están recolectados, ¿qué van a hacer con
ellos? Cada vez están haciendo más decisiones acerca de nosotras.

Y, extrañamente, parece como que las decisiones que están automatizando primero
son las decisiones humanas con más alto riesgo. El sistema de justicia criminal
está usando estos sistemas para predecir qué tan probable es que usted vaya a
cometer un crimen en el futuro. O hay sistemas que están averiguando si usted
va a ser contratada sólo escaneando currículos. Yo creo que es divertido porque
los sistemas automatizados que la mayoría de personas usan son probablemente
los mapas, y esas cosas apestan. Como la mitad del tiempo le están diciendo que
va para el lugar equivocado, y aún así nosotras como, «¡Ya lo sé! ¡Esta cosa
es asombrosa; déjeme usarla para decisiones que realmente tienen alto riesgo
como si mandar a la gente a la cárcel o no!»

*TP*: ¡O matarles!

*RH*: Yo pienso que la gente muchas veces dice, «Bueno, no estoy haciendo nada
malo,. Tome mi información, véndame cosas. No estoy haciendo bombas en mi
sótano». Pero eso sólo es en apariencia la punta del iceberg.

*JA*: Bueno, esa es una posición muy privilegiada. Básicamente, la gente que
me dice eso es blanca. El número de cosas que son ilegales en este mundo es
realmente alto. Tenemos muchas leyes, y hay ciertas partes de nuestra
comunidad, en su mayoría la gente café, que fueron sujetos de un cumplimiento
mucho más fuerte de estas. Y entonces yo creo que el sentimiento de «Yo no
estoy haciendo nada malo» en realidad es una falacia. Porque, por supuesto,
incluso si le atrapan haciéndolo, usted saldría librada porque así es como
está configurada nuestra bella sociedad. Pero de hecho, hay gente monitorizando los
canales de Twitter de Black Lives Matter, y todas las personas de Ferguson, y
tratando de encontrar algo para atraparles. Y eso es lo que pasa con la
vigilancia - usted puede atrapar a alguien por cualquier cosa.

Cuando yo estaba trabajando en mi libro fui a visitar los archivos de la Stasi
en Berlín, donde tienen los archivos que la Stasi mantenía sobre las
ciudadanas, e hice una solicitud de registros públicos. Obtuve un par de
archivos que incluso están traducidos, y están en mi sitio web. Lo que fue
tan sorprendente para mi fue que la Stasi tuviera tan poco, pero sólo
necesitaban alguna pequeña cosa. Como esta estudiante de colegio que se
escapó de clases un par de veces, o algo, entonces fueron a su casa y le dijeron
como, «Usted debe convertirse en informante, o si no le vamos a castigar».
Todo era sobre convertir a la gente en informantes con base en una diminuta
pizca de algo que tenían. Y yo creo que eso es la vigilancia realmente: es
sobre el efecto escalofriante, la auto censura, en la que usted no está
dispuesta a intentar nada riesgoso. Podrían haber consecuencias muy fuertes,
en particular si usted es parte de un grupo que quieren oprimir.

**TP**: Yo estoy en desacuerdo en el sentido de que yo pienso... Bueno, Reyhan,
usted está embarazada. Digamos que tengo todos sus datos de geolocalización, y
se que visitó una licorera en cierto día. ¿Le vendo esta información a sus
proveedor de seguro médico? Eso va a tener consecuencias en su capacidad de
acceso a seguro médico, o va a significar que tendría que pagar primas más
altas. Y creo que no es sólo un tipo de estructura legalista; es una
estructura acerca de «¿Cuáles son lugares en los que puedo aprovechar
información para extraer dinero de usted, así como determinar si usted se
convierte en parte del sistema de justicia criminal?»

**RH**: A veces tengo problemas para imaginar que todo esto está pasando.

**JA**: Sí parece irreal. De hecho, lo que encuentro tan impactante es que
sentí que pinté la visión más pesimista que pude en 2014, y es mucho peor;
mucho peor de lo que imaginé. Entonces, veamos una cosa sencilla en este país,
y luego una en China.

En este país, estas calificaciones de riesgo de las que he estado hablando
están siendo usadas para arrestarle. Le dan una calificación, 1 a 10, de qué
tan probable es que usted vaya a cometer un futuro crimen, y eso puede
determinar sus condiciones de fianza. Existe este enorme movimiento a lo
largo de la nación para que, en lugar de poner gente en la cárcel - la gente
con el riesgo más alto - sólo les pongan un brazalete electrónico en el
tobillo. Entonces esencialmente una decisión automatizada obliga a alguien a
tener encima, literalmente, un monitor GPS en todo momento. Eso es algo que yo
en realidad no pensé que pasaría así de rápido. Hace tres años yo hubiese
dicho, «No, eso es un poco distópico».

Ahora piense en China: hay una región muy musulmana, y el gobierno chino está
tratando de romperla porque creen que todas son terroristas. Hace poco estaba
con esta mujer de Human Rights Watch quien trabajó en la región y agrupó toda
la información acerca del equipo de vigilancia que trajeron a la región y lo
que estaban haciendo. Y construyeron un programa automatizado, que
básicamente le dice si va a la gasolinera demasiadas veces, o si compra
fertilizante, o si hace cualquier cosa sospechosa. Algo aparece que dice que
usted es de alto riesgo y le envían a un campo de reeducación. Es un sistema
automatizado el que determina quién es enviada a un campo de reeducación. Y
yo como, «Por dios, no puedo creer que esto esté pasando *en este momento*;
¡ni siquiera estamos hablando del futuro!»

*TP*: Este sistema de crédito ciudadano de China ha estado en las noticias un
poquito. Básicamente, los programas piloto, que están en preparación para el
lanzamiento nacional del 2020, pueden monitorizar todo lo que usted hace en
redes sociales, si usted cruza la calle de forma imprudente o paga los recibos
tarde o habla de forma crítica del gobierno, recolecta todos estos puntos de
datos, y le asigna una calificación ciudadana. Si usted tienen una alta,
obtiene, como, descuentos en entradas al cine, y es más fácil obtener una visa
para viajar, y si tiene una más baja, va a pasarla realmente mal. Está creando
mecanismos extremos de aplicación de la ley, y creo que muchas de nosotras aquí
en los Estados Unidos decimos, «Ah bueno, esto sólo puede ocurrir en China
porque tienen una relación diferente entre el estado y la economía y un
concepto diferente de poder estatal». Pero las mismas cosas exactamente están
pasando aquí, sólo que están tomando una forma diferente porque vivimos en un
sabor diferente de capitalismo que el de China.

**RH**: ¿Las ciudadanas de China saben sobre esto?

**JA**: Sí.

**RH**: Entonces estas calificaciones no son un secreto o subterráneas...

**JA**: No, están pensadas como un sistema de incentivos, ¿cierto? En realidad
sólo es una herramienta de control social; es una herramienta para la
conformidad, y probablemente bastante efectiva, ¿cierto? O sea, usted obtiene
premios reales; es muy pavloviana. Obtiene las cosas buenas, o no. Es sólo que
el gobierno nunca antes había tenido herramientas tan granulares. Han habido
formas que el gobierno puede intentar para controlar ciudadanas, ¡pero es
*difícil* tratar de controlar una población! Estas herramientas de vigilancia
son en realidad parte de un aparato de control social.

**RH**: ¿Qué tan buenos son los datos?

**JA**: Esa es la cosa: no importa. Primero, todos los datos apestan, pero eso
no importa. Yo estaba preguntándole a esta analista de Human Rights Watch por
qué no sólo ponían a una persona al azar en el campo de detención, porque es
en realidad sólo algo disuasorio para que todas las demás estén asustadas.
En realidad no necesita ninguna ciencia. Pero de lo que se dan cuenta es que
en realidad están tratando de lograr el buen comportamiento, entonces aunque
cueste dinero construir este algoritmo, al que nadie está probando su
precisión, este crea mejores incentivos. La gente actuará de cierta forma.
Entonces los datos en sí mismos podrán ser basura, pero la gente pensará,
«Ah, si yo trato de hacer cosas buenas, ganaré. Puedo engañar al sistema».
Es todo sobre apelar a la ilusión de que usted tiene algo de control. Que es
en realidad lo mismo que las configuraciones de seguridad de Facebook - están
construidas para darle la ilusión de control. Usted puede juguetear con sus
pequeños botones, y usted estará como, «¡Estoy tan protegida!» ¡Pero no lo
está! Le van a sugerir a su terapeuta como amiga, y le van a descubrir como
persona gay ante sus progenitoras. ¡No lo puede controlar!

**RH**: Yo creo que tengo malos hábitos personales, porque tengo problemas para
creer que mi teléfono realmente le está dando información a Google o Facebook
acerca de dónde estoy. Tengo problemas para creer eso en cierto nivel.

**TP**: ¡Se lo dice!

**RH**: Pero que les importo tanto, incluso como consumidora.

**JA**: Bueno, usted en realidad no les importa; digo, necesitan a todas. Es
más sobre voracidad. Necesitan ser el único lugar al que las anunciantes van,
necesitan poder decir, «Sabemos todo sobre *todas*». Entonces es cierto, no
están sentadas estudiando detenidamente el archivo de Reyhan - a menos que
usted tenga ahí alguien que le odie que podría querer estudiar detenidamente
ese archivo. Pero es más acerca de control de mercado. Es similar al control
estatal en el sentido que, para ganar, necesita ser monopolista. Eso es en
realidad como la regla de negocio. Y entonces ellas - Google y Facebook - están
en una carrera a muerte para saber todo sobre todo el mundo. En eso compiten, y
esa es su métrica. Porque van a las mismas anunciantes y dicen, «No, compre con
nosotras». Y las anunciantes como, «Bueno, ¿me puede dar a la dama embarazada
camino a la licorera? Porque eso es lo que quiero». Y cualquiera que pueda
entregar eso, entra en el negocio. Están en una carrera para adquirir datos,
así mismo como las empresas de petróleo compiten por obtener distintos campos
petroleros.

**TP**: La única cosa que quiero subrayar de nuevo es que usualmente pensamos
acerca de estas como plataformas de publicidad esencialmente. Esa no es la
trayectoria a largo plazo que tienen. Yo pienso en ellas como plataformas para
extraer dinero de su vida.

**JA**: La forma actual en que extraen el dinero es publicidad, pero van a
convertirse - ya se están convirtiendo - en intermediarias, guardianas. Que es
el negocio más grande de todos. «No puede obtener nada a menos de que pase a
través de nosotras».

**TP**: Sí, y seguro médico, crédito...

**JA**: Es por eso que quieren disrumpir todas esas cosas - eso es lo que
significa.

**RH**: ¿Entonces así es como se va a llevar a cabo? ¿Es como cuando una se
registra en un servicio y puede escribir su correo electrónico o darle al botón
de Facebook? ¿Así es como está ocurriendo?

**JA**: Bueno, así es cómo está ocurriendo ahora. Pero usted sabe que Amazon
anunció seguro médico, ¿verdad? Vamos a ver más de eso. Mi esposo y yo
estábamos hablando recientemente acerca de los carros que se conducen solos.
Google entonces también se convertirá en su aseguradora. Entonces van a
necesitar incluso más datos acerca de usted, porque van a disrumpir los
seguros y sólo brindarlos ellas mismas. Saben más acerca de usted que nadie,
¿entonces por qué no deberían aunar el riesgo? Hay muchas formas en las que
sus avenidas van a venir, y para ser honesta, tienen una ventaja competitiva
real porque ya tienen esa reserva de datos.

Y luego también resulta, extrañamente, que nadie más parece ser capaz de
construir buen software, entonces ahí también está esta barrera tecnológica
de entrada, que yo aún no he entendido del todo. Pienso en esto como Ford:
primero, cuando inventó la fabrica, él tenía un par de décadas de ventaja en
que eso era *la* cosa. Y luego, todas construyeron fábricas, y ahora puede
simplemente construir una fábrica de una caja. Entonces creo que la barrera de
entrada desaparecerá. Pero ahora, las compañías aseguradoras - incluso si
tuvieran los datos de Google - no serían capaces de construir el software lo
suficientemente sofisticado. No tienen la tecnología de inteligencia artificial
para predecir su riesgo. Entonces estas compañías tienen dos ventajas, una de
las cuales podría desaparecer.

**TP**: Pero la recolección de datos es una ventaja masiva.

**JA**: Es una ventaja *masiva*.

**TP**: O sea, muchos de estos sistemas simplemente no funcionarán a menos que
tengan datos.

**JA**: Sí, y a veces hablan sobre esto como sus «datos de entrenamiento», que
son todos los datos que recolectan y usan para entrenar sus modelos. Y sus
modelos, por definición, pueden hacer mejores predicciones porque para
empezar tienen más datos. Entonces, ya saben, cuando ven a estas compañías
como monopolios, algunas personas piensan en términos de ingresos por
publicidad. Yo tiendo a pensar acerca de esto en términos de datos de
entrenamiento. Tal vez *eso* es lo que se necesita romper, o tal vez tiene que
ser un recurso público, o algo, porque esa es la cosa que hace imposible
competir.

**RH**: ¿Qué tan buenas son usando estos datos? Muchas veces yo he estado poco
impresionada por los resultados finales.

**JA**: Bueno, eso es lo que pasa con los monopolios: se vuelven perezosos. Es
un claro signo de monopolio, en realidad.

**TP**: Sí, ¿qué tan buena es su agencia de reporte crediticio?

**JA**: ¡Es basura!

**RH**: ¡Totalmente! Todo esto parece como basura, ¿saben? Facebook está
constantemente ajustando lo que me muestra, siempre está mal.

**JA**: Cierto. Por eso es que hubo un anuncio reciente, como, «Ah, van a
cambiar el canal de noticias». No les interesa lo que haya en el canal de
noticias. No importa, usted no es la cliente. No quieren que se vaya, pero
usted tampoco tiene a donde ir, entonces...

**RH**: Bueno. Y es como Hotel California: en realidad nunca va a poder salir.

**JA**: ¡Sí, correcto!

**RH**: Entonces están ocupadas en este masivo esfuerzo a largo plazo para ser
las guardianas entre usted y cualquier servicio que necesite, cualquier momento
que necesite escribir su nombre en cualquier lugar.

**JA**: O sea, en serio, las guardianas entre usted y su dinero. Como, lo que
sea necesario para hacer esa transacción.

**RH**: O hacer que otra gente les de su dinero.

**JA**: Sí, seguro, a través de usted.

**TP**: Tampoco ignoremos totalmente al estado, porque los registros pueden
ser obtenidos con una citación, con frecuencia sin orden judicial. Yo pienso
mucho acerca de Ferguson, en términos de que es un sistema municipal rapaz
entregando, como, decenas de miles de ordenes judiciales para una población de,
como, tantas personas, y toda la infraestructura está diseñada solo para
acechar a toda la población. Cuando usted controla ese tipo de grandes
infraestructuras, ese básicamente es el modelo.

**RH**: Y eso es muy poderoso. ¿Cómo la sed de privatización de la actual
administración juega en este tipo de cosas? Porque puedo imaginar una
situación en la que usted quisiera tener tarjetas de identidad nacional, y
quién mejor para dar ese servicio que Facebook?

**JA**: Ah, ese esfuerzo está en camino. Hay un grupo de trabajo nacional que
lleva cinco años, en realidad, que está buscando que el sector privado
desarrolle formas de autenticar identidades, y las compañías de tecnología
están muy involucradas en esto. La cosa acerca de esta administración que es
interesante es que, por sus fallos para asignar personal en cualquier
posición, hay algunos...

**RH**: ¡Un lado positivo!

**JA**: Sí, ¡algunos de estos esfuerzos están en realidad estancados! Entonces
en realidad esto podría ser un gane, no lo se. Pero las compañías siempre han
estado interesadas en brindar identidad digital.

**TP**: No hay que ver sólo al nivel federal. Pueden ver al nivel municipal,
en términos de ciudades asociadas con corporaciones para hacer multas de
tránsito. O ver a Google en las escuelas. Yo creo que, con la construcción de
estas infraestructuras, hay un tipo de privatización de facto ocurriendo. Apple
o Google irán a las municipalidades; les brindarán computadoras a sus escuelas,
pero van a recolectar todos los datos de las estudiantes que las usan,
etcétera. Luego usarán estos datos para asociarse con las ciudades de otras
formas para cobrar por servicios que antes hubieran sido servicios públicos.
De nuevo, hay muchos ejemplos de este control - tome por ejemplo Vigilant
Solutions. Esta compañía que hace lectura y ubicación de placas de vehículos
dirá, «Bueno, tenemos esta base de datos de placas, y vamos a unir esto con
su base de datos municipal para que pueda usar nuestro programa para
identificar dónde están todas esas personas que le deben dinero, personas que
tienen una orden de arresto y deudas pendientes en la corte. Y vamos a
hacer un cobro de servicio por eso». Ese es el modelo de negocio. Yo creo que
es una visión de la municipalidad en el futuro en cada sector.

**JA**: Saben, ese es un gran ejemplo porque Vigilant en realidad es,
básicamente, una recolectora de deudas. Entonces empieza esencialmente como
rapaz. Todas estas recolectoras ya están manejando alrededor de las ciudades
de noche, buscando los autos de la gente que las comerciantes están embargando
porque la gente no ha pagado. Luego se dan cuenta, «Ah, el gobierno en realidad
amaría una base de datos nacional con las placas de todo el mundo», entonces
empiezan a escanear con estos lectores de placas automatizados en sus autos.
Entonces las recolectoras a lo largo del país salen cada noche - yo creo que
fue en Baltimore donde la competencia de Vigilant tenía cuarenta autos saliendo
cada noche - a tomar fotografías de dónde está estacionado cada auto y
poniéndolos en una base de datos. Y luego construyen estas a lo largo del país,
y ellas como, «Ah, DHS, ¿quiere saber dónde está un auto?» y luego hacen una
oferta por un contrato. Y entonces agregan la capa del gobierno y luego
vuelven a agregar la parte rapaz de esto, que es... realmente espectacular.

**RH**: Pero si habla con una ejecutiva de Vigilant, sería como, «¿Cuál es el
problema? Sólo estamos brindando un tipo de servicio».

**JA**: Sí, yo gasté un montón de tiempo con sus competidoras con base en
Chicago, me reuní con un montón de recolectoras muy agradables que dijeron,
«Vea, estamos haciendo el trabajo del Señor. O sea, estas son personas malas,
y tenemos que atraparlas». Y luego yo decía, «¿Pueden buscar mi auto?» y
*pum*, presionaban un botón, y ahí estaba, ¡justo en la calle en frente de mi
casa! Yo como, mierda.

**TP**: Vaya.

**JA**: Eso es tan escalofriante.

**RH**: ¡Eso es una locura! Es el tipo de cosa en la que, de nuevo, diríamos
como, «Nadie sabe donde está mi auto; ¡Yo no se donde está mi auto! Joder que
no lo van a encontrar», ¿saben?

**JA**: Cierto.

**TP**: Pero podrían llevarlo más lejos. Piensen acerca de la nueva generación
de autos Ford. Ellas saben exactamente en dónde está su auto en todo momento,
entonces ahora Ford se convierte en una gran compañía de datos brindando datos
de geolocalización.

**RH**: Y ni siquiera van a ser dueñas de su auto ya que el software ahora es
propiedad del fabricante.

**TP**: Se remonta a eso de si van a licoreras, si van con exceso de velocidad.
Mañana, Ford podría hacer un programa para aplicación de la ley que diga,
«Cada vez que alguien va con exceso de velocidad, en cualquier jurisdicción
que esté, usted le puede hacer una multa si se registra en nuestro...» ¿Saben?
O sea, este es un programa conjunto completo de aplicación de la ley de
tránsito.

**JA**: Y una cosa que quiero decir, porque tengo que plantear el asunto del
debido proceso por lo menos una vez durante cualquier conversación como esta.
Mi esposo y yo peleamos sobre esto todo el tiempo: él como, «Quiero que sepan
que estoy manejado de forma segura porque estoy seguro, y estoy feliz de tener
el GPS en el auto, y soy asombroso, y me voy a beneficiar de este sistema».
Pero el problema, cuando piensan acerca de las cámaras en los semáforos y
las multas automáticas, que ya se están volviendo bastante generalizadas, es
realmente un asunto de debido proceso. ¿Cómo se apela? Probablemente sería
realmente difícil de hacer. Como, ¿vamos a tener que instalar cámaras en el
tablero para vigilarnos a nosotras mismas para tener algún tipo de protección
contra el estado que nos está vigilando, o quien sea que nos está vigilando?
Pienso que muchos de los problemas planteados por este tipo de cosas son en
realidad acerca de, odio decirlo, pero son nuestros derechos individuales.
Como acerca del hecho de que no se puede apelar a estos sistemas.

**TP**: Eso es verdad. Hay muchos derechos de facto que hemos tenido porque los
sistemas de poder son ineficientes. Usando una estructura de justicia criminal
en la que esta es la ley, este es el límite de velocidad, y si lo rompe está
rompiendo la ley - así no es en realidad cómo hemos vivido históricamente.

**JA**: Correcto.

**TP**: Y eso es muy fácil de cambiar con estos tipos de sistemas.

**RH**: Sí, porque entonces si están buscando fijar a alguien como objetivo,
ahí es donde los datos son convertidos en armas. Y también, hay mucha
incompetencia en los cargos de justicia criminal, y en todos estos sistemas
humanos. Tal vez lo rescatable ha sido que las humanas sólo pueden procesar
cierta cantidad de información al mismo tiempo.

**TP**: Yo creo que lo divertido es que han sido los gremios policiales los
más vocalmente opuestos a instalar cosas como cámaras de semáforos porque
*quieren* esa ineficiencia.

**RH**: Sí, y dicen, «Estamos haciendo juicios de valor».

**JA**: O sea estaba esta loca historia - saben de las camioneras, ¿verdad?
Las camioneras están siendo vigiladas fuertemente por sus empleadoras. Muchas
de ellas son trabajadoras independientes, pero están estas reglas federales
sobre cuantas horas se puede manejar. Es uno de esos ejemplos clásicos - más o
menos como la asistencia social, en donde usted en realidad no puede comer lo
suficiente con la cantidad de dinero que le dan, pero es ilegal trabajar extra.
Entonces de forma similar, para las camioneras, la cantidad de horas que
necesitan manejar para ganar dinero son más que las horas que se les permite
manejar. Y para asegurarse que las camioneras no se pasen de estas millas, han
agregado todos estos monitores GPS a los camiones, y las camioneras están
involucradas en estas enormes estrategias de contravigilancia de, como, rodar
millas hacia atrás, y hacer todas estas cosas locas. Y pasó esta cosa
asombrosa un día en Newark, hace un año o dos, en donde la torre de control
perdió contacto con los aviones en el aeropuerto de Newark, y resultó ser por
un camionero. ¡Él estaba manejando y tenía una antivigilancia tan fuerte
para su monitor GPS que interrumpió el tráfico aéreo!

**TP**: ¡Vaya!

**RH**: Salvaje. Dios, hace una década en el BART, y yo nunca hago esto, pero
estaba hablando con este tipo a mi lado y él era un conductor de camiones. Él
me estaba diciendo acerca de la vigilancia en ese momento. Y él como, «Esto
ya ha estado pasando». ¡Y eso fue hace diez años! Dijo que los primeros tipos
de autos autoconducidos van a ser camiones.

**JA**: Sí, es correcto, van a ser camiones.

**RH**: Sólo recuerdo salir del BART y quedarme como, «¿Qué tal si eso fue un
oráculo o algo?

**JA**: Las camioneras nos están trayendo las noticias del futuro.

**RH**: Entonces esta conversación está siendo enmarcada como «el fin de la
confianza», pero ¿alguna vez *tuvimos* confianza?

**JA**: No estoy segura de estar de acuerdo con «el fin de la confianza». Pero
estoy más con ver esto ahora como una subcontratación de la confianza. Entonces
esencialmente creo que en esta búsqueda sin fin por eficiencia y control hemos
escogido - y a veces creo que es algo bueno - confiar en los datos por encima
de las interacciones humanas. Estamos en este enorme debate, por ejemplo,
acerca de las noticias falsas. Y hemos decidido que todo es un problema de
Facebook. Ellas necesitan determinar qué es real y qué no. ¿Por qué
subcontratamos la confianza de todo lo que es verdadero en el mundo, todos los
hechos, a Facebook? ¿O a Google, al algoritmo de noticias? Eso no tiene
sentido alguno.

Y entonces en la agrupación de datos, también de alguna forma hemos regalado
muchas de nuestras métricas de confianza, y eso es en parte porque estas
compañías tienen tantos datos, y tienen un fuerte argumento de que pueden
hacerlo con la mate, y van a ganarlo con la IA, van a traernos predicciones
perfectas. Lo que realmente me preocupa es el hecho que hemos roto los sistemas
humanos de confianza que entendíamos. Hay estudios que muestran que cuando
usted ve a una persona, usted establece confianza con ella, como, en un
milisegundo. Y puede saber quién no es digna de confianza de inmediato. Incluso
no puede articularlo; sólo lo sabe. Pero en línea y a través de datos,
simplemente no tenemos esos sistemas, entonces es realmente fácil crear cosas
falsas en línea, y no sabemos cómo construir un sistema confiable. Eso es lo
más interesante para mi acerca del momento en el que vivimos.

Creo que estamos en un momento en el que podríamos reclamarlo. Podríamos
decir, «Esto es periodismo, y el resto no». Vamos a declarar de forma
afirmativa qué es confiable. Siempre hemos tenido guardianas de la
información, ¿cierto? Las maestras dirían, «Lean esta enciclopedia, ahí es
donde están los hechos». Ahora mis niñas vienen a casa y sólo buscan las cosas
en la internet. Bueno, esa no es la forma correcta de hacerlo. Creo que estamos
en un periodo en el que no hemos entendido cómo establecer confianza en línea,
y hemos subcontratado a todas estas grandes compañías que no tienen nuestros
intereses en el corazón.

**RH**: En términos del «fin de la confianza», ¿qué se siente amenazante, o
que sea valioso crearle una acción colectiva al rededor?

**TP**: Para mi, es la capacidad de infraestructuras, si son privadas o de
control, para obtener acceso a momentos de su vida diaria mucho más allá de lo
que eran capaces hace veinte años. No es un cambio en especie, sólo es la
capacidad del capital y la policía de literalmente colonizar la vida en formas
mucho más expansivas.

**RH**: Ambas de ustedes están involucradas en trabajo que trata de hacer estas
cosas visibles - y es muy difícil. Las compañías no quieren que lo vean. Mucho
de su trabajo, Trevor, ha sido acerca de lo visceral, y su trabajo, Julia, ha
sido acerca de decir, «Hemos probado estos algoritmos, y podemos mostrarles
cómo Facebook escoge mostrar un puesto de trabajo a una persona de veinte años de edad,
y otro a una de cincuenta». ¿Qué desean que pudieran mostrarle a las personas?

**TP**: Hay límites muy claros en las políticas para hacer visibles estas
cosas. Sí, seguro, es el primer paso, y es útil porque desarrolla un
vocabulario que se puede usar para tener una conversación. El trabajo que
usted hace, Julia, crea un vocabulario para que nosotras podamos hablar acerca
de la relación entre Facebook y el debido proceso, en la que antes no teníamos
el vocabulario, en serio, ni siquiera para tener una conversación comprensible.

Yo creo que es sólo una parte de un proyecto mucho más grande, pero por sí sola
no hace nada. Es por eso que tenemos que desarrollar un sentido de cívica
diferente, en serio. Un sentido más consciente de cuáles partes de nuestra vida
diaria queremos someter a estos tipos de infraestructuras, y cuáles no. Hay
cosas buenas que se pueden hacer, por ejemplo, con inteligencia artificial,
como motivar la eficiencia energética. Entonces creo que necesitamos decir,
«Bueno, estas son las cosas que quiero optimizar, y estas son las cosas que no
quiero optimizar». No debería quedar todo en manos del capital, y de la
policía, y de la seguridad nacional del estado para decidir cuáles van a ser
los criterios para esas optimizaciones.

**JA**: Yo creo fuertemente que para resolver un problema, hay que
diagnosticarlo, y que todavía estamos en la fase de diagnóstico de esto. Si
piensan acerca del cambio de siglo y la industrialización, tuvimos, no se,
treinta años de trabajo infantil, horas laborales ilimitadas, terribles
condiciones laborales, y tomó un montón de escándalos periodísticos y activismo para diagnosticar el problema y tener un entendimiento de lo que era, y
luego el activismo para hacer que cambiaran las leyes.

Yo siento como que estamos en una segunda industrialización de la información
de datos. Creo que algunas la llaman la segunda era de la máquina. Estamos en
una fase de sólo despertar de la emoción embriagadora y euforia de tener
acceso a la tecnología a nuestro alcance en todo momento. Eso en realidad han
sido nuestros últimos veinte años, esa euforia, y ahora estamos como, «Esperen,
parece que hay algunas desventajas». Yo veo mi rol como intentar hacer esas
desventajas tan claras como sea posible, y diagnosticarlas de forma realmente
precisa para que puedan ser resolubles. Eso es trabajo difícil, y se necesita
muchas más personas haciéndolo. Cada vez más se está convirtiendo en un campo,
pero no creo que estemos allá aún. Una cosa que en realidad no hemos
abordado: suena realmente trillado, pero no hemos hablado del lavado de
cerebros. La verdad es que los humanos son realmente susceptibles. Nunca antes
habíamos estado en un mundo en el que las personas pueden ser expuestas a
cada idea horrible en el universo. Parece que, por lo que se ve ahí afuera,
las personas son fáciles de convencer de cosas bastante idiotas, y no sabemos
lo suficiente acerca de eso, de esa sicología cerebral. Lo que es interesante
es que las compañías de tecnología han estado contratando algunas de las
mejores personas en ese campo, y entonces también es un poco perturbador que
podrían saber más de lo que la literatura académica ha presentado.

**RH**: Que loco.

**JA**: Lo se, odio decir *lavado de cerebros*. Probablemente hay una mejor
palabra para eso. *Persuasión cognitiva* o algo.

**RH**: O solo entender a los humanos como animales, y estamos recibiendo estas
señales animales.

**JA**: Sí, correcto. O sea, tenemos esas economías del comportamiento. Eso ha
dirigido un poco el camino, pero simplemente no ha llegado tan lejos como lo
que se ve con la espiral de motores de recomendaciones, por ejemplo, en
Facebook, donde busca una cosa relacionada con las vacunas y le lleva a
todas las teorías de conspiración anti vacunas y de estelas químicas, que
termina tomando agua directo de la alcantarilla al final.

**RH**: Cierto, gasta cuarenta dólares en una Rainbow Grocery en San Francisco
comprando su agua pura.

**JA**: Es un camino asombroso. La gente se radicaliza a través de este tipo
de cosas, y nosotras simplemente no entendemos lo suficiente acerca de medidas
contra esto.

**RH**: Sí, eso es realmente aterrador. ¿Ustedes interactúan con gente joven?
¿Ellas les dan algún sentido de esperanza?

**JA**: Mi hija es asombrosa. Tiene trece años. En realidad tengo grandes
esperanzas en la juventud porque ella realmente sólo habla en Snapchat,
entonces está completamente involucrada en que todo sea efímero. Y su
curación de su personalidad pública es tan agresiva y tan asombrosa. Ella
tiene, como, cinco fotos en su Instagram, duramente curadas, y tiene, por
supuesto, cinco Instagrams diferentes para círculos diferentes de amistades.
¡Es bastante impresionante! O sea, toma mucho tiempo, y estoy triste por ella
por que tenga que gastar ese tiempo, porque a esa edad yo todo lo que vestí
era literalmente rosado y turquesa por dos años completos y no hay
documentación de eso. Como, nada. Era la cumbre de Esprit. Y yo sólo puedo
decírselo, y nunca podrán verlo. Ella siempre tendrá ese rastro, lo que es
decepcionante, pero yo también aprecio que ella está consciente de eso y ella
sabe cómo manejarlo aunque sea una tarea triste, creo.

**RH**: Sí, siempre me fascinan los hábitos de higiene computacional de las
adolescentes.

**JA**: Creo que son mejores que los de las adultas.

**RH**: Me da curiosidad acerca de las cosas de IA. Hay mucho «hype» alrededor
de la IA, pero todo lo que realmente veo de eso son fotos deformes en Reddit
o algo así. Y yo, «estamos entrenándoles para reconocer fotos de perros»,
¿saben? ¿Cuál es el objetivo final ahí? ¿Esta tecnología va a formar
poderosamente nuestro futuro en una manera atemorizante y oculta? ¿O sólo es
un montón de trabajo, un montón de dinero y un montón de tiempo para aun
resultado que, francamente, no es tan impresionante?

**TP**: Sí, hay fotos deformes en Facebook y Reddit y ese tipo de cosas - eso
es gente como nosotras jugando. Eso no es en realidad lo que está
pasando bajo el capó. Eso no es lo que está dirigiendo a esas compañías.

**JA**: Sólo quiero que retrocedamos un poco porque la IA ha sido definida de
una forma muy limitada. IA sólo solía significar toma de decisiones
automatizada. Luego recientemente ha sido definida mayoritariamente como
aprendizaje automático («machine learning»). El aprendizaje automático está
en sus etapas tempranas, entonces ven que cometen errores. Google
caracterizando rostros negros como gorilas porque tenían datos de
entrenamiento de mierda [sesgados racialmente]. Pero de hecho, la amenaza
de la IA es, para mí, más amplia que sólo la parte de aprendizaje automático.
La amenaza es en realidad esta idea de dar la confianza y toma de decisiones
a programas a los que les hemos puesto algunas reglas y sólo los hemos
soltado. Y cuando introducimos esas reglas, introducimos nuestros sesgos.
Entonces, en esencia, hemos automatizado nuestros sesgos y los hemos hecho
inescapables. Ya no hay debido proceso para luchar contra esos sesgos.

Una cosa realmente importante es que nos estamos moviendo hacia un mundo en el
que algunas personas son juzgadas por máquinas, y esas son las pobres y más
vulnerables, y algunas personas son juzgadas por humanos, y esas son las ricas.
Y entonces esencialmente sólo algunas personas son clasificadas por IA - o por
lo que sea que lo llamemos: algoritmos, decisiones automáticas - y luego otras
personas tienen vía rápida. Es como la pre verificación de TSA.

**TP**: Salió un artículo reportando que Tesco, la empleadora privada más
grande de Reino Unido usa IA tanto como es posible en su proceso de
contratación. Mientras que esta tarde yo tuve que formar parte de un comité
contratando a la nueva directora de nuestra organización: todo son personas.

**JA**: Sí, cierto, exactamente. Es casi como dos mundos separados. Y eso es
lo que creo que es más perturbador. Yo he escrito sobre sesgos dentro de la
tecnología de IA, pero la cosa es, esos sesgos sólo están dentro de un grupo
de personas que están siendo juzgadas por esta, lo que ya es un subconjunto.

**TP**: Mi amiga tenía una cita graciosa. Estábamos hablando acerca de esto
y ella dijo, «La singularidad ya ocurrió: se llama neoliberalismo».

**JA**: Ah sí, esa es la cita más grande de todos los tiempos. Diré esto,
sólo porque quiero quedar registrada diciéndolo: Yo en realidad no tengo
paciencia para las discusiones sobre singularidad porque lo que hacen - sólo
para ser realmente explícita acerca de esto - es mover la bola hacia allá,
de forma que a una no se le permite preocuparse acerca de nada de lo que está
pasando ahora en la vida real. Son como, «Usted tiene que esperar hasta que
los robots asesinen humanos para preocuparse, y no tiene permitido preocuparse
de lo que está pasando ahora con la distribución de la asistencia, y la
clasificación de las "pobres"», - que es en realidad todo lo que son esos
sistemas automáticos: esta idea de que hay algunas personas que merecen ser
pobres y algunas otras que no. Moralmente, esto ya es una construcción falsa,
pero entonces hemos automatizado esas decisiones y son incontestables.

**RH**: Entonces una podría atravesar todo el sistema y nunca ver a un humano,
pero todos los sesgos humanos ya habrán estado explícitamente incrustados.

**JA**: Sí, está este gran libro que acaba de salir llamado *Automatizando la
inequidad*, por Virginia Eubanks, que recomiendo mucho.

**RH**: ¿Podrían ustedes dos hablar un poco sobre su higiene de seguridad
personal?

**TP**: Bueno, la gente siempre es como, «Nosotras escogemos dar nuestros
datos a Google». No, no lo hacemos. Si quiere tener su trabajo, si quiere
tener mi trabajo, tiene que usar estos dispositivos. O sea, es sólo parte
de vivir en sociedad, ¿cierto?

**JA**: Correcto.

**TP**: Tengo a alguien que trabaja para mi quien escoge no hacerlo, y es
un completo, absoluto dolor de cabeza cuando literalmente todo el estudio
se tiene que adaptar a esta única persona que no tiene un teléfono
inteligente. Vale la pena porque son realmente talentosas, pero al mismo
tiempo, en cualquier otra compañía, olvídelo. Estos son tan parte de nuestras
infraestructuras diarias que no tenemos la capacidad individual para escoger
usarlos o no. Incluso si no usa Facebook, ellas aún tienen sus cosas.

**JA**: Sí, estoy de acuerdo. No quiero dar la impresión de que hay alguna
elección. La razón por la que escribí mi libro desde el relato en primera
persona de tratar de abandonar todas estas cosas fue para mostrar que no
sirvió. Y no estoy segura que todas entendieron esa imagen, entonces sólo
quiero expresarlo de una forma realmente clara. Yo tomo pequeñas medidas
aquí y allá en lo rincones, pero, no, vivo en el mundo, y ese es el precio
de vivir en el mundo ahora. Quiero terminar en la nota que esto es un
problema colectivo que no se puede solucionar individualmente.

**RH**: Cierto, como, descargar Signal es genial, pero no va a resolver los
problemas más grandes.

**TP**: Exactamente. Yo creo que una conclusión importante de esta
conversación es que estos problemas históricamente han sido articulados en
la escala individual, y eso no tiene sentido con la infraestructura en la
que estamos viviendo actualmente.

**JA**: Cierto. Pero descarguen Signal.

***

Probabilidad de que, una vez detenida, una conductora negra sea registrada
comparada con una conductora blanca: 3:1

Fuente: National Registry of Exonerations