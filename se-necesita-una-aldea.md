# Se necesita una aldea, *por Camille Fassett*

(ensayo)

> Un movimiento que crece está luchando por la supervisión ciudadana de la vigilancia policial en sus comunidades.

En julio de 2017, residentes de un barrio de Boston vieron a oficiales de policía volando un dron cerca de una urbanización.
My'Kel McMillen, residente de Jamaica Plain, lo vio por sí mismo.
Él tomó fotografías que claramente mostraban al dron, los carros del escuadrón del Departamento de Policía de Boston (BPD, por sus siglas en inglés) y oficiales en uniforme, y las envió a su grupo local de ACLU.

Kade Crockford, Director del Programa de Tecnología para la Libertad del ACLU de Massachusetts, quedó impactado.
Días después, Crockford hizo una solicitud de registros públicos con el BPD para ver por qué oficiales estaban volando un dron de vigilancia en un área residencial, predominantemente negra.

El departamento respondió que, aunque habían comprado tres drones, ninguno había sido volado ese día.
Afirmaron que los drones no habían sido sacados de sus cajas.
A pesar de las numerosas fotografías, testigas y artículos noticiosos acerca del incidente, el departamento repitió su afirmación de que los drones nunca habían sido usados.

«No había información comunicada acerca de que habrían drones en mi comunidad, y ningún aviso de parte del Departamento de Policía de Boston de que los estarían probando», dijo McMillen.
«Nadie sabía qué era lo que estaban filmando o grabando».

En 2015, el BPD luchó contra solicitudes de registros públicos de dos individuos que pedían información relacionada con el uso del departamento de tecnologías de vigilancia de teléfonos celulares antes de que finalmente confirmaran que sí poseían tal sistema.
Antes de la solicitud de Crockford, el BPD no había revelado que tenía drones, o incluso que estuviera considerando comprarlos.
No tenía un requisito legal para hacerlo.

Como el BPD, otras agencias policiales están adquiriendo rápidamente nuevas tecnologías de vigilancia, y en muchos casos no tienen el requisito de obtener la aprobación del gobierno local o incluso de informar al público.

Cuando el departamento de policía en San José, California, compró un dron, no informó al público ni antes ni después de la compra.
Usó cerca de siete mil dólares de subvención monetaria del gobierno federal para comprarlo - dinero que era para ayudar al escuadrón de bombas del departamento al evaluar amenazas tales como explosivos.
Con frecuencia los departamentos evitan el escrutinio y socavan los procesos democráticos locales al obtener nuevos equipos policiales a través de fondos federales.
El Departamento de Seguridad Nacional, por ejemplo, tira millones de dolares en subvenciones monetarias para las agencias policiales locales.

Drones, o sistemas aéreos no tripulados, pueden llevar sensores de calor y cámaras, incluyendo infrarrojas.
Algunos pueden rastrear personas y vehículos a altitudes de veinte mil pies.
La policía los usa para propósitos de rutina, tales como localizar personas perdidas y evaluar daños a casas, puentes, cables de electricidad e instalaciones de petróleo y gas natural después de desastres naturales.
Pero algunas han propuesto usarlos también para registrar violaciones de tránsito y para monitorear protestas.
Un proyecto de ley en Illinois, que por suerte fue detenido por el momento, habría permitido que la policía despliegue drones armados con programas de reconocimiento facial a cualquier reunión de más de cien personas.

Las agencias policiales tienen el requisito de obtener una orden para usar drones sólo en aproximadamente un tercio de los estados.
Y por su gran cantidad de capacidades tecnológicas, tales como volar mucho más alto que los límites de la vista humana, los drones pueden ser usados para espiar a estadounidenses sin su conocimiento, incluso cuando una orden es necesaria.

Pero el cambiante panorama de alta tecnología electrónica para la vigilancia va mucho más allá de los drones.
Siendo tal vez el equipo de espionaje más común usado por los departamentos de policía, las cámaras de vigilancia pueden permitir a la policía monitorear a la gente virtualmente en todo lugar.
Varios departamentos de policía también están permitiendo que negocios privados y residencias les den acceso a las grabaciones de sus cámaras, expandiendo la vigilancia policial a espacios privados.

Algunas agencias han comprado lectores automáticos de placas de automóviles (ALPRs por sus siglas en inglés), que capturan el número de cada placa por la que pasan, y las enlazan a información como la ubicación, fecha y hora en que fueron registradas.
Aunque los datos recolectados por ALPRs no están enlazados al nombre de una persona, esta información puede revelar patrones de conducción identificables, qué vehículos visitaron una ubicación en particular, y conductoras asociadas.
Muchos de estos datos se consolidan en una base de datos que puede ser accesada por agencias a lo largo del país.

Los programas de reconocimiento facial podrían ser utilizados para identificar personas a partir de fotos, en tiempo real o incluso cuando el tráfico está detenido.
Y sistemas con sensores de audio, con frecuencia vendidos bajo la marca ShotSpotter, alertan a la policía del sonido de disparos.
Para hacer esto, los sistemas necesariamente escuchan en todo momento.

Los simuladores de sitios celulares - llamados comúnmente «Stingrays» - son dispositivos de vigilancia particularmente controversiales que imitan torres de telefonía celular de forma que la policía pueda identificar y rastrear los dispositivos celulares cercanos sin siquiera involucrar a las compañías telefónicas.
Algunos pueden recolectar información incluyendo datos únicos acerca de un dispositivo específico e incluso el contenido de llamadas o mensajes no cifradas.

Estas tecnologías, en particular cuando se combinan, pueden aumentar de forma exponencial la capacidad de los departamentos policiales para vigilar a sus ciudadanas.
Por ejemplo, los drones pueden llevar otros tipos de tecnologías de vigilancia - como simuladores de sitios celulares y cámaras con programas de reconocimiento facial - amplificando las capacidades de ambos.

Estos vastos poderes, en gran parte faltos de supervisión, son una carga desproporcionada para personas que ya están en desventaja.
En 2012, el Departamento de Policía de Nueva York capturó los números de las placas de cada carro estacionado cerca de una mezquita.
El Servicio de Inmigración y Control de Aduanas de los Estados Unidos (ICE, por sus siglas en inglés) tiene acceso a una base de datos nacional que le permite a la agencia rastrear inmigrantes.
La vigilancia masiva indiscriminada impacta de forma específica a personas que visitan lugares sensibles, como mujeres que visitan clínicas de salud reproductiva, y aquellas que ya eran un blanco para la policía - en gran parte personas musulmanas, negras y cafés.

Pero a lo largo de los Estados Unidos, comunidades preocupadas por la vigilancia secreta están luchando para cambiar esto, promulgando legislación local que requeriría transparencia y rendición de cuentas en la adquisición y uso de tecnologías de espionaje por la policía.

El condado de Santa Clara en Silicon Valley y Seattle hicieron historia al convertirse en las primeras jurisdicciones de los Estados Unidos en promulgar decretos de vigilancia, en 2016 y 2017 respectivamente.
En ambas regiones, las agencias ahora deben involucrar al público, obtener aprobación de oficiales antes de adquirir nuevas tecnologías de vigilancia, y desarrollar políticas de uso que incluyen protecciones de libertades civiles.

El supervisor del condado de Santa Clara, Joe Simitian, propuso la legislación de Santa Clara, que asigna como mandato el reporte anual de vigilancia que detalla cómo fueron implementadas las tecnologías durante el año.
También requiere que tecnologías existentes que fueron adquiridas antes de que la ordenanza fuera adoptada tengan políticas de uso.

Por ejemplo, el reporte de vigilancia de enero de 2018 acerca del uso en oficiales de cámaras que se llevan en el cuerpo (BWC, por sus siglas en inglés) entra en detalle acerca de los costos y datos compartidos por la tecnología.
Incluye la nota de que «hubieron 861 archivos de evidencia» compartidos con la oficina de la fiscal de distrito ese año, y «cuatro solicitudes del Acta de Registros Públicos de California por imágenes de BWC».
Una vez aprobados, los reportes de vigilancia quedan disponibles en línea para que cualquier persona miembro del público la vea.

«La ordenanza no prohíbe la adquisición o el uso de ninguna tecnología, sino que requiere una discusión considerada y minuciosa, y las políticas son aprobadas por la junta de supervisores antes de adquirirla», dijo Simitian.

Justo como en Boston, en 2010 la ciudad de Seattle adquirió drones sin un proceso público y sin articulación de ninguna política gobernando su uso y retención de datos.
La fuerte oposición que resultó cuando la compra de dos drones se hizo pública no solo llevó a detener el programa de drones, sino que también inició esfuerzos hacia la creación de lo que se convertiría en una de las primeras ordenanzas de vigilancia del país.

Shankar Narayan, Director del Proyecto de Tecnología y Libertad para el ACLU de Washington, dijo que la primera versión de la ordenanza que fue aprobada en Seattle más tarde ese año no fue efectiva, dejando lagunas, y que le hacía falta mecanismos de aplicación.
Él cree que la segunda versión de la ordenanza, que fue pasada en 2017 a pesar de la resistencia de la policía, contiene lenguaje más fuerte que remedia estos problemas y que define claramente la tecnología de vigilancia.

La legislación dice que pretende «gobernar la adquisición y el uso de equipo de vigilancia» y que el uso de la ciudad de tecnologías de vigilancia «debería incluir pasos específicos para mitigar las preocupaciones de libertades civiles y los riesgos de compartir información con entidades como el gobierno federal; y debería incorporar principios de equidad racial en tales protocolos».

Intentos futuros de las agencias de Seattle para adquirir cualquier nueva tecnología (excepto cámaras) que sería usada principalmente para observar o monitorear ciudadanas «de una forma que razonablemente levantará preocupaciones acerca de libertades civiles, libertad de expresión o asociación, equidad racial o justicia social» se someterán a un proceso riguroso de aprobación que incluye aportes de la comunidad.
La revisión por el Consejo de la Ciudad de toda la tecnología de vigilancia que posee Seattle empezó en marzo de 2018.

Las ordenanzas del Condado de Santa Clara y de Seattle son poderosos ejemplos de cómo las jurisdicciones nacionales pueden dirigir la vigilancia masiva de forma proactiva y mantener transparencia.
Desde su implementación, varias regiones han considerado legislaciones similares.

Estos esfuerzos son parte de la campaña de ACLU para el Control Comunitario de la Vigilancia Policial (CCOPS, por sus siglas en inglés).
Lanzada en Setiembre de 2016, es una asociación entre ACLU y otras organizaciones nacionales, activistas y miembras de la comunidad, muchas luego de pelear por un lugar en la mesa del proceso de toma de decisiones policiales por años.

La campaña CCOPS ha creado un proyecto de ley modelo para ciudades interesadas en pasar legislación que aplique transparencia y principios de empoderamiento público para los procesos de adquisiciones en las agencias de gobierno.
El proyecto de ley modelo fue desarrollado por una coalición de grupos de libertades civiles cerca de 2014, y ha sido mejorado a lo largo de los años en respuesta de la retroalimentación de diferentes ciudades que lo han adoptado, según dice Tracy Rosenberg, miembra del grupo activista Oakland Privacy y Directora Ejecutiva de Media Alliance.

«El Concejo de la Ciudad ve que es esencial tener un debate informado y público tan pronto como sea posible, acerca de las decisiones relacionadas con el financiamiento, adquisición y despliegue de equipo militar y de vigilancia por parte de la policía local», dice al inicio el proyecto de ley modelo CCOPS+M de ACLU, que también cubre el uso de equipo militar.

En una entrada en su blog nacional acerca de CCOPS, el ACLU escribió que «el principal objetivo de este esfuerzo es pasar leyes CCOPS que aseguren a las residentes, a través de sus concejos locales, el poder de decidir si y cómo son usadas las tecnologías de vigilancia, a través de un proceso que maximice la influencia del público sobre estas decisiones». Continua, «Pasar leyes CCOPS empoderaría a los concejos de las ciudades a decir "no" a la vigilancia secreta, compartiendo acuerdos entre federales y la policía local».

Cerca del momento de las revelaciones de Snowden en 2013, la Ciudad de Oakland, Calif., había estado trabajando hacia la construcción de un proyecto de vigilancia masiva.
Hubiera incluido una red de más de setecientas cámaras, software de reconocimiento facial, lectores de placas de carros, y un sensor que identifica el sonido de disparos.
Un grupo de ciudadanas preocupadas se movilizó rápidamente en contra de este esfuerzo y limitó con éxito la adopción de estas tecnologías en Oakland.
Este concejo de ciudad eliminó los elementos particularmente preocupantes del proyecto, como software de reconocimiento facial y retención de datos, y creó un comité para ser el punto de referencia de ciudadanas para supervisar el resto de las partes del proyecto.

El comité ciudadano se convirtió en una comisión privada que aconseja al concejo de la ciudad, y años más tarde lideró la ordenanza que mandaba transparencia e involucramiento público en el uso de equipo de vigilancia por la policía.
Además de este nuevo proceso de aprobación, la ordenanza también incluyó protecciones para informantes.

«Las protecciones para informantes declaran de forma afirmativa que son prohibidas las acciones vengativas, incluyendo acciones personales, en contra de empleadas o aplicantes por llevar a cabo las provisiones de la ordenanza o quejarse acerca del fallo para hacerlo. Brindan medidas cautelares específicas y, cuando son justificados, daños financieros», dijo Rosenberg.

Brian Hofer, miembro de la Comisión de Privacidad de Oakland, ve la ordenanza de privacidad de Oakland como un modelo que cualquier ciudad podría replicar y modificar para que cada comunidad pueda determinar qué niveles de intrusión gubernamental están dispuestas a permitir.
«La belleza de esta ordenanza es que cualquier ciudad puede usar el mismo marco de trabajo, y llegar a conclusiones diferentes para aumentar transparencia y, ojalá, prohibir la mala conducta», dijo.

El 13 de marzo de 2018, el Concejo de la Ciudad de Berkeley aprobó de forma unánime su propia ordenanza de privacidad, poniendo un fuerte precedente para las ciudades que la rodean.
La Ciudad de Davis, Calif., le siguió el 20 de marzo.
Otras jurisdicciones en el Área de la Bahía de San Francisco - incluyendo el Condado de Alameda y Bay Area Rapid Transit - están considerando adoptar similares ordenanzas basadas en el proyecto de ley modelo CCOPS.
Más allá de California, Nashville, Tenn., y Somerville, Mass., han adoptado ordenanzas como la de Oakland, y esfuerzos similares están en camino en Cambridge, Mass., y St. Louise, Mo.

Hasta ahora, las medidas de supervisión proactiva de la tecnología han sido realizadas en su mayoría a nivel local.
Pero en California, está en camino un proyecto de ley que promulgaría una reforma en la tecnología de vigilancia a nivel del estado: El proyecto de ley del Senado 1186, introducido por el Senador de California Jerry Hill.

SB 1186 pretende asegurar que miembras de la comunidad tengan un lugar en la mesa del proceso de toma de decisiones acerca del uso policial de tecnología de vigilancia.
Requiere un debate público y votación por liderezas locales electas previo a la adquisición de nuevas tecnologías de vigilancia por parte de la policía.
Matt Cagle, abogado de tecnología y libertades civiles en el ACLU de California del Norte, dijo que «SB 1186 asegura que las preguntas correctas sobre tecnología de vigilancia sean respondidas desde el inicio y que se hagan decisiones inteligentes para mantener a las comunidades seguras».

Cagle dijo que legislación estatal como SB 1186 «refuerza y construye sobre el progreso que ciudades como Oakland y Davis han hecho».

SB 1186 requeriría que cada agencia «envíe una propuesta de Política de Uso de Vigilancia a su cuerpo de gobierno en una audiencia agendada de forma regular, abierta al público, para el uso de cada tipo de tecnología de vigilancia e información recolectada, como se especifica», y «prohíbe a las agencias policiales vender, compartir o transferir información obtenida por la tecnología de vigilancia, excepto si se trata de otra agencia policial como se permite por la ley y los términos de la Política de Uso de Vigilancia». SB 1186 también permite que las personas demanden a las agencias que violan tales políticas, y que recobren los gastos de abogadas.

Una legislación similar - El proyecto de ley 21 del Senado - murió la temporada pasada luego de pasar por el senado.
Pero Cagle piensa que este movimiento está ganando impulso, y confía que esta vez el proyecto de ley llegará al escritorio de la gobernadora.
Notó que SB 1186 es apoyado por una «amplia coalición estatal de derechos civiles que incluye organizaciones que luchan por la justicia racial y derechos de las inmigrantes».

«Sería genial ver también en Washington algo como el proyecto de ley 21 del Senado», dijo Narayan, enfatizando que la legislación tiene un rol en promover la justicia y la opinión pública en la adopción tecnológica a niveles locales, estatales y federales.

«Debería haber un proceso local para que las comunidades articulen preocupaciones que les son cercanas», me dijo Narayan.
«Pero hay cosas que pueden ser reguladas por el gobierno federal, como la privacidad de banda ancha.
Idealmente, veremos leyes que interactúan entre ellas».

Las jurisdicciones como la del Condado de Santa Clara y Seattle intentan promulgar ordenanzas que sean suficientemente flexibles y amplias para cubrir una gran diversidad de equipo de vigilancia, desde simuladores de sitios celulares hasta programas de reconocimiento facial, y la naturaleza de la tecnología que evoluciona rápidamente.
En otras jurisdicciones, las comunidades están luchando por legislación más específica que responda a las tecnologías específicas que están siendo instaladas.

En Northampton, Mass., el departamento de policía propuso aumentar drásticamente el número de cámaras de vigilancia en el centro de la ciudad.
Preocupadas por esta expansión en el poder de la policía, activistas de la privacidad escribieron el borrador de una ordenanza que establecía restricciones en el uso de cámaras en áreas públicas.
El jefe de la policía había admitido que el departamento entregaría las imágenes capturadas por sus cámaras a cualquier agencia policial que las pidiera, incluyendo a ICE.
«Esto socavaría la designación de ciudad santuario que hemos adoptado al permitir que ICE use las imágenes luego de solicitarlas», dijo William Newman, un abogado del ACLU de Massachusetts.

Newman notó que la amenaza de la expansión de cámaras avivó entre el público la conciencia de los peligros de la vigilancia.
«Este debate ha abierto el potencial de que Northampton toque el tema más amplio de tecnología de vigilancia», dijo.

Luego de que el concejo de la ciudad pasara una ordenanza para limitar el uso de cámaras propiedad de la ciudad en el distrito de negocios de Northampton, la alcalde lo vetó.
En una fuerte defensa de la privacidad de las residentes de Northampton, el concejo de la ciudad anuló el veto de la alcalde y votó por sostener la ordenanza en enero.

Crockford está de acuerdo con que las personas se preocupan más acerca de su derecho a la privacidad que antes, y cita las revelaciones de Edward Snowden de 2013 como una razón significativa para que la gente esté consciente de la vigilancia gubernamental.
«La privacidad es un valor negativo - muchas personas sólo reconocen su valor cuando se la quitan, como una necesidad oculta».

Y conforme la lucha por la privacidad gana impulso, también lo gana el movimiento por las ordenanzas de privacidad.

En contraste con las fuertes políticas que ordenan el involucramiento de la comunidad y la rendición de cuentas de la policía, el status quo actual pone en el público la responsabilidad de luchar contra las agencias gubernamentales por el acceso a la información de solicitudes de registros públicos y de informantes.

McMillen dice que sin políticas de uso y comunicación entre la ciudad y sus residentes, la comunidad no puede hacer que el gobierno rinda cuentas de su uso de drones u otro tipo de tecnología de vigilancia.
«Es intimidante, en especial para miembras de la comunidad que tienen miedo de hablar por miedo a represalias policiales».

La ordenanza de Santa Clara que requiere transparencia en las tecnologías de vigilancia ha estado en funcionamiento desde 2016.
Desde entonces, algunas propuestas de equipamiento han sido adoptadas, otras rechazadas, y algunas modificadas luego de retroalimentación y criticismo.
El Departamento de la Alguacil del Condado de Santa Clara envió una política de uso y declaración del impacto de la vigilancia como parte de su propuesta para BWCs en oficiales.
Luego de que grupos de libertades civiles se opusieran inicialmente al amplio lenguaje redactado, el Departamento actualizó el texto para solucionar algunas de sus preocupaciones.
Finalmente, la propuesta fue aprobada.

Pero luego de realizar un debate público sobre una propuesta diferente, como requiere la ordenanza, la junta de supervisoras del Condado de Santa Clara puso una moratoria al uso de reconocimiento facial con las BWCs de las oficiales de policía.

«[La ordenanza] ha forzado a que la gente responda preguntas como, ¿cuándo y cómo usaremos esto?
Si adquirimos datos, ¿quién los mantendrá y quién tendrá acceso?
Estas preguntas no estaban siendo hechas, menos respondidas, antes de que fuera requisito», dijo Simitian, el supervisor del Condado de Santa Clara.

Las ordenanzas de privacidad no necesariamente detienen a los departamentos policiales cuando tratan de obtener o abusar de la tecnología de vigilancia.
Pero la legislación es una táctica o metodología posible para hacer retroceder el monitoreo masivo del estado, y presionar por mayor transparencia.
«La solución es cambiar la forma en la que definimos la seguridad pública, y una cantidad de conceptos asociados como la naturaleza de la vigilancia, la definición de terrorismo, y la suposición de que recolectar la máxima cantidad tecnológicamente posible de datos sobre las personas, sin importar si hubo una mala conducta, es incuestionablemente bueno», dijo Rosenberg.

Si una ordenanza de vigilancia como las de Santa Clara o Seattle hubiera estado implementada en Boston, el BPD hubiera tenido que ganar la aprobación de un cuerpo electoral antes de adquirir los drones, y con más razón antes de usarlos para sobrevolar en Jamaica Plain.
La comunidad hubiera tenido la oportunidad de luchar contra su uso antes de que fueran obtenidos, y hubieran habido medidas implementadas para que la policía rindiera cuentas en caso de que las herramientas fueran mal usadas.

«Nunca ha faltado el uso de tecnología y vigilancia por parte de la policia estatal para controlar a la población. Nunca un partido en el gobierno ha realizado vigilancia masiva sin que termine en grandes daños», me dijo Hofer.
«El uso sin restricciones siempre lleva a cosas malas, y ningún caso rompe esto - no hay un caso de pruebas amistoso para la vigilancia masiva. Siempre lleva a abusos humanitarios y de libertades civiles».
