# Todo pasa tanto *por Sara Wachter-Boettcher*

(ensayo)

> ¿Qué pasa cuando la industria sobre la que ha estado advirtiendo por años
> finalmente le pone en la mira?

«Google en Boston está interesada en recibirla para un evento», me escribió
mi publicista en un correo electrónico un día de setiembre pasado. Sin pago -
ni siquiera gastos de viajes vendrían de Philly. Pero querían grabar mi
presentación para su serie Charlas en Google, dijeron, y comprarían algunos
libros para repartir.

Normalmente declinaría este tipo de cosas: ¿una gigante de tecnología que está
luchando por convertirse en la primera compañía valorada en un trillón de
dólares quiere mi tiempo y experiencia de gratis? *jajaja, nop*, le escribiría
a una amiga, tirando el emoji de los ojos en blanco para hacerle énfasis. Pero
el libro *Técnicamente Incorrecto*, mi primer título popular y del que estaba
profundamente nerviosa, se iba a lanzar ese mes. Y dado este tema - una
mirada a cómo la cultura tóxica dentro de la industria de tecnología lleva a
productos con sesgo, exclusión y todo tipo de comportamientos no éticos
incorporados - pareció algo que no debería dejar pasar. «Hablé sobre esto en
Google» le agrega una capa de credibilidad, y el vídeo algo de visibilidad.

Entonces aquí estoy, caminando por Cambridge en una mañana ventosa de octubre,
con el control remoto para la presentación en mi mano, carcomiendo ansiedad en
mi estómago. Sólo han pasado dos meses desde que la prensa recogió el memo
de Google - notorio discurso del ex ingeniero James Damore argumentando que
las mujeres simplemente son menos capaces biológicamente para programar que
los hombres, y demandando que Google acabe sus esfuerzos de diversidad y deje
de entrenar a su personal en habilidades como la empatía. Asumo que me
invitaron como un tipo de plan de relaciones públicas de recuperación por
crisis. Pero también se que Damore tiene suficientes fans dentro de Google, y
no estoy segura del tipo de recepción que recibiré en el lugar una vez que
llegue.

Serpentear por la cafetería - perdón, *café del campus* - antes de mi charla
no ayuda. Alrededor de mi hay grupos de chicos en camisas polo y caquis.
Grupos de chicos en camisetas y capuchas. Grupos de chicos en camisas de
franela y lentes Warby Parker. No puedo decir que estoy sorprendida,
exactamente - en la oficina de Boston son mayoritariamente ingenieros y
la fuerza de trabajo de Google, aún después de pregonar por años sus
esfuerzos de diversidad en contratación, es 80% masculina. Pero estoy
nerviosa, hiper consciente de la distancia entre ellos y yo.

Me deslizo en un asiento de una mesa, con un tazón de kale orgánico y pollo
subsidiado por Google en la mano, y pienso acerca de la gente que me rodea.
¿Se dan cuenta que se mueven en manadas? ¿Habrán notado alguna vez la gran
cantidad de hombres que les rodean, día tras día - y, correspondientemente
la poca cantidad de mujeres? ¿Alguno de ellos aparecerá en mi charla?

De todos modos entro al escenario, y la charla va bien: hablo acerca de
programas de reconocimiento de fotos que no pueden reconocer gente negra,
notificaciones de aplicaciones que omiten a gente «queer» y una industria
tecnológica tan empeñada en «deleitar» que no se ha molestado en preguntar si
lo que está haciendo es ético. Las asistentes hacen preguntas inteligentes. Un
grupo se queda al final para charlar. Me devuelvo a la ciudad aliviada.

Un mes después, Google me hace saber que el vídeo ya está en línea. Hago clic
en el enlace de YouTube.

> Este es exactamente el tipo de mujer histérica, mojigata, que volvió ley la
> Prohibición.

> Esta mujer está mentalmente enferma... Necesita atención médica, no un
> micrófono en Google.

> Esa «dama» no necesita vestir esos pantalones. Es ofensiva a mi vista.

Cierro la pestaña, suficientemente inteligente para saber que no hay nada que
ganar leyendo a extraños debatir si soy voluptuosa o sólo gorda. Y luego lloro
de todos modos.

No es la hostilidad la que me rompe. Yo sabía que eso pasaría cuando empecé a
escribir mi libro. De hecho, pensé que podría ser peor - había preparado mis
cuentas, advertí a mi esposo, le pregunté a mi editora cómo me ayudarían si
fuésemos el blanco de los ataques (no pareció que entendieran la pregunta). Es
la velocidad: antes de que pudiera incluso compartir el enlace con mis
amistades ya habían docenas de comentarios como este. Fue publicado en algún
foro u otro, y un pequeño ejército en el campo de guerra contra las mujeres
ya había sido enviado a ponerme en mi lugar. Practica estándar, para ser
honesta.

Debería saberlo. Escribí miles de palabras subrayando las formas en las que
los productos tecnológicos están diseñados para permitir, o algunas veces
incluso promover, el abuso. Podemos verlo en Twitter, donde - más de media
década después de que las mujeres empezaron a reportar el acoso sistemático en
la plataforma, cuatro años después de que Gamergate hizo la vida de Zoe Quinn
un infierno, dos años desde que Milo Yiannopoulos envió una legión de troles a
atacar a Leslie Jones por atreverse a ser una mujer negra en una película de
*Cazafantasmas* - los ejecutivos aún no tienen idea de cómo frenar el abuso en
su plataforma, un lugar en el que los avatares de Pepe la rana y apologistas
Nazis se vuelven locos. Podemos verlo en Reddit, en donde los subreddits
repletos de racismo virulento se supone que sean manejados por moderadores sin
pago - como lo puso el anterior gerente general Erik Martin, «Haga que las
personas usuarias hagan la parte difícil». Y lo podemos ver, como lo hice yo,
en YouTube, donde misóginos (y los bots trabajando para ellos) acuden en masa
al contenido sobre problemas de justicia social, intentando bajarle votos a
los vídeos ellos mismos mientras suben votos a los comentarios más viles en
esos vídeos. El juego es simple: si usted puede rápidamente hacer que el
contenido parezca impopular, YouTube se lo mostrará a menos personas. Y
aquellas a las que se les muestra el vídeo no sólo verán la charla; también
verán los comentarios más votados.

No sólo estoy consciente de este sistema. Lo he mapeado laboriosamente, viendo
la forma en la que a los chicos blancos financieramente confortables se les
ocurren ideas que funcionan genial para ellos - sin notar nunca lo mal que
fallarán para personas marginalizadas o vulnerables al abuso. Y luego, una vez
que *sí* lo notan, lo hablan por meses o incluso años sin realmente hacer
nada.

Los insultos mismos ni siquiera son sorprendentes. Todos se leen como una
página de un libro básico de sexismo: Soy fea, soy irracional, soy una
regañona sin humor. Todo es tan obvio. Después de todo, he tenido treinta y
cinco años para aprender las reglas. Yo se que para ser tomada en serio
debería borrar mis sentimientos, endurecer mi piel, evitar ser *dramática* -
o esperar que mi inteligencia y salud mental sean cuestionadas. Yo se que si
quiero evitar el ridículo también debería odiar mi cuerpo - debería cubrir
mis brazos, amarrar mis muslos, vestir más de negro, ser más invisible. Yo he
traicionado todas esas enseñanzas. He sido segura. He hablado sobre esos
sentimientos. Me he subido al escenario en *pantalones morados*, por dios. Y
los troles sabían exactamente como castigarme por mis transgresiones. Ellos
habían aprendido las mismas reglas que yo.

Nada de eso se debería sentir personal, pero por supuesto que lo hace. Siempre
lo hace. Pero también, de alguna forma, es validador. Como levantarse con un
furioso resfrío después de tres días de preguntarse si esa picazón en la
garganta era real o no. Yo no estaba imaginando cosas. La enfermedad ha
estado supurando todo este tiempo.

Yo no confiaba mucho en las compañías de tecnología antes de escribir este
libro, y ahora confío incluso menos. Las grandes se preocupan por satisfacer
a las accionistas. Las pequeñas se preocupan por satisfacer a las inversoras
de capital de riesgo. Ninguna de estas se preocupa por usted.

Pero lo que no había anticipado es cómo la tecnología también erosionaría la
confianza en mi misma.

Hace siglos, en esos dulces años antes de las noticias falsas y el jaqueo
ruso en las elecciones, antes de Gamergate, antes de que hombres cualquiera
aparecieran en mis menciones cada día explicándome mi propio trabajo, Twitter
era mi salvavidas. Era una forma de encontrar y conectar con pares en una
industria - estrategia de contenido y diseño de experiencia de usuaria - que
apenas estaba emergiendo, y que yo apenas estaba empezando a reclamar como
mía.

También era una forma de juntar mis lados personal y profesional, jugando con
un espacio propio que se sintiera inteligente y auténtico. Podía ser graciosa.
Podía ser seria. Podía compartir un artículo que había escrito sobre metadatos
el martes en la mañana, y luego enviar una serie de «tweets» medio borracha
sobre un programa de TV esa noche. Me sentía vista. Me sentía comprendida.

Diez años después apenas reconozco a esa persona. Un día escribo borradores y
los elimino, viendo como el flujo sigue sin mi. El siguiente, comparto elogios
sobre mi libro, o enlazo al nuevo episodio de mi podcast, o reenvío el último
artículo que escribí - y me siento avergonzada de la auto promoción. Vacilo
entre la necesidad de compartir mi voz - de usar *mi plataforma*, como ellos
dicen - y un deseo creciente de esconderme.

No sólo ya no me siento vista. Me siento vigilada. Juzgada. Ansiosa acerca de
lo que todo esto significa. Calculo: ¿me estoy convirtiendo a mi misma en el
blanco? ¿Será este el «tweet» o será esta la opinión que finalmente traerá
una ola tan grande de troles de pastilla roja y supremacistas blancos enojados
que me tumbe para siempre? ¿El sentirme vigilada es el precio que tengo que
pagar por ser ambiciosa, por querer crear y criticar y participar en el mundo?
¿Qué dice sobre mi que estoy dispuesta a pagarlo?

Y luego, si estoy siendo honesta aquí, también pienso en algo mucho más
oscuro: ¿por qué no estoy recibiendo más abuso? ¿Es mi trabajo demasiado
ignorable? ¿Son mis opiniones demasiado seguras? ¿No debería ser más
controversial ya?

Me reprendo a mi misma. ¿Qué tipo de monstruo se siente *celosa* de la gente
que está siento acosada?

Resulta que esta.

Allá en 2011 me obsesioné con @horse_ebooks. La cuenta, supuestamente un bot
de un «spammer» ruso, regularmente enviaba «tweets» con fragmentos de
texto absurdos: «Desafortunadamente, como probablemente ya saben, la gente».
«Alístense para volar helicópteros». Y mi favorito personal: «Todo pasa
tanto». Los «tweets» eran fascinantes, inexplicablemente hilarantes, y
salvajemente populares. Eran el Twitter Raro en todo su esplendor.

También no eran generados por un bot - o por lo menos no por mucho tiempo.
Aparentemente el «spammer» ruso vendió la cuenta a un empleado de Buzzfeed
en algún momento el mismo año que la descubrí, y él había sido el que escribía
los «tweets» desde eso. Aun así, años después, esa línea sigue haciendo ruido
en mi cerebro. Todo pasa tanto. Me he encontrado susurrándolo de forma
inconsciente mientras recorro mis canales en la web, abrumada por las últimas
noticias y conversaciones y bromas y troles y gatos y todo lo demás.

*Todo pasa tanto*. Esa es la belleza, pero también el problema. No es que la
tecnología rompió mi confianza - por lo menos no al inicio. Pero rompió mi
contexto: no se dónde estoy. No se si estoy en el trabajo o jugando, si estoy
viendo noticias o charlando con una amiga. Esto solía sentirse liberador: no
tenía que escoger. Podía simplemente *existir*, flotando en una amalgama del
universo de mi propio diseño. Pero si se deja sin revisar por mucho tiempo -
por compañías de tecnología miopes, y por mis propios deseos mezquinos - la
falta de contexto cría algo siniestro: un lugar en el que los motivos de todas
son sospechosos. No se quién me está viendo, o cuándo vienen por mi. Pero se
que están ahí: los fans de James Damore, los ejércitos de troles de YouTube,
los Nazis de Twitter, los misóginos casuales deseando jugar al abogado del
diablo.

Por ahora, por lo menos, también estoy yo. Sólo que no estoy del todo segura
de por qué.
