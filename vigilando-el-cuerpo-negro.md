# Vigilando el cuerpo Negro, *por Malkia Cyril*

(ensayo)

> Un resurgimiento de las estrategias de vigilancia de la era de los derechos civiles está poniendo en peligro a activistas Negras mientras las compañías de tecnología obtienen ganancias.

En Diciembre de 2017, agentes del FBI forzaron a Rakem Balogun y a su hijo de quince años a que salieran de su casa en Dalas.
Arrestaron a Balogun bajo la acusación de posesión ilegal de armas de fuego y le incautaron un libro llamado *Negros con pistolas*.
Después de que le negaran la fianza y de pasar cinco meses en prisión, Balogun fue liberado y se le retiraron todos los cargos.

Para su consternación, Balogun luego descubrió que el FBI le había estado monitoreando por años.
También descubrió que había sido arrestado ese día por una razón en específico: había publicado en Facebook una actualización criticando a la policía.

Algunas personas consideran a Balogun como el primer individuo perseguido bajo un programa secreto del gobierno que rastrea a las llamadas «Identidades Extremistas Negras» (INEs).

Una Extremista Negra es como el FBI llamó a mi madre, hace cincuenta años.

## La historia se repite a sí misma

En definitiva hubieron cosas extremas acerca de mi madre.
El dolor de vivir con anemia falciforme era extremo.
La cantidad de libros que pensaba que podían caber en un cuarto, apilándolos hasta lo alto en una sala de nuestro hogar: eso era extremo.

Recuerdo estar sentado en sus hombros durante mi primer protesta, a principios de 1980, contra la deportación de personas haitianas que llegaban en bote.
Sentado ahí, en la cima de mi muy pequeño mundo, escuchando historias, una tras otra, de cuerpos Negros arrastrados por el mar por sólo tratar de buscar una vida mejor, empecé a entender claramente: ser Negra en América, y en cualquier parte del mundo, era una experiencia extrema.

¿Pero era extremo querer seguridad, libertad y justicia para ella, su familia y su gente?

A pesar del dolor y las complicaciones de la anemia falciforme, y hasta que la enfermedad tomó su vida en 2005, mi madre trabajó cada día como educadora, haciendo su parte para lograr derechos humanos para todas las personas oprimidas y específicamente para las personas Negras en los Estados Unidos.
Ya fuese en una protesta, en el piso de la Librería Liberation en Harlem, en la casa oscurecida de la activista japonesa Yuri Kochiyama, o en un centro de votación en el día de las elecciones, mi madre siempre tomó el tiempo para contarnos historias a mi y a mi hermana sobre sus días llenando sobres para el Comité Estudiantil de Coordinación No Violenta, luego como coordinadora para toda la ciudad del Programa de Desayuno Gratuito para Niñas, que en ese momento operaba en iglesias a lo largo de la Ciudad de Nueva York.
De acuerdo con mi mamá, encontrar su voz en el movimiento de liberación Negra fue poderoso.

Sin embargo, por su voz, hasta el momento de su muerte, el cuerpo Negro de mi madre estuvo bajo constante vigilancia por el FBI y otras agencias gubernamentales.

Nosotras sentimos directamente la vigilancia de mi madre por parte del FBI a finales de los años 70.
Para hostigarla, el FBI envió el expediente de mi madre a los Servicios de Salud, donde ella trabajaba como asistente de dirección para programas de salud mental en las cárceles de Nueva York, y a los oficiales penitenciarios de las cárceles en las que trabajaba.
En su favor, los Servicios de Salud rechazaron la intervención del FBI.
Pero la Oficina de Prisiones le prohibió a mi madre entrar a las cárceles, obligándola a supervisar sus programas de forma remota.
Recuerdo cuando, años después, mi madre obtuvo acceso a su expediente del FBI a través de una solicitud del Freedom of Information Act.
Era grueso, con montones de páginas editadas que hablaban de la policía y el FBI persiguiéndola desde mediados de los años 60.

Dos semanas antes de que mi madre murió, agentes del FBI visitaron nuestro hogar, demandando que ella viniera para interrogarla acerca de un caso de los años 70.
Mi madre apenas podía caminar, estaba sufriendo de demencia, y estaba muriendo.
Ella se negó.

Mi madre era el blanco de la vigilancia por su compromiso con la justicia social y los derechos humanos.
Por esto, yo crecí con vigilancia gubernamental como el agua en la que nadaba, el aire que respiraba.

Aprendí que el FBI tenía una larga historia de monitorear líderes de derechos civiles y liberación Negra como Ella Baker, Fannie Lou Hamer y Martin Luther King, Jr.
Monitoreaban líderes Musulmanes Negros como Malcolm X.
Monitoreaban líderes inmigrantes Negros como Marcus Garvey.

Llegué a aprender acerca del Programa de Contrainteligencia del FBI, o COINTELPRO, el programa gubernamental secreto iniciado en 1956 para monitorear e interrumpir las actividades del Partido Comunista de los Estados Unidos.
Sus actividades con frecuencia eran ilegales, y se extendieron en los años 60 para fijar como objetivo a las activistas Negras de movimientos de derechos civiles y Poder Negro, llamando a estas activistas - lo adivinaron - Extremistas Negras.

En 1975, un Comité del Senado, popularmente llamado el Comité de Church, se formó para investigar los programas de inteligencia del FBI, una respuesta a la presión de un grupo que publicó documentos exponiendo la existencia de COINTELPRO.
En un artículo de 2014 para *Nation*, Betty Medsger subraya la conclusión del Comité no sólo de que las Afro Americanas estaban siendo observadas por el gobierno más que cualquier otro grupo, sino también de que el FBI no requería evidencia de «extremismo» para justificar la vigilancia.
Para nuestras comunidades, no importaba si una nunca había pronunciado una palabra subversiva, ni mucho menos tomado parte de cualquier violencia.
Como escribe Medsger, «ser Negra era suficiente».
Este espionaje de activistas Negras sin orden judicial resultó en docenas de muertes Negras por disparos policiales, y otras vidas Negras tragadas completamente por décadas de encarcelación ilícita de prisioneras políticas.
Hombres como Kamau Sadiki y mujeres como Safiya Bukhari, a quienes crecí llamando tío y tía, estaban entre estas.

En última instancia, el reporte final del Comité de Church concluyó que COINTELPRO fue un programa peligroso.
Como Robyn C. Spencer explica en *Black Perspectives*, el reporte declara que el FBI usó tácticas que aumentaron el «riesgo de muerte», con frecuencia haciendo caso omiso «de los derechos personales y la dignidad de sus víctimas».
El Comité determinó que el FBI usó «justificaciones vagamente definidas de "inteligencia pura" e "inteligencia preventiva"» para su vigilancia de ciudadanas que no habían cometido ningún crimen - por razones que tenían poco o nada que ver con la aplicación de la ley.

Dada esta historia, la historia de mi madre, mi historia, yo no me sorprendí cuando *Foreign Policy* publicó la historia de que una evaluación de inteligencia del FBI de agosto de 2017 había identificado una nueva designación: la «Identidad Negra Extremista».

No estaba sorprendida, pero estaba asustada.

## Entonces, ¿qué es una «Identidad Negra Extremista»?

«Identidad Negra Extremista» es una categoría construida, inventada por el FBI y documentada en una evaluación de agosto de 2017 titulada «Identidades Negras Extremistas motivadas para poner en la mira a oficiales de policía».

El FBI fabricó la designación INE para crear sospecha en personas Negras que respondieron públicamente a los asesinatos policiales extrajudiciales, pero no para ahí.
El documento también se enfoca fuertemente en la convergencia de lo que llama «ideología de ciudadanas soberanas árabes [musulmanas]» y radicalización Negra como razones para el elevado enfoque de aplicación de la ley.
Como soporte, la evaluación cita específicamente los casos completamente no relacionados de Micah Johnson, un hombre que disparó y mató a múltiples oficiales de policía en Dalas durante una protesta de 2016; Zale H. Thompson, quien atacó a la policía en Queens, N.Y., con una hacha en 2014; Gavin Eugene Long, quien asesinó múltiples oficiales de policía en Baton Rouge, La.; y algunos otros sujetos sin nombrar.
En cada uno de los incidentes citados los perpetradores actuaron solos y sin ninguna conexión entre ellos más allá del hecho de que todos eran hombres Negros.
No solo los casos no están relacionados, sino que ninguno está relacionado con el movimiento mayor organizado para las vidas Negras en general y la Black Lives Matter Global Network en particular.
El objetivo del FBI es claro: enlazar ficticiamente actividades protegidas democráticamente dirigidas a terminar la violencia y conducta indebida policial con lo que llama «violencia premeditada, vengativa y letal» contra oficiales de policía.
Esto no sólo es poco ético e inexplicable; pone vidas Negras en peligro real.

Incluso la propia definición del FBI en la evaluación es vaga y probablemente inconstitucional: «El FBI define identidades negras extremistas como individuos que buscan, totalmente o en parte, a través de actos ilegales de fuerza o violencia, en respuesta al racismo y la injusticia percibidas en la sociedad americana, [establecer] un territorio negro separado o instituciones sociales, comunidades u organizaciones de gobierno negras autónomas, dentro de los Estados Unidos».
Esta definición - incluyendo cualquier acto de fuerza conducido, incluso de forma parcial, en respuesta a injusticia en la sociedad - no tiene límite.
Le da al FBI y a abogadas acusadoras una amplia discreción para tratar cualquier violencia por personas que resultan ser Negras como parte de una conspiración terrorista.
También es absolutamente sin fundamento.

El hecho es que, como el *Washington Post* reportó en 2015, no es más probable que oficiales de policía sean matados por delincuentes Negras que por delincuentes blancas.
Más de la mitad de todas las oficiales matadas en la línea del deber mueren como resultado de accidentes en la comisión de su trabajo, en lugar de por ataques de cualquier tipo.
El número total de oficiales matadas en los ataques de estilo de emboscada que son centrales para la narrativa INE se mantiene bastante bajo, y en general las muertes recientes de oficiales se mantienen por debajo del promedio nacional comparado con la última década.

La conclusión es: las «Identidades Negras Extremistas» no existen. La evaluación del FBI está enraizada en una historia de racismo anti Negro dentro y más allá del FBI, con la fea adición de la islamofobia.
Lo que es peor es que la designación, al enlazar protestas políticas protegidas por la constitución con la violencia de unas pocas personas que resultan ser Negras, sirve para desalentar la disidencia vital.
Dada la sórdida historia del FBI, esta evaluación también podrían ser usada para racionalizar el acoso a las protestantes Negras e incluso una respuesta policial aún más militante en su contra.

A pesar de las graves preocupaciones de las defensoras, la evaluación y designación del FBI ya están siendo usadas para justificar tanto la erosión de los decretos de consentimiento basados en la justicia racial y la introducción de más de treinta y dos leyes de «Blue Lives Matter» a lo largo de catorce estados en 2017.
La evaluación del FBI también alimenta esta narrativa infundada en el entrenamiento de la policía local. Un curso de 2018 ofrecido por el Departamento de Servicios de Justicia Criminal de Virginia, por ejemplo, incluye «Identidades Negras Extremistas» en su visión general de «grupos domésticos de terror y subculturas criminalmente subversivas que son encontradas diariamente por profesionales de la aplicación de la ley».

## La acción policial de alta tecnología de disidencia Negra

El programa INE no sólo me recuerda COINTELPRO; representa su resurgimiento, esta vez a plena vista.
Hoy, sin embargo, ayudado por la industria tecnológica, este COINTELPRO moderno ha sido digitalizado y actualizado para el siglo veintiuno.

Justo cuando «Black Lives Matter» y el movimiento más amplio para vidas Negras se organizan para confrontar la acción policial persistentemente brutal y sin explicación, programas como INE están legalizando la vigilancia extrajudicial de comunidades Negras.
El acceso policial a datos de redes sociales está ayudando a impulsar esta vigilancia.
Las grandes compañías tecnológicas y de datos no sólo están esperando y observando el espectáculo; están vendiendo tiquetes.
Y a través de la publicidad y la venta directa de tecnologías de vigilancia, están haciendo una matanza.

Demasiadas personas aún creen que violaciones de derechos civiles y humanos de estas proporciones no pueden ocurrir en América.
O no saben que han estado ocurriendo por siglos, o equivocadamente creen que esos días terminaron hace mucho tiempo.
Pero ahora, ciudades americanas con grandes poblaciones Negras, como Baltimore, están convirtiéndose en laboratorios de tecnologías policiales tales como drones, simuladores de teléfonos celulares y lectores de placas de autos.
Estas herramientas, con frecuencia adquiridas a través de programas subvencionados por el FBI, están siendo usadas para perseguir a activistas Negras.

Esto no es nuevo.
Compañías de tecnología y plataformas digitales históricamente han jugado un rol único en socavar los derechos democráticos de las comunidades Negras.
En el siglo veinte el FBI conspiró con Ma Bell para apropiarse de líneas telefónicas y escuchar las conversaciones de líderes civiles, entre otras.

Dada esta historia, la vigilancia de alta tecnología de cuerpos Negros no se siente nueva o distópica para mi.
Todo lo contrario.
Como la autora Simone Browne articula bellamente en su libro *Dark Matters*, las agencias construidas para monitorear comunidades Negras y refugiar a nacionalistas blancas usarán cualquier tecnología disponible para llevar a cabo el mandato de la supremacía blanca.
Estas prácticas del siglo veintiuno son simplemente una extensión de la historia y una manifestación de las relaciones de poder actuales.
Para los cuerpos Negros en América, la vigilancia persistente y generalizada es simplemente un hecho de la vida diaria.

De hecho, el monitoreo de cuerpos Negros es mucho más viejo que la versión actual de alta tecnología y que el COINTELPRO original.
Browne nota que en Nueva York del siglo dieciocho, «leyes linterna» hicieron necesario que las personas Negras esclavizadas estuvieran iluminadas cuando caminaban de noche sin compañía de una persona blanca.
Estas leyes, junto con un sistema de pases que le permitía a las personas Negras ir y venir, leyes Jim Crow que segregaron cuerpos Negros, y el linchamiento que reprimía la disidencia Negra con fuerza asesina extrajudicial, son todas formas de monitorear que, como Claudia García Rojas observó en una entrevista con Browne para Truthout en 2016, han «hecho posible que personas blancas identifiquen, observen y controlen el cuerpo Negro en el espacio, pero también crear y mantener barreras raciales».
Estas son las condiciones continuas que dieron nacimiento a la designación INE del FBI.

Siempre ha sido peligroso ser Negra en América.
La conformidad de las compañías tecnológicas y - bajo el liderazgo del Fiscal General Jeff Sessions y el Presidente Trump - la designación INE escala ese peligro de forma exponencial.

Por ejemplo, mientras muchas han luchado por mucho tiempo por la diversidad racial en la fuerza de trabajo de Amazon en los Estados Unidos, pocas estaban preparadas para la bomba de que Amazon estaba vendiendo su herramienta de reconocimiento facial, Rekognition, a departamentos de policía locales, habilitándoles para identificar a activistas Negras.
Este problema se hace peor por el hecho de que se ha mostrado que las herramientas de reconocimiento facial discriminan contra caras Negras.

Cuando el Center for Media Justice (CMJ), la organización que yo dirijo, y otras docenas de grupos de derechos civiles demandaron que Amazon parara la venta de tecnología de vigilancia al gobierno, la compañía defendió su práctica diciendo, «Nuestra calidad de vida sería mucho peor hoy si volviéramos ilegal la tecnología nueva porque algunas personas pueden escoger abusarla».
Tales apelaciones asumen una línea base de igualdad en este país que nunca ha existido, ignorando el muy real sesgo anti Negro incrustado en el software de reconocimiento facial.
La respuesta de Amazon también rechaza cualquier responsabilidad por los bien conocidos abusos contra las comunidades Negras.
Pero estos pasan a diario a manos de las mismas fuerzas policiacas que están comprando Rekognition.
Puesto simplemente, lo acepten o no, Jeff Bezos y su compañía están escogiendo las ganancias por encima de las vidas Negras.

La proliferación de tecnologías inefectivas, que no pueden rendir cuentas y son discriminatorias en manos de brutales agencias de aplicación de la ley con un mandato de criminalizar disidentes protegidas por la ley usando la designación INE del FBI no es simplemente peligrosa para las vidas Negras - es mortal.

En 2016, la ACLU de California del Norte, publicó un reporte esbozando cómo Facebook, Instagram y Twitter brindan datos de usuarias a Geofeedia, un producto de vigilancia de redes sociales usado por oficiales gubernamentales, firmas privadas de seguridad, agencias de mercadeo, y sí, por la policía para monitorear las actividades y discusiones de activistas de color.

Estos ejemplos muestran que nuestro entorno digital del siglo veintiuno ofrece a las comunidades Negras un péndulo en constante oscilación entre promesa y peligro.
Por un lado, la tecnología del siglo veintiuno está abriendo rutas para eludir las guardianes tradicionales del poder a través de internet gratuita y abierta - permitiendo a comunidades de color marginalizadas unirse y construir movimientos extendidos para el cambio.
El crecimiento del movimiento de vidas Negras es sólo un ejemplo.
Por el otro lado, el perfilado, la acción policial y el castigo de alta tecnología están haciendo crecer de forma enorme la discriminación racial y poniendo las vidas Negras y la disidencia en un riesgo aún mayor.
Con demasiada frecuencia, la segunda se disfraza de la primera.

## Defender nuestros movimientos demandando la desobediencia de las compañías de tecnología

Una forma de luchar es clara: organizarnos para demandar la desobediencia de las compañías de tecnología hacia la vigilancia policial masiva.
Y - a pesar de la respuesta inicial de Amazon a la crítica de sus tecnologías de reconocimiento facial - la presión pública hacia estas compañías públicas para que paren de alimentar la vigilancia policial ha tenido éxito antes.

Para luchar contra la vigilancia de Geofeedia, CMJ se asoció con la ACLU de California del Norte y Color of Change para presionar a Facebook, Instagram y Twitter para que dejaran de permitir que sus plataformas y datos sean usados para los propósitos de la vigilancia gubernamental.
Tuvimos éxito.
Todas estas tres plataformas de redes sociales han dejado de permitir que Geofeedia mine los datos de usuarias.

Tanto desde adentro - a través de las demandas de su propia fuerza de trabajo - y desde afuera - a través de la presión de sus usuarias, el público y grupos como CMJ y ACLU - podemos crear una elección importante para las compañías públicas como Amazon, Twitter, IBM, Microsoft y Facebook.
Podemos empujarles para que aumenten su rol de empoderar a activistas Negras y detener su participación en la persecución de esas mismas personas.

La ruta hacia adelante no será fácil.
Como fue revelado por el escándalo de Cambridge Analytica, en el que la información de más de ochenta millones de usuarias de Facebook fue vendida a una firma de datos políticos contratada por la campaña de la elección de Donald Trump, las prácticas de altas tecnologías usadas por la policía para perseguir activistas Negras ya están profundamente incrustadas en un ecosistema tecnológico en gran parte blanco y masculino.
No es coincidencia que actoras Rusas también usaron Facebook para influenciar las elecciones americanas de 2016, y lo hicieron usando retórica anti Negra, anti Musulmana y anti inmigrante.
Ellas saben que los prejuicios del público en general son fáciles de inflamar.
Algunas en tecnología seguirán el concurso por un más amplio acceso policial a datos de redes sociales, pero debemos aislarles.

El punto es, demandar la no cooperación de las compañías de tecnología y datos es una herramienta increíblemente poderosa para resistir la creciente infraestructura que amenaza la disidencia Negra.

En una era digital, los datos son poder.
Compañías de datos como Facebook están disfrazadas de redes sociales, pero sus ganancias vienen de los datos que procuran y comparten.
Un programa INE, como la vigilancia de mi madre antes de este, necesita datos para funcionar.
El rol de las compañías de tecnología y datos en este concurso por el poder no podría ser más crítico.

## ¿Vigilancia para quién? El mito de combatir el extremismo violento

En un intento de justificar su vigilancia, el gobierno con frecuencia señala la seguridad nacional.
Pero si el FBI, el Fiscal General Sessions y el Departamento de Justicia en serio se preocuparan por la seguridad de todas las personas en este país, usarían sus sistemas de vigilancia para perseguir a nacionalistas blancos.
Por años, la amenaza creciente de la violencia supremacistas blanca ha sido clara y obvia.
Un Joint Intelligence Bulletin de 2017 advertía que los grupos supremacistas blancos «fueron responsables de 49 homicidios en 26 ataques de 2000 a 2016... más que cualquier otro movimiento extremista doméstico» y que ellos «es probable que continuaran representando una amenaza el siguiente año».
Sin embargo, poco se ha hecho para abordar esta amenaza mayor.

Ya existe una estructura fuertemente dotada de recursos que podría en teoría abordar tal violencia supremacista blanca: Coutering Violent Extremism (CVE).
Estos programas tocan escuelas e instituciones religiosas y civiles, llamando para que líderes comunitarias y religiosas locales trabajen con la policía y otras agencias gubernamentales para identificar y reportar «extremistas radicalizadas» con base en un conjunto de criterios generalizados.
De acuerdo con un reporte de Brennan Center, los criterios incluyen «expresiones de desesperanza, un sentido de ser tratadas injustamente, salud en general y nivel económico».
El reporte apunta a que todas desde oficiales escolares hasta líderes religiosas tienen la tarea de identificar personas con base en estas medidas.

A pesar de ser formulado en términos neutrales, hasta ahora CVE se ha enfocado casi exclusivamente en comunidades Americanas Musulmanas.
Recientemente, la administración de Trump botó todas las pretensiones y propuso cambiar el nombre del programa de Countering Violent Extremism a Countering Islamic Extremism.
Como reportó *Reuters* en febrero de 2017, este programa renombrado «ya no perseguiría grupos tales como supremacistas blancos».

El enfoque desproporcionado en monitorear comunidades Musulmanas a través de CVE también ha ayudado a justificar el enfoque desproporcionado en el llamado extremismo Negro.
Cerca del 32 por ciento de las Musulmanas nacidas en Estados Unidos son Negras, según el Pew Research Center.
De esta forma, la actual represión a la disidencia Negra por parte del FBI está conectada, en parte, a la represión de disidencia islámica.
Como notamos arriba, la designación INE se vincula directamente con el islam.
Y, por supuesto, los programas CVE fueron modelados como COINTELPRO, y la designación INE está modelada en la persecución exitosa de las comunidades musulmanas en violación directa de sus derechos civiles y humanos.
Y la tecnología está aquí, también.
CVE funciona en combinación con el uso reflexivo y discriminatorio de análisis predictivos y monitoreo de GPS dentro de nuestro sistema de justicia criminal.
Sume a esto el crecimiento de la Iniciativa de Examinación Extrema del Departamento de Seguridad Nacional, que usa redes sociales y reconocimiento facial para militarizar la frontera y detener generaciones de inmigrantes de forma ilegal.
Juntos, estos programas crean un ambiente político en le que activistas Negras pueden ser perseguidas, consideradas terroristas domésticas y ser despojadas de sus derechos democráticos básicos.

## Nosotras decimos que las vidas Negras importan

La designación del FBI nunca estuvo basada en una preocupación por la seguridad de oficiales o la seguridad nacional.
No estaba basada en ninguna evidencia de que la «Identidad Negra Extremista» tan siquiera existiera.
Ninguna de esas justificaciones se sostienen.
En su lugar, está basada en un deseo histórico de reprimir la disidencia Negra y prevenir que los movimientos sociales ganen impulso.

Y aún así el movimiento por las vidas Negras, de hecho, ha ganado impulso.

Yo me volví miembro de la Black Lives Matter Global Network después del asesinato brutal de Trayvon Martin y la subsiguiente absolución de su asesino, George Zimmerman.
Fue extraordinario ser testigo de la velocidad y el impacto con las que este movimiento creció en línea y en las calles.
Impulsada por la bravura de las comunidades Negras en Ferguson, Mo., yo estaba orgullosa de ser parte de ese crecimiento: marchando en la calle, confrontando el aparentemente interminable patrón de muertes Negras a manos de policías.
Fue un sentimiento extraordinario unirme a personas Negras a lo largo del país cuando cerrábamos estaciones de policía en protesta por su violencia, deteniendo el tránsito para decir los nombres de las mujeres Negras asesinadas, y por último obligando a las candidatas Democráticas a abordar las preocupaciones de las votantes Negras.

La designación INE del FBI es un flagrante intento de socavar este impulso.
Busca criminalizar y enfriar el disentimiento Negro y prevenir alianzas entre Negras, Musulmanas, inmigrantes y otras comunidades.
Mientras las activistas Negras podrían ser apuntadas por la designación INE, no somos las únicas impactadas por esta aproximación manipuladora.
Las organizadoras de la resistencia trabajando para oponerse a la detención, deportación y separación de familias inmigrantes; aquellas luchando contra el fascismo y la supremacía blanca; las comunidades Musulmanas; y otras están siendo vigiladas y amenazados junto a nosotras.

En 2018, tuvimos una Corte Suprema que ha confirmado una prohibición inconstitucional a las Musulmanas junto a los esfuerzos de la Casa Blanca de negar el debido proceso a las familias indocumentadas; tenemos un Fiscal General y un Departamento de Justicia que respaldan el espionaje de redes sociales y la vigilancia de alta tecnología hacia personas por simplemente decir y asegurar que las vidas Negras importan.
No es coincidencia que conforme las activistas Negras están siendo perseguidas, la Cámara de Representantes ha pasado silenciosamente una ley de «Blue Lives Matter», que pronto se moverá al Senado, protegiendo la policía ya fuertemente defendida.
Esto incluso mientras las víctimas de violencia policial encuentran poca justicia, si acaso, a través de las cortes debido a la maraña de protecciones e inmunidades que ya son disfrutadas por la policía.

El movimiento por las vidas Negras es un movimiento en contra de las fronteras y en favor de la pertenencia.
Demanda que las compañías de alta tecnología dejen la vigilancia de las comunidades Negras, y en su lugar inviertan en nuestras vidas y nuestra sostenibilidad.

Si mi madre estuviera viva, ella me recordaría que un gobierno que ha esclavizado Africanas y vendido a sus niñas, igual de rápido criminalizaría progenitoras inmigrantes y detendría a sus niñas como rehenes, y llamaría terroristas a niñas Musulmanas, Árabes y Sur Asiáticas, para bombardearlas hasta que dejen de existir.

Ella me recordaría que socavar los derechos civiles y humanos de comunidades Negras es el arco extremo de la historia, un arco que aún puede ser doblado en la dirección de la justicia por los mismos cuerpos que ahora están siendo monitoreados.
El único remedio es el nuevo y creciente movimiento que somos nosotras, y demandamos no ser vigiladas sino observadas.
