# El punto más alto de la negación *por Cory Doctorow*

(ensayo)

> Cada día más gente cree que tenemos un problema - ¿podemos conseguirles antes
> de que el nihilismo de la privacidad se establezca?

El 22 de abril de 1990 yo era una de las decenas de miles de personas que
llenaron la plaza Nathan Phillips de Toronto, en frente del ayuntamiento, para
conmemorar el Día de la Tierra, el primero en veinte años. Yo tenía dieciocho
años, y tan lejos como pude recordar, había tenido conciencia de que había una
crisis ambiental a la vista - desde los PCB en el agua hasta el agujero en la
capa de ozono. Pero conforme avanzaba el día, las oradoras tomaban el
escenario y la multitud zumbaba y hervía, me di cuenta de que esto era serio.
No importaba cuán grande y desesperanzadora era la causa; la ecología frágil
del único planeta capaz de sustentar la vida humana en el *universo* estaba
en riesgo de un colapso permanente e irreversible.

Ahí fue cuando me convertí en un pesado del clima. Durante los años siguientes
traté de convencer a la gente a mi alrededor de que nos estábamos arrojando a
toda velocidad hacia una crisis que podría afectarles negativamente a ellas y
a todas las personas que amaban: mares crecientes, vientos asesinos, olas de
enfermedades y crisis de agua y migración amenazaban nuestro futuro, y para
cuando se manifestaran de tal forma que fueran innegables podría ser demasiado
tarde.

Fallé. Todas fallamos. Cuando juntamos suficiente presión política para
provocar un esfuerzo internacional, se desmoronó: Kioto, Copenhague, París.
Todos poco ambiciosos, no vinculantes, insuficientes y, por supuesto,
ignorados por casi todas las firmantes.

Pero pasó una cosa divertida en el camino hacia el apocalipsis climático: el
trabajo cambió. Hace veinte años la mayoría de gente con la que tenía la
charla sobre el clima asumía que estaba exagerando el peligro. Ahora, la
mayoría de personas con las que hablo aceptan el peligro - sólo no creen que
puedan hacer algo acerca de eso.

Eso es porque en algún lugar del camino cruzamos «el punto más alto de la
negación» - el momento en el cual la cantidad de personas que simplemente
piensan que el cambio climático no es un asunto importante está en inevitable
disminución. Alguna combinación de comunicación científica (películas como
*Una verdad incómoda*) y las consecuencias innegables (inundaciones,
huracanes, sequías, tormentas de nieve y otros eventos climáticos extremos)
convencieron a una masa crítica de personas que el problema es urgente.

Las consecuencias sólo se están volviendo más innegables conforme la ciencia
se vuelve más clara, el alfabetismo científico se vuelve más extendido y las
comunicadoras afilan sus explicaciones. Es difícil imaginar cómo la cantidad
de personas que admiten la realidad del cambio climático podría tan siquiera
disminuir. De ahora en adelante, la cantidad de personas cuyas experiencias
directas las vuelven creyentes del cambio climático solo aumentará.

Esto presenta un nuevo reto táctico para activistas. Ya no es tan importante
convencer a las personas de que el cambio climático es real - ahora tenemos
que convencerles que podemos y debemos hacer algo al respecto. En lugar de
discutir sobre problemas, ahora estamos discutiendo sobre soluciones, y en
específico si las soluciones tan siquiera existen. Antes era una batalla entre
verdad y negación; ahora es una batalla entre esperanza y nihilismo.

No es sólo el cambio climático. Muchas de nosotras pasamos un viaje individual
similar a este con los cigarros: racionalizando un hábito peligroso y
antisocial apoyándonos en la duda costosamente sembrada por las compañías
tabacaleras. Al final el tabaco, así como los gases de invernadero, no
manifiestan sus consecuencias de inmediato - la relación de causa y efecto
está oscurecida por décadas de intervención entre el primer jalón y el
primer tumor. Y así como con el cambio climático, para cuando los síntomas
son imposibles de ignorar, es tentador concluir que es muy tarde para dejarlo:
«Mejor disfrutar el tiempo que nos queda».

Lo que me trae a la privacidad.

Las violaciones a nuestra privacidad comparten muchas de las mismas
características lamentables del cambio climático y los cigarros: sus primeras
revelaciones imprudentes y su primera violación de privacidad es probable que
estén separadas por mucho tiempo y espacio. Es difícil explicar cómo cualquier
revelación le hará daño, pero es casi cierto que después de una gran cantidad
de revelaciones imprudentes alguna de ellas regresará a morderle, y de hecho,
tal vez muy fuerte. Las revelaciones son acumulativas: la foto
georreferenciada aparentemente inocente que publicó hoy se combina con otra
base de datos mañana, o el año siguiente, y le enlaza con el lugar en donde
una doctora estaba escribiendo una prescripción de metadona o en donde una
figura política controversial estaba presente.

Hacer que la gente comparta de más es un gran negocio, y las compañías que se
benefician de la duda pública acerca de los riesgos de unir información
personal han financiado caras campañas de relaciones públicas para convencer
que los riesgos están exagerados y que los daños son inevitables y tolerables.

Pero el hecho es que recolectar, almacenar y analizar tanta información
personal acerca del público como sea posible es un negocio peligroso. El
potencial de daño a los sujetos de esta vigilancia es real, y crece con cada
día que pasa conforme los silos de bases de datos se hacen más grandes.

La gente cuya información personal ha sido recolectada enfrenta una larga
lista de riesgos: chantaje, acoso, extralimitación policiaca o gubernamental,
represalias en el lugar de trabajo y robo de identidad están en el tope de esa
lista, pero esta sigue y sigue (por ejemplo, los nombres de algunas víctimas
de violaciones de privacidad han sido falsificados en cartas a la FCC para
oponerse a la neutralidad de la red).

Más aún, los daños de violaciones de privacidad no desaparecen con el tiempo:
empeoran. Tal vez su cuenta violada en un sitio de recuperación de adicciones
está enlazada a un alias que no está conectado con su nombre hoy, pero que una
década después un sitio diferente en el que usted usaba el mismo alias será
violado y alguna persona inteligente usará ambas violaciones para
reidentificar ese alias suyo antes anónimo.

La privacidad es un deporte de equipo. Los datos de mis cuentas violadas
pueden comprometerle a *usted*, no a mi. Por ejemplo, sus mensajes dirigidos
a mi pueden revelar que estaba pensando en divorciarse de su esposa años
antes, un hecho que es revelado cuando mis datos son violados, años después
de que ambas se hayan reconciliado.

Sume a eso el efecto en progreso social. En memoria viviente, era un crimen
ser gay, fumar marihuana, casarse con alguien de otra raza. Esas leyes
cedieron luego de cambios masivos de actitud social, y esos cambios fueron el
resultado de que las personas marginalizadas pudieron escoger el tiempo y
forma de revelar su yo verdadero a las personas que les rodeaban. Con una
conversación silenciosa a la vez, las personas que vivían una doble vida
fueron capaces de forjar alianzas entre ellas y con sus amistades y
familiares. Quite el derecho de las personas a escoger cómo se revelan a sí
mismas y les quita la posibilidad de reclutar simpatizantes a su causa. Hoy,
alguien a quien aman tiene un secreto que nunca les ha revelado. Si les
quitamos la posibilidad de escoger cómo se revelan a sí mismas con el mundo,
podría nunca llegar a conocer ese secreto. Su persona amada podría irse a la
tumba con pesar sin que usted llegue a darse cuenta por qué.

Lo que es decir que cada día que pasa autorecluta más gente al floreciente
ejército que entiende que hay un problema de privacidad real. Estas personas
están siendo reclutadas gracias a sus experiencias viscerales de violaciones
de privacidad, y están enojadas y heridas.

Eso significa que nuestro trabajo está cambiando. Pasamos décadas sonando la
alarma de un próximo apocalipsis de privacidad sin éxito ante una horda
indiferente, y así como las ambientalistas y activistas del tabaco, fallamos.
El triste estado actual de la privacidad y la tecnología significa que
millones han sido dañadas, y que más daño viene en camino. La única cosa peor
que todo ese sufrimiento sería que se desperdicie.

Las personas que finalmente entienden que hay un problema no necesariamente
entienden que hay una solución: debemos emprender una revisión de tecnología,
leyes, normas sociales y negocios que haga un futuro apto para la vida
humana. Los procesos puestos en marcha por la combinación y retención de datos
tienen consecuencias inevitables, y no vamos a dejar de usar correo
electrónico, navegación satelital o redes sociales, entonces es tentador
concluir que la causa no tiene esperanza, que la única cosa por hacer es
rendirse a lo inevitable.

Esto es nihilismo de privacidad, y es el nuevo frente para activistas de
privacidad. La tecnología nos ha dado vigilancia ubicua sin precedentes, pero
también nos ha dado un fuerte cifrado sin precedentes que hace más posible
evadir la vigilancia que cualquier otro momento en la historia. Hemos
sido testigas del crecimiento de monopolios digitales que nos abusan sin miedo
de ser castigados por un mercado competitivo, pero también hemos vivido el
crecimiento de herramientas digitales que hacen posible que personas
ordinarias se organicen a sí mismas y demanden reglas antimonopolistas fuertes
e impuestas vigorosamente. Nuestras legisladoras torpemente demandan
prohibiciones al cifrado en funcionamiento, pero también son cada vez más
avergonzadas e incluso destruidas políticamente por violaciones de privacidad,
y se están uniendo al coro que demanda mejores reglas de privacidad.

La mala noticia es que convencer a la gente que ponga atención a daños que
están lejanos es tan difícil que bien podría ser imposible. Pero la buena
noticia es que convencer a la gente que el desastre en el que están viviendo
puede ser evitado, con sus buenas acciones y actos enérgicos, es mucho mucho
más fácil. Como especie, podemos ser trágicamente miopes, pero lo compensamos
siendo tan ferozmente indomables cuando el desastre se asoma.

Se está asomando ahora. Hora de empezar a luchar.
